<?php

if ( ! defined( 'ABSPATH' ) ) {

	exit;
	// Exit if accessed directly.
}

/**
 * Handle frontend forms
 *
 * @class Ebay_Form_Handler
 * @version 1.0.0
 * @package ebay-connector/include/
 * @category Class
 * @author webkul
 */
class Ebay_Form_Handler {
	/**
	 * Constructor
	 */
	 public function __construct() {

		add_filter( 'wp_ebay_import_categories', 'ebay_import_categories',10,1 );

		add_action( 'admin_init', array( $this, 'default_settings' ) );


		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_script' ) );

		add_action( 'wk_ebay_tab_view_ebay_account', 'view_ebay_account', 10, 1 );
		add_action( 'wk_ebay_tab_view_import_category', 'view_import_categories', 10, 1 );
		add_action( 'wk_ebay_tab_view_map_category', 'view_ebay_category', 10, 1 );
		add_action( 'wk_ebay_tab_view_map_product', 'view_ebay_products', 10, 1 );
		add_action( 'wk_ebay_tab_view_map_order', 'view_ebay_orders', 10, 1 );
		add_action( 'wk_ebay_tab_view_export_to_ebay', 'view_ebay_exports', 10, 1 );
		add_action( 'wk_ebay_tab_view_feed_back_to_ebay', 'view_feed_back_to_ebay', 10, 1 );
		add_action( 'wk_ebay_tab_view_mapped_ebay', 'view_mapped_categories', 10, 1 );
		// for cron
		add_action( 'wk_ebay_tab_view_cron_job', 'ebay_export_product_to_random_account', 10, 1 );

		add_action( 'save_ebay_account', 'ebay_account_details',10,1 );

		add_action( 'ebay_seller_account_tabs','ebay_account_tab_pane',10,1 );

		add_action( 'admin_menu', 'webkul_ebay_admin_menu' );

		// Ajax Calls.
		add_action( 'wp_ajax_nopriv_wc_ebay_get_store_childs', 'wc_ebay_get_store_childs' );
		add_action( 'wp_ajax_wc_ebay_get_store_childs', 'wc_ebay_get_store_childs' );
		add_action( 'wp_ajax_nopriv_wc_ebay_catogories_childs', 'wc_ebay_catogories_childs' );
		add_action( 'wp_ajax_wc_ebay_catogories_childs', 'wc_ebay_catogories_childs' );

		add_action( 'wp_ajax_nopriv_ebay_category_exist', 'ebay_category_exist' );
		add_action( 'wp_ajax_ebay_category_exist', 'ebay_category_exist' );

		add_action( 'wp_ajax_nopriv_ebay_import_products_from_api', 'ebay_import_products_from_api' );
		add_action( 'wp_ajax_ebay_import_products_from_api', 'ebay_import_products_from_api' );
		add_action( 'wp_ajax_nopriv_ebay_import_products_raw_data', 'ebay_import_products_raw_data');
		add_action( 'wp_ajax_ebay_import_products_raw_data', 'ebay_import_products_raw_data');
		add_action( 'wp_ajax_nopriv_ebay_import_products', 'ebay_import_products' );
		add_action( 'wp_ajax_ebay_import_products', 'ebay_import_products' );
		add_action( 'wp_ajax_nopriv_get_ebay_import_products_api_data', 'get_ebay_import_products_api_data' );
		add_action( 'wp_ajax_get_ebay_import_products_api_data', 'get_ebay_import_products_api_data' );

		add_action( 'wp_ajax_nopriv_ebay_import_orders_from_api', 'ebay_import_orders_from_api' );
		add_action( 'wp_ajax_ebay_import_orders_from_api', 'ebay_import_orders_from_api' );
		add_action( 'wp_ajax_nopriv_get_ebay_import_orders_api_data', 'get_ebay_import_orders_api_data' );
		add_action( 'wp_ajax_get_ebay_import_orders_api_data', 'get_ebay_import_orders_api_data' );
		add_action( 'wp_ajax_nopriv_ebay_import_orders', 'ebay_import_orders' );
		add_action( 'wp_ajax_ebay_import_orders', 'ebay_import_orders' );
		
		add_action( 'wp_ajax_nopriv_import_message_from_ebay', 'import_message_from_ebay' );
		add_action( 'wp_ajax_import_message_from_ebay', 'import_message_from_ebay' );
		add_action( 'wp_ajax_nopriv_get_message_ebay', 'get_message_ebay' );
		add_action( 'wp_ajax_get_message_ebay', 'get_message_ebay' );
		add_action( 'wp_ajax_nopriv_reply_message_ebay', 'reply_message_ebay' );
		add_action( 'wp_ajax_reply_message_ebay', 'reply_message_ebay' );

		add_action( 'wp_ajax_nopriv_ebay_import_categories_raw_data', 'ebay_import_categories_raw_data' );
		add_action( 'wp_ajax_ebay_import_categories_raw_data', 'ebay_import_categories_raw_data' );
		add_action( 'wp_ajax_nopriv_ebay_import_cat_from_api', 'ebay_import_cat_from_api' );
		add_action( 'wp_ajax_ebay_import_cat_from_api', 'ebay_import_cat_from_api' );
		add_action( 'wp_ajax_nopriv_ebay_import_categories_finally', 'ebay_import_categories_finally' );
		add_action( 'wp_ajax_ebay_import_categories_finally', 'ebay_import_categories_finally' );
		add_action( 'wp_ajax_nopriv_get_ebay_import_category_api_data', 'get_ebay_import_category_api_data' );
		add_action( 'wp_ajax_get_ebay_import_category_api_data', 'get_ebay_import_category_api_data' );

		add_action( 'add_meta_boxes', 'ebay_condition_id_meta_box' );

		add_action( 'save_post', 'ebay_condition_id_meta_box_save' );

	}

	 function default_settings() {

		register_setting( 'webkul-ebay-settings-group', 'ebay_access_token' );
		register_setting( 'webkul-ebay-settings-group', 'ebay_certificate_id' );
		register_setting( 'webkul-ebay-settings-group', 'ebay_dev_id' );
		register_setting( 'webkul-ebay-settings-group', 'ebay_app_id' );
		register_setting( 'webkul-ebay-settings-group', 'ebay_mode' );
		register_setting( 'webkul-ebay-settings-group', 'ebay_global_site' );
		register_setting( 'webkul-ebay-settings-group', 'ebay_userid' );

		add_action( 'deleted_post', array( $this, 'on_product_delete' ), 10 );

	}

	function on_product_delete( $post_id ) {

		if ( ! empty( $post_id ) ) {

			global $wpdb;

			$product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';

			if( $wpdb->get_var( "SHOW TABLES LIKE '$product_map_table'" ) == $product_map_table ) {

				$product_id = $wpdb->get_row( "SELECT store_product_id FROM $product_map_table WHERE store_product_id='$post_id'");

				if( ! empty( $product_id ) ) {

					$delete_raw = $wpdb->delete(
						$product_map_table,
						array(
							'store_product_id'   => $post_id
						),
						array( '%d' )
					);

				}
			}
		}

	}

	function admin_enqueue_script() {


		$loader_url = EBAY_API;

		wp_enqueue_script( 'jquery-ui-datepicker' );

		wp_enqueue_style( 'jquery-ui-css', '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' );

		wp_enqueue_script( 'ebay-script', EBAY_API . 'assets/js/plugin-ebay.js', array( 'jquery' ) );

		wp_enqueue_style( 'ebay-style', EBAY_API . 'assets/css/style.css', array() );

		wp_localize_script( 'ebay-script', 'api_script', array( 'api_admin_ajax' => admin_url( 'admin-ajax.php' ), 'api_nonce' => wp_create_nonce( 'api-ajaxnonce' ), 'loader_url' => $loader_url ) );

		wp_enqueue_script( 'select2-js', plugins_url() . '/woocommerce/assets/js/select2/select2.min.js' );

		wp_enqueue_style( 'select2', plugins_url() . '/woocommerce/assets/css/select2.css' );

	}

}

new Ebay_Form_Handler();
