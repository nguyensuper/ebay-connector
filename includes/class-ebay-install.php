<?php
/**
 * Installation related functions and actions.
 *
 * @author 		Webkul
 *
 * @category 	Admin
 *
 * @version     1.0.0
 */
if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

if (!class_exists('Ebay_Install')) :
    /**
     * Ebay_Install Class.
     */
    class Ebay_Install
    {
        /**
         * Hook in tabs.
         */
        public function __construct()
        {
            register_activation_hook(EBAY_PLUGIN_FILE, array($this, 'install'));
            register_deactivation_hook(EBAY_PLUGIN_FILE, array($this, 'product_updator_uninstaller'));
            add_action('update_products', array($this, 'perform_product_update'));
            add_filter('cron_schedules', array($this, '_add_custom_scheduler'));
        }

        public function product_updator_uninstaller()
        {
            wp_clear_scheduled_hook('update_products');
        }

        public function perform_product_update()
        {
            $start_time = strtotime(get_option('wc_ebay_cron_start_date'));
            $end_time = strtotime(get_option('wc_ebay_cron_end_date'));

            $current_time = strtotime(current_time('Y-m-d H:i:s'));

            if (get_option('get_process_for_cron') == 1) {
                $export_to_ebay = new Export_To_Ebay();
            } elseif (get_option('get_process_for_cron') == 2) {
                $export_to_ebay = new Update_From_Ebay();
            } elseif (get_option('get_process_for_cron') == 3) {
                $export_to_ebay = new Update_To_Ebay();
            }

            $return_data = '';
            $account_id = get_option('ebay_accounts');
            $email = get_option('ebay_email_id');
            $start_cron = get_option('start_cron');

            if ($start_cron != '') {
                if ($current_time >= $start_time && $current_time < $end_time) {
                    if ($account_id) {
                        $data = $this->table_data();
                        foreach ($data as $key => $value) {
                            $acks = array();
                            if (get_option('get_process_for_cron') == 1) {
                                $export_to_ebay = new Export_To_Ebay();
                                $acks = $export_to_ebay->ebay_export($value->product_id, $account_id);
                            } elseif (get_option('get_process_for_cron') == 2) {
                                $export_to_ebay = new Update_From_Ebay();
                                $acks = $export_to_ebay->woo_update($value->product_id, $account_id);
                            } elseif (get_option('get_process_for_cron') == 3) {
                                $export_to_ebay = new Update_To_Ebay();
                                $acks = $export_to_ebay->ebay_update($value->product_id, $account_id);
                            }
                            if (!empty($acks)) {
                                dump($acks['Ack']);
                                if ($acks['Ack'] == 'success') {
                                    foreach ($acks['msg'] as $msg) {
                                        $return_data = $return_data.'<div style="background: #fff; border-left: 4px solid #46b450; box-shadow: 0 1px 1px 0 rgba(0,0,0,.1); margin: 5px 15px 2px; padding: 1px 12px;">';

                                        $return_data = $return_data.'<p>'.$msg.'updated successfully</p>';

                                        $return_data = $return_data.'</div>';
                                    }
                                } else if ($acks['Ack'] == 'warning') {
                                    foreach ($acks['msg'] as $msg) {
                                        $return_data = $return_data.'<div class="notice notice-warning "style="background: #fff; border-left: 4px solid red; box-shadow: 0 1px 1px 0 rgba(0,0,0,.1); margin: 5px 15px 2px; padding: 1px 12px;">';
                                        $return_data = $return_data.'<p>'.$msg.'</p>';

                                        $return_data = $return_data.'</div>';
                                    }
                                } else {
                                    foreach ($value['msg'] as $msg) {
                                        $return_data = $return_data.'<div class="notice notice-error "style="background: #fff; border-left: 4px solid red; box-shadow: 0 1px 1px 0 rgba(0,0,0,.1); margin: 5px 15px 2px; padding: 1px 12px;">';
                                        $return_data = $return_data.'<p>'.$msg.'</p>';

                                        $return_data = $return_data.'</div>';
                                    }
                                }
                            }
                        }

                        $headers = 'MIME-Version: 1.0'."\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1'."\n";
                        $headers .= "X-Priority: 1 (Highest)\n";
                        $headers .= "X-MSMail-Priority: High\n";
                        $headers .= "Importance: High\n";
                        if ($email != '') {
                            wp_mail($email, 'Sync Report(For export product to ebay)', $return_data, $headers);
                        }
                    }
                }
            }
        }

        private function table_data()
        {
            global $wpdb;
            $table_name = $wpdb->prefix.'woocommerce_ebay_product_map';

            if (get_option('get_process_for_cron') == 1) {
                $data = array();
                $id = array();

                $s = isset($_POST['s']) ? $_POST['s'] : '';

                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => -1,
                    'post_status' => 'publish',
                );

                $loop = new WP_Query($args);
                $i = 0;
                while ($loop->have_posts()) : $loop->the_post();

                $product_id = get_the_ID();

                $res = $wpdb->get_results("SELECT store_product_id FROM $table_name WHERE store_product_id = $product_id");
                $ebay_products = wp_list_pluck($res, 'store_product_id');

                if (empty($ebay_products) || !in_array($product_id, $ebay_products)) {
                    $id[] = $product_id;

                    $_product = wc_get_product($product_id);

                    $data[] = array(
                            'product_id' => $id[$i],
                        );

                    ++$i;
                }

                endwhile;
            } elseif (get_option('get_process_for_cron') == 2 || get_option('get_process_for_cron') == 3) {
                $data = $wpdb->get_results("SELECT store_product_id as product_id FROM $table_name");
            }

            return $data;
        }

        public function _add_custom_scheduler($schedules)
        {
            $time_interval = get_option('cron_interval');

            if ($time_interval) {
                $schedules['time_interval'] = array(
                    'interval' => $time_interval * 3600,
                    'display' => __('Time Interval'),
                );

                return $schedules;
            } else {
                $schedules['time_interval'] = array(
                    'interval' => 24 * 3600,
                    'display' => __('Time Interval'),
                );

                return $schedules;
            }
        }

        /**
         * Install table.
         */
        public function install()
        {
            global $wpdb;

            $wpdb->hide_errors();

            $charset_collate = $wpdb->get_charset_collate();

            $sql = "
            CREATE TABLE {$wpdb->prefix}woocommerce_ebay_account (
            id bigint(20) NOT NULL auto_increment,
            ebay_global_site_id bigint(20),
            ebay_userid varchar(50),
            store_name varchar(50),
            access_token longtext,
            dev_id varchar(100),
            app_id varchar(100),
            cer_id varchar(100),
            postal_code varchar(100),
            ebay_paypal longtext,
            status tinyint,
            fetch_chat_at bigint(20),
            update_status_at bigint(20),
            cron_at bigint(20),
            status_msg longtext,
            brand varchar(100),
            UNIQUE KEY id (id)
            ) $charset_collate;
            CREATE TABLE {$wpdb->prefix}woocommerce_ebay_category_raw_data (
            id varchar(100) NOT NULL,
            item_data longtext,
            PRIMARY KEY id (id)
            ) $charset_collate;
            CREATE TABLE {$wpdb->prefix}woocommerce_ebay_category_map (
            id bigint(20) NOT NULL auto_increment,
            category_id bigint(20) NOT NULL,
            ebay_category_id bigint(20),
            ebay_category_name varchar(100) UNIQUE,
            category_specification text,
            category_condition text,
            UNIQUE KEY id (id)
            ) $charset_collate;
            CREATE TABLE {$wpdb->prefix}woocommerce_ebay_product_map (
            id bigint(20) NOT NULL auto_increment,
            ebay_product_id bigint(20),
            ebay_product_name varchar(100),
            product_type varchar(20),
            store_product_id bigint(20),
            store_category varchar(100),
            UNIQUE KEY id (id)
            ) $charset_collate;
            CREATE TABLE {$wpdb->prefix}woocommerce_ebay_product_raw_data (
            id varchar(100) NOT NULL,
            item_data longtext,
            PRIMARY KEY id (id)
            ) $charset_collate;
            CREATE TABLE {$wpdb->prefix}woocommerce_ebay_order_raw_data (
            order_id varchar(100) NOT NULL,
            order_data longtext,
            PRIMARY KEY order_id (order_id)
            ) $charset_collate;
            CREATE TABLE {$wpdb->prefix}woocommerce_ebay_order_map (
            id bigint(20) NOT NULL auto_increment,
            ebay_order_id bigint(100) UNIQUE NOT NULL,
            store_order_id bigint(20),
            ebay_order_status varchar(100),
            created_at varchar(100),
            UNIQUE KEY id (id)
            ) $charset_collate;
            CREATE TABLE {$wpdb->prefix}woocommerce_ebay_messenger (
            id bigint(20) NOT NULL UNIQUE auto_increment,
            ebay_sender varchar(255),
            ebay_messenger_id bigint(20),
            ebay_messenger_subject longtext,
            ebay_messenger_content longtext,
            ebay_messenger_type longtext,
            ebay_messenger_received_at bigint(20),
            ebay_messenger_expiration_at bigint(20),
            ebay_external_message_id bigint(20),
            ebay_item_id bigint(20),
            store_product_id bigint(20),
            status tinyint,
            account_id bigint(20),
            created_at varchar(100),
            PRIMARY KEY id (id)
            ) $charset_collate;
            ";

            require_once ABSPATH.'wp-admin/includes/upgrade.php';
            if (!wp_next_scheduled('update_products')) {
                wp_schedule_event(time(), 'time_interval', 'update_products');
            }
            dbDelta($sql);
        }
    }

endif;

return new Ebay_Install();
