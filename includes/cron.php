<?php
if ( ! defined('ABSPATH') ) {
    /** Set up WordPress environment */
    require_once( dirname( __FILE__, 5 ) . '/wp-load.php' );
}
do_action( 'ebay_send_slack', "Cron start running!!!");
require_once( dirname( __FILE__ ) . '/admin/export.php' );
require_once( dirname( __FILE__ ) . '/admin/messenger.php' );

global $wpdb;
$product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';
$account_table = $wpdb->prefix.'woocommerce_ebay_account';
$export_to_ebay = new Export_To_Ebay();

$queryAcc = "SELECT * FROM $account_table where (status = 0 or status is null)";
$accounts = $wpdb->get_results($queryAcc);
if (!count($accounts)) die('Done! no account is live');
// $accIds = wp_list_pluck($accounts, 'id');
$ranAccKey=array_rand($accounts, 1);
$account = $accounts[$ranAccKey];
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'post_status' => 'publish',
);

$loop = new WP_Query($args);
while ($loop->have_posts()) : $loop->the_post();
  $product_id = get_the_ID();
  $product_name = get_the_title();
  $account_name = trim($account->store_name);
  $acocunt_ebay_id = trim($account->ebay_userid);
  $res = $wpdb->get_results("SELECT store_product_id FROM $product_map_table WHERE store_product_id = $product_id");
  $ebay_products = wp_list_pluck($res, 'store_product_id');
  if (empty($ebay_products) || !in_array($product_id, $ebay_products)) {
  	$res = $export_to_ebay->ebay_export($product_id, $account->id, true);
  	// http://wordpress.local/wp-content/plugins/ebay-connector/includes/cron.php
      $errCode = [];
  	if (isset($res->Ack) && strtolower($res->Ack) == 'success') {
  		// noti to slack
		    do_action( 'ebay_send_slack', "Product *{$product_name}* was export to ebay account: *{$account_name}*({$acocunt_ebay_id})" );
  	} elseif (isset($res->Ack) && strtolower($res->Ack) == 'warning') {
  		// noti to slack
		    do_action( 'ebay_send_slack',
		          "Product *{$product_name}* was export to ebay account: *{$account_name}*({$acocunt_ebay_id}), but have warning(s):",
		          'warning' );
          // ErrorCode
		    if (isset($res->Errors)) {
              $errors = is_array($res->Errors) ? $res->Errors : [$res->Errors];
              foreach ($errors as $err) {
                  if (isset($err->SeverityCode)) {
                      do_action( 'ebay_send_slack',
                                "Product: *{$product_name}* - Account: *{$account_name}* ===> {$err->LongMessage}",
                                $err->SeverityCode);
                  }
                  if (isset($err->ErrorCode)) {
                      $errCode[$err->ErrorCode] = ['type' => $err->SeverityCode, 'msg' => $product_name . "-" . $err->LongMessage];
                  }
              }
        }
  	} else {
  		// noti to slack
  		do_action( 'ebay_send_slack',
		          "Product *{$product_name}* exporting to ebay account: *{$account_name}*({$acocunt_ebay_id}) return Error(s):",
		          'error' );
          $mesForCheck = "";
		  if (isset($res->Errors) && is_array($res->Errors)) {
        $errors = is_array($res->Errors) ? $res->Errors : [$res->Errors];
        foreach ($errors as $err) {
            if (isset($err->SeverityCode)) {
                do_action( 'ebay_send_slack',
                          "Product: *{$product_name}* - Account: *{$account_name}* ===> {$err->LongMessage}",
                          $err->SeverityCode);
            }
            if (isset($err->ErrorCode)) {
                $errCode[$err->ErrorCode] = ['type' => $err->SeverityCode, 'msg' => $product_name . "-" . $err->LongMessage];
            }
            $mesForCheck .= $err->LongMessage;
        }
    }
		// and update product status
    if (preg_match('/(Policy|policy)/', $mesForCheck)) {
        wp_delete_post($product_id);
    }
    if (isset($res['msg']) && $res['msg'] == 'Description empty') {
      continue;
    }
	}
  if (isset($errCode[21919456])) {
      unset($errCode[21919456]);
  }
  if (count($errCode)) {
        $action = 'update_ebay_account_status_pending';
        // full
        if (isset($errCode[21919188])) {
            $action = 'update_ebay_account_status_full';
        }
        do_action( $action, $account->id );
        $msg = json_encode($errCode);
        do_action( 'update_ebay_account_status_msg', $account->id, $msg );
    }
    do_action( 'update_ebay_account_cron_at', $account->id );
    if (count($errCode)) {
        die('Cron done!');
    }
    break;
  }
endwhile;
// fetch messengers
$messengerClass = New Messenger();
$messengerClass->wc_ebay_get_api_raw_data(0);
die('Cron done!!!');
?>
