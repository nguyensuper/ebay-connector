<?php

if (!defined('ABSPATH')) {
    exit;
}

// Admin settings page

if (!function_exists('webkul_ebay_admin_menu')) {
    function webkul_ebay_admin_menu()
    {
        require_once EBAY_DIR.'/includes/admin/menu/items.php';
    }
}

if (!function_exists('view_ebay_account')) {
    function view_ebay_account($eid)
    {
        ?>

      <div id="map_account" class="panel woocommerce_options_panel">

        <form method="post">

          <?php wp_nonce_field('ebay_action', 'ebay_nonce_field'); ?>

          <?php

            require_once EBAY_DIR.'/templates/tabs/ebay-map-account.php'; ?>

          <div class="asve-conf">

              <input type="hidden" name="ebay_accid" value="<?php echo $eid; ?>" />

              <button type="submit" class="button button-primary" name="submit-ebay-conf">Save Configuration</button>

          </div>



        </form>

      </div>

  <?php
    }
}

if (!function_exists('view_import_categories')) {
    function view_import_categories($eid)
    {
        ?>

        <div id="import_category" class="panel woocommerce_options_panel">

          <?php

             require_once EBAY_DIR.'/templates/tabs/ebay-import-categories.php'; ?>

        </div>

    <?php
    }
}

if (!function_exists('view_ebay_category')) {
    function view_ebay_category($eid)
    {
        ?>

      <div id="map_category" class="panel woocommerce_options_panel">

        <?php

           require_once EBAY_DIR.'/templates/tabs/ebay-map-category.php'; ?>

      </div>

  <?php
    }
}

if (!function_exists('view_ebay_products')) {
    function view_ebay_products($eid)
    {
        ?>

      <div id="map_product" class="panel woocommerce_options_panel">

        <?php

          require_once EBAY_DIR.'/templates/tabs/ebay-map-products.php'; ?>

      </div>

  <?php
    }
}

if (!function_exists('view_ebay_orders')) {
    function view_ebay_orders($eid)
    {
        ?>

      <div id="map_order" class="panel woocommerce_options_panel">

        <?php

         require_once EBAY_DIR.'/templates/tabs/ebay-map-orders.php'; ?>

      </div>

  <?php
    }
}

if (!function_exists('view_ebay_exports')) {
    function view_ebay_exports($eid)
    {
        ?>
      <div id="import_to_ebay" class="panel woocommerce_options_panel">

        <?php

          require_once EBAY_DIR.'/templates/tabs/ebay-export.php'; ?>

      </div>

    <?php
    }
}

// view_feed_back_to_ebay
if ( ! function_exists( 'view_feed_back_to_ebay' ) ) {
    /**
     * Customize: Panel.
     *
     * Customize function.
     *
     * @param  object WP_Customize  Instance of the WP_Customize_Manager class.
     * @since  1.0.0
     */
    function view_feed_back_to_ebay( $wp_customize ) {
        ?>
      <div id="feedback_to_ebay" class="panel woocommerce_options_panel">

        <?php

          require_once EBAY_DIR.'/templates/tabs/ebay-feedback.php'; ?>

      </div>

    <?php
    }
}


if (!function_exists('view_mapped_categories')) {
    function view_mapped_categories($eid)
    {
        ?>
      <div id="mapped_ebay" class="panel woocommerce_options_panel ebay_fields">

        <?php

          require_once EBAY_DIR.'/templates/tabs/mapped-categories.php'; ?>

      </div>

    <?php
    }
}

if (!function_exists('ebay_account_details')) {
    function ebay_account_details($post)
    {
        // if this fails, check_admin_referer() will automatically print a "failed" page and die.

        if (!empty($_POST) && check_admin_referer('ebay_action', 'ebay_nonce_field')) {
            global $wpdb;

            $table_name = $wpdb->prefix.'woocommerce_ebay_account';

            // process form data

            $ebay_id = intval($post['ebay_accid']);
            $store_name = esc_attr($post['ebay_store_name']);
            $ebay_userid = esc_attr($post['ebay_userid']);
            $ebay_global_sites = esc_attr($post['ebay_global_sites']);
            $ebay_access_token = $post['ebay_access_token'];
            $ebay_dev_id = esc_attr($post['ebay_dev_id']);
            $ebay_app_id = esc_attr($post['ebay_app_id']);
            $ebay_certication_id = esc_attr($post['ebay_certication_id']);
            $shop_postal_code = esc_attr($post['shop_postal_code']);
            $email_paypal = esc_attr($post['ebay_paypal']);
            $brand = esc_attr($post['brand']);
            if (!empty($store_name) && !empty($ebay_access_token) && !empty($ebay_app_id) && !empty($ebay_certication_id) && !empty($ebay_dev_id) && !empty($shop_postal_code) && !empty($email_paypal)) {
                $settings = new Categories();
                $token_check = $settings->check_access_token($ebay_access_token, $ebay_id);
                $data = $settings->ebay_add_account_validate($post);
                $client = $data['client'];
                if ($client) {
                    $current_ebay_access_token = '';
                    if( !empty( $ebay_id ) ) {
                        $ebay_account = new Ebay_Account($ebay_id);
                        $current_ebay_access_token = $ebay_account->get_access_token();
                    }
                    if ($current_ebay_access_token !== $ebay_access_token) {
                        $results = $client->getRefreshToken($ebay_access_token);
                        $ebay_access_token = $results['refresh_token'] ?? '';
                        $post['ebay_access_token'] = $ebay_access_token;
                    }
                    $data = $settings->ebay_add_account_validate($post);
                    $client = $data['client'];
                    if (!$client) {
                        ?>
                      <div class="notice notice-error my-acf-notice is-dismissible" >
                        <p><?php echo __('Someting when wrong.! Pls try agan.', 'wc_ebay'); ?></p>
                      </div>

                        <?php
                      return;
                    }
                    $userInfo = $client->GetUser([]);
                    $ebay_userid = $userInfo->User->UserID;
                    if (empty($ebay_access_token)) {
                        ?>
                      <div class="notice notice-error my-acf-notice is-dismissible" >
                        <p><?php echo 'Token not access'; ?></p>
                      </div>

                    <?php

                      return;
                    } elseif (!empty($token_check)) {
                        ?>
                      <div class="notice notice-error my-acf-notice is-dismissible" >
                        <p><?php echo __('Cannot use same access token for two accounts.!', 'wc_ebay'); ?></p>
                      </div>

                    <?php

                      return;
                    }
                    // if client account exist.
                    if ($ebay_id) {
                        $result = $wpdb->update(
                        $table_name,
                        array(
                          'ebay_global_site_id' => $ebay_global_sites,
                          'ebay_userid' => $ebay_userid,
                          'access_token' => $ebay_access_token,
                          'dev_id' => $ebay_dev_id,
                          'app_id' => $ebay_app_id,
                          'cer_id' => $ebay_certication_id,
                          'postal_code' => $shop_postal_code,
                          'ebay_paypal' => $email_paypal,
                          'brand' => $brand
                        ), array(
                          'id' => $ebay_id,
                        )
                      );
                    } else {
                        $result = $wpdb->insert(
                        $table_name,
                        array(
                          'ebay_global_site_id' => $ebay_global_sites,
                          'ebay_userid' => $ebay_userid,
                          'store_name' => $store_name,
                          'access_token' => $ebay_access_token,
                          'dev_id' => $ebay_dev_id,
                          'app_id' => $ebay_app_id,
                          'cer_id' => $ebay_certication_id,
                          'postal_code' => $shop_postal_code,
                          'ebay_paypal' => $email_paypal,
                          'brand' => $brand
                        )
                      );
                    }

                    if ($result) {
                        ?>
                        <div class="notice notice-success my-acf-notice is-dismissible" >
                            <p><?php _e('All details are saved successfully!', 'wc_ebay'); ?></p>
                        </div>

                        <?php
                    }
                } else {
                    ?>
                    <div class="notice notice-error my-acf-notice is-dismissible" >
                      <p><?php _e('Please fill all the fields!', 'wc_ebay'); ?></p>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="notice notice-error my-acf-notice is-dismissible" >
                  <p><?php _e('Please fill all the fields!', 'wc_ebay'); ?></p>
                </div>
                <?php
            }
        }
    }
}

if (!function_exists('wc_ebay_get_store_childs')) {
    function wc_ebay_get_store_childs()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $child_arr = array();

            $parent_id = $_POST['child_id'];    // parent category id

            // Get child categories from parent id

            $get_child_category = get_terms('product_cat', array('child_of' => $parent_id, 'hide_empty' => false));

            // Filter products from child category

            if (!empty($get_child_category)) {
                foreach ($get_child_category as $child_category) {
                    array_push($child_arr,
                    array(
                        'cat_id' => $child_category->term_id,
                        'cat_name' => $child_category->name,
                      )
                    );
                }
            }

            wp_send_json($child_arr);

            wp_die();
        }
    }
}

if (!function_exists('wc_ebay_screen_option')) {
    function wc_ebay_screen_option()
    {
        $options = 'per_page';

        $args = array(
        'label' => 'account Per Page',
        'default' => 20,
        'option' => 'wc_ebay_per_page',
    );

        add_screen_option($options, $args);
    }
}

if (!function_exists('ebay_settings_page')) {
    function ebay_settings_page()
    {
        require_once EBAY_DIR.'/templates/ebay-settings.php';
    }
}

if (!function_exists('ebay_messgener_page')) {
    function ebay_messgener_page()
    {
        require_once EBAY_DIR.'/templates/tabs/ebay-feedback.php';
    }
}

 if (!function_exists('ebay_category_exist')) {
     function ebay_category_exist()
     {
         if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
             $results = $conditionAttrValues = array();

             $store_id = $_POST['store_cat_id'];

             $ebay_leaf_id = $_POST['ebay_leaf_id'];

             $cat_name = $_POST['cat_name'];

             global $wpdb;

             $table_name = $wpdb->prefix.'woocommerce_ebay_category_map';

             if (!empty($ebay_leaf_id) && !empty($store_id)) {
                 $res = $wpdb->get_results("SELECT id FROM $table_name WHERE category_id = $store_id OR ebay_category_id = $ebay_leaf_id");

                 if (!empty($res)) {
                     $result['msg'] = 'Leaf Already Mapped';

                     wp_send_json($result);

                     die;
                 } else {
                     /**for get ebay category condition and variations **/
                     $account_id = $_POST['acc_id'];

                     $settings = new Categories();

                     try {
                         $data = $settings->ebay_conf($account_id);
                         $eBayConfig = $data['eBayConfig'];
                         $client = $data['client'];
                         if ($client) {
                             $params = ['Version' => 891,
                                            'DetailLevel' => 'ReturnAll',
                                            'WarningLevel' => 'High',
                                            'CategoryID' => $ebay_leaf_id,
                                            'ViewAllNodes' => true,
                                          ];
                             $results = $client->GetCategoryFeatures($params);

                             if (isset($results->Ack)) {
                                 $varsEnabled = 0;

                                 if (isset($results->Category->VariationsEnabled)) {
                                     $varsEnabled = (int) $results->Category->VariationsEnabled;
                                 }

                                 $conditionAttr = 'N/A';

                                 if (isset($results->Category->ConditionValues)) {
                                     $conditionAttrValues = ebay_convertConditionInArray($results->Category->ConditionValues);
                                 }

                                 //get specification of category form eBay
                                 $params = [
                                          'CategorySpecific' => ['CategoryID' => $ebay_leaf_id],
                                          'MaxValuesPerName' => 2147483647,
                                          'Version' => 891,
                                        ];

                                 $results = $client->GetCategorySpecifics($params);

                                 $specsAttr = createProSpecsAttr($results, $cat_name, $store_id);
                             }
                         }
                     } catch (\Exception $e) {
                         $result['msg'] = $e->getMessage();
                         wp_send_json($result);
                         die;
                     }

                     if (!empty($specsAttr)) {
                         $specs_id = maybe_serialize($specsAttr);
                     } else {
                         $specs_id = '';
                     }

                     if ($conditionAttrValues) {
                         $conditionIds = maybe_serialize($conditionAttrValues);
                     } else {
                         $conditionIds = '';
                     }

                     $insert = $wpdb->insert(
                          $table_name,
                          array(
                            'category_id' => $store_id,
                            'ebay_category_id' => $ebay_leaf_id,
                            'ebay_category_name' => $cat_name,
                            'category_specification' => $specs_id,
                            'category_condition' => $conditionIds,
                          )
                        );

                     if ($insert) {
                         $result['success'] = true;
                         wp_send_json($result);
                         die;
                     }
                 }
             } else {
                 $result['msg'] = 'invalid data';
                 wp_send_json($result);
                 die;
             }
         }
     }
 }

if (!function_exists('ebay_convertConditionInArray')) {
    function ebay_convertConditionInArray($conditionIds)
    {
        $Arr = array();
        foreach ($conditionIds->Condition as $key => $value) {
            $Arr[] = array(
        'id' => $value->ID,
        'name' => $value->DisplayName,
      );
        }

        return $Arr;
    }
}

if (!function_exists('wc_ebay_catogories_childs')) {
    function wc_ebay_catogories_childs()
    {
        $catarr = array();
        $account_id = $_POST['ebay_accid'];
        $parent_category = $_POST['parent'];
        $level = $_POST['level'];
        $settings = new Categories();

        try {
            $data = $settings->ebay_conf($account_id);
            $eBayConfig = $data['eBayConfig'];
            $client = $data['client'];
            if ($client) {
                $params = [
                                    'Version' => 853,
                                    'SiteID' => 0,
                                    'CategorySiteID' => $eBayConfig['globalsites'],
                                    'LevelLimit' => $level,
                                    'ViewAllNodes' => true,
                                    'DetailLevel' => 'ReturnAll',
                                    'CategoryParent' => $parent_category,
                                ];

                $result = $client->GetCategories($params);

                if ($result->CategoryCount != 1 && isset($result->CategoryArray->Category)) {
                    foreach ($result->CategoryArray->Category as $value) {
                        if (isset($value->LeafCategory)) {
                            $leaf = $value->LeafCategory;
                        } else {
                            $leaf = false;
                        }

                        array_push($catarr,
                                        array(
                                          'cat_name' => $value->CategoryName,
                                          'category_id' => $value->CategoryID,
                                          'category_pid' => $value->CategoryParentID,
                                          'Is_Leaf' => $leaf,
                                        )
                                      );
                    }
                    if (!empty($catarr)) {
                        $results['success'] = $catarr;
                    } else {
                        $msg = 'No parent categories.';
                        $results['msg'] = $msg;
                    }
                } else {
                    $msg = 'Leaf category.!';
                    $results['msg'] = __($msg);
                }
            } else {
                $msg = 'Please fill all ebay details in configuration';
                $results['msg'] = __($msg);
            }
        } catch (\Exception $e) {
            $results['msg'] = $e->getMessage();
        }

        wp_send_json($results);
        wp_die();
    }
}

          function recursive_array_searc($needle, $haystack, $currentKey = '', $return = '')
          {
              foreach ($haystack as $key => $value) {
                  if (is_object($value)) {
                      $value = (array) $value;
                  }
                  if (is_array($value)) {
                      if (is_numeric($key)) {
                          $key = '['.$key.']';
                      } else {
                          $key = '->'.$key;
                      }
                      $nextKey = recursive_array_searc($needle, $value, $currentKey.$key, $value);
                      if ($nextKey) {
                          return $nextKey;
                      }
                  } elseif ($value == $needle) {
                      return $return;
                      //  return is_numeric($key) ? $currentKey . '[' .$key . ']' : $currentKey;
                  }
              }

              return false;
          }

  if (!function_exists('ebay_import_cat_from_api')) {
      function ebay_import_cat_from_api()
      {
          $catarr = array();
          $results = array();

          $account_id = $_POST['ebay_accid'];

          $settings = new Categories();

          try {
              $data = $settings->ebay_conf($account_id);
              $eBayConfig = $data['eBayConfig'];
              $client = $data['client'];
              if ($client) {
                  $params = [
                          'Version' => 891,
                          'CategoryStructureOnly' => true,
                          'LevelLimit' => 3,
                          'ViewAllNodes' => true,
                          'DetailLevel' => 'ReturnAll',
                      ];

                  $result = $client->GetStore($params);

                  if (isset($result->Store->CustomCategories->CustomCategory)) {
                      $results['success'] = $result->Store->CustomCategories->CustomCategory;
                  } else {
                        $msg = [];
                        if (isset($result->Errors) && is_array($result->Errors)) {
                            foreach ($result->Errors as $err) {
                                if (isset($err->SeverityCode) && $err->SeverityCode == 'Error') {
                                    $msg[] = $err->LongMessage;
                                }
                            }
                        } elseif (isset($result->Errors)) {
                            if (isset($result->Errors->SeverityCode) && $result->Errors->SeverityCode == 'Error') {
                                $msg[] = $result->Errors->LongMessage;
                            }
                        }
                        $results['msg'] = implode(', ', $msg);
                  }
              } else {
                  $results['msg'] = 'Error settup account';
              }
          } catch (\Exception $e) {
              $results['msg'] = $e->getMessage();
          }

          wp_send_json($results);
          wp_die();
      }
  }

  if (!function_exists('ebay_import_categories_raw_data')) {
      function ebay_import_categories_raw_data()
      {
          if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
              $raw_data = $_POST['raw_data'];

              $res = Categories::wc_ebay_insert_categories_in_bulk($raw_data);

              wp_send_json($res);

              wp_die;
          }
      }
  }

if (!function_exists('get_ebay_import_category_api_data')) {
    function get_ebay_import_category_api_data()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $result = Categories::wc_ebay_get_api_category_data();

            wp_send_json($result);

            wp_die;
        }
    }
}

  if (!function_exists('ebay_import_categories_finally')) {
      function ebay_import_categories_finally()
      {
          if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
              $raw_categories = json_decode(stripslashes($_POST['raw_data']));

              $result = Categories::import_categories_to_db($raw_categories);

              wp_send_json($result);

              die;
          }
      }
  }

    if (!function_exists('ebay_import_categories')) {
        function ebay_import_categories($aid)
        {
            $catarr = array();
            $results = array();

            $account_id = $aid;

            $settings = new Categories();

            try {
                $data = $settings->ebay_conf($account_id);
                $eBayConfig = $data['eBayConfig'];
                $client = $data['client'];
                if ($client) {
                    $params = [
                                      'Version' => 853,
                                      'SiteID' => 0,
                                      'CategorySiteID' => $eBayConfig['globalsites'],
                                      'LevelLimit' => 1,
                                      'ViewAllNodes' => true,
                                      'DetailLevel' => 'ReturnAll',
                                  ];

                    $result = $client->GetCategories($params);

                    if (isset($result->CategoryArray->Category)) {
                        foreach ($result->CategoryArray->Category as $value) {
                            if (isset($value->LeafCategory)) {
                                $leaf = $value->LeafCategory;
                            } else {
                                $leaf = false;
                            }

                            array_push($catarr,
                                          array(
                                            'cat_name' => $value->CategoryName,
                                            'category_id' => $value->CategoryID,
                                            'category_pid' => $value->CategoryParentID,
                                            'Is_Leaf' => $leaf,
                                          )
                                        );
                        }
                        if (!empty($catarr)) {
                            $results['success'] = $catarr;
                        } else {
                            $msg = 'No parent categories.';
                            $results['msg'] = $msg;
                        }
                    } else {
                        $msg = 'There is something wrong in the configuration';
                        $results['msg'] = __($msg);
                    }
                } else {
                    $msg = 'Please fill all ebay details in configuration';
                    $results['msg'] = __($msg);
                }
            } catch (\Exception $e) {
                $results['msg'] = $e->getMessage();
            }

            return $results;
        }
    }

if (!function_exists('createProSpecsAttr')) {
    function createProSpecsAttr($results, $ebayCategoryName, $store_id)
    {
        if (isset($results->Ack) && $results->Ack == 'Success' && isset($results->Recommendations->NameRecommendation)) {
            $attr = array();
            $store_cat_name = get_term_by('id', $store_id, 'product_cat')->name;

            foreach ($results->Recommendations->NameRecommendation as $eBaySpecification) {
                if (isset($eBaySpecification->ValueRecommendation)) {
                    $dataValInputType = _convertSpecsInArray($eBaySpecification);
                    $values = $dataValInputType['values'];
                    $type = $dataValInputType['type'];
                    $input = $dataValInputType['input'];

                    $attributeCode = str_replace(' ', '_', $eBaySpecification->Name);
                    $attributeCode = preg_replace('/[^A-Za-z0-9\_]/', '', $attributeCode);
                    $wooAttrCode = substr('ebay_'.strtolower($attributeCode), 0, 29);

                    $attrName = $eBaySpecification->Name;
                    $attrName = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $attrName);

                    $attr[] = proccess_add_attribute($values, array('attribute_name' => $wooAttrCode, 'attribute_label' => $attrName.'[For '.$store_cat_name.']', 'attribute_type' => $input, 'attribute_orderby' => 'menu_order', 'attribute_public' => false));
                }
            }

            return $attr;
        }
    }
}

function proccess_add_attribute($val, $attribute)
{
    global $wpdb;

    $table = $wpdb->prefix.'woocommerce_attribute_taxonomies';

    $attribute_id = $wpdb->get_row('select attribute_id from '.$table.' where attribute_name="'.$attribute['attribute_name'].'"');

    if (empty($attribute_id)) {
        $insert = $wpdb->insert(
            $table,
            array(
              'attribute_label' => $attribute['attribute_label'],
              'attribute_name' => $attribute['attribute_name'],
              'attribute_type' => $attribute['attribute_type'],
              'attribute_orderby' => $attribute['attribute_orderby'],
              'attribute_public' => isset($attribute['attribute_public']) && true === $attribute['attribute_public'] ? 1 : 0,
            ),
            array('%s', '%s', '%s', '%s', '%d')
          );

        // Checks for an error in the product creation.
        if (is_wp_error($insert)) {
            throw new WC_API_Exception('woocommerce_api_cannot_create_product_attribute', $insert->get_error_message(), 400);
        }

        $attr_id = $wpdb->insert_id;

        // Add new taxonomy, NOT hierarchical (like tags)
        $labels = array(
            'name' => _x($attribute['attribute_label'], $attribute['attribute_label'], 'wk'),
            'singular_name' => _x($attribute['attribute_label'], $attribute['attribute_label'].' singular name', 'wk'),
            'search_items' => __('Search'.$attribute['attribute_label'], 'wk'),
            'popular_items' => __('Popular'.$attribute['attribute_label'], 'wk'),
            'all_items' => __('All'.$attribute['attribute_label'], 'wk'),
            'parent_item' => null,
            'parent_item_colon' => null,
            'edit_item' => __('Edit'.$attribute['attribute_label'], 'wk'),
            'update_item' => __('Update'.$attribute['attribute_label'], 'wk'),
            'add_new_item' => __('Add New '.$attribute['attribute_label'], 'wk'),
            'new_item_name' => __('New '.$attribute['attribute_label'].' Name', 'wk'),
            'separate_items_with_commas' => __('Separate '.$attribute['attribute_label'].' with commas', 'wk'),
            'add_or_remove_items' => __('Add or remove'.$attribute['attribute_label'], 'wk'),
            'choose_from_most_used' => __('Choose from the most used'.$attribute['attribute_label'], 'wk'),
            'not_found' => __('No '.$attribute['attribute_label'].' found.', 'wk'),
            'menu_name' => __($attribute['attribute_label'], 'wk'),
          );

        $args = array(
            'hierarchical' => false,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array('slug' => 'pa_'.$attribute['attribute_name']),
          );

        register_taxonomy('pa_'.$attribute['attribute_name'], 'product', $args);

        $taxonomy = 'pa_'.$attribute['attribute_name'];

        foreach ($val as $v) {
            $args = array();

            $args['slug'] = sanitize_title(wp_unslash($v));

            $term = wp_insert_term($v, $taxonomy, $args);

            // Checks for an error in the term creation.
            if (is_wp_error($term)) {
                return $term->get_error_message();
            }

            $id = $term['term_id'];

            do_action('woocommerce_api_create_product_attribute_term', $id, $v);
        }

        // Clear transients.

        wp_schedule_single_event(time(), 'woocommerce_flush_rewrite_rules');

        delete_transient('wc_attribute_taxonomies');
    } else {
        $attr_id = $attribute_id->attribute_id;
    }

    return $attr_id;
}

/**
 * @param array|object $conditionValues
 *
 * @return array
 */
function _convertSpecsInArray($eBaySpecification)
{
    $values = [];
    $type = 'int';
    $input = 'select';

    if (is_array($eBaySpecification->ValueRecommendation)) {
        foreach ($eBaySpecification->ValueRecommendation as $key => $options) {
            $values[$key] = $options->Value;
        }
    } else {
        $type = 'varchar';
        $input = 'text';
    }

    return ['type' => $type, 'input' => $input, 'values' => $values];
}

if (!function_exists('ebay_import_products_from_api')) {
    function ebay_import_products_from_api()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $settings = new Categories();

            $account_id = $_POST['ebay_accid'];

            $start_date = $_POST['start_date'];

            $time = date_create($start_date)->getTimestamp();

            $tMicro = sprintf('%03d', ($time - floor($time)) * 1000);

            $start_date = gmdate('Y-m-d\TH:i:s.', $time).$tMicro.'Z';

            $end_date = $_POST['end_date'];

            $time = date_create($end_date)->getTimestamp();

            $tMicro = sprintf('%03d', ($time - floor($time)) * 1000);

            $end_date = gmdate('Y-m-d\TH:i:s.', $time).$tMicro.'Z';

            if (empty($start_date) || empty($end_date) || !($start_date < $end_date)) {
                wp_send_json(array());
                die;
            }

            if (date_diff(date_create($start_date), date_create($end_date))->format('%R%a') > 120) {
                wp_send_json(array());
                die;
            }

            $date_diff = date_diff(date_create($start_date), date_create($end_date))->format('%R%a day');

            $data = $settings->ebay_conf($account_id);

            $eBayConfig = $data['eBayConfig'];

            $client = $data['client'];

            $ebayItemObject = array();

            if ($client) {
                $pagenumber = $_POST['page_number'];

                $params = [
                    'Version' => 849, //version
                    'IncludeVariations' => true,
                    'UserID' => $eBayConfig['ebayuserid'],
                    'DetailLevel' => 'ReturnAll',
                    'Pagination' => [
                        'EntriesPerPage' => '100',
                        'PageNumber' => $pagenumber,
                    ],
                    'StartTimeFrom' => $start_date,
                    'StartTimeTo' => $end_date,
                ];

                $results = $client->GetSellerList($params);

                if (isset($results->ItemArray->Item)) {
                    $ebayItemObject = $results->ItemArray->Item;
                }
                wp_send_json($ebayItemObject);
                die;
            }
            die;
        }
    }
}

if (!function_exists('ebay_import_products_raw_data')) {
    function ebay_import_products_raw_data()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $raw_data = $_POST['raw_data'];

            $res = Products::wc_ebay_insert_data_in_bulk($raw_data);

            wp_send_json($res);

            wp_die;
        }
    }
}

if (!function_exists('get_ebay_import_products_api_data')) {
    function get_ebay_import_products_api_data()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $result = Products::wc_ebay_get_api_raw_data();

            wp_send_json($result);

            wp_die;
        }
    }
}

if (!function_exists('ebay_import_products')) {
    function ebay_import_products()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $raw_product = json_decode(stripslashes($_POST['raw_data']));

            $result = Products::import_products($raw_product);

            wp_send_json($result);

            die;
        }
    }
}

if (!function_exists('ebay_import_orders_from_api')) {
    function ebay_import_orders_from_api()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $orders = new Orders();

            $settings = new Categories();

            $account_id = $_POST['ebay_accid'];

            $start_date = $_POST['start_date'];

            $time = date_create($start_date)->getTimestamp();

            $tMicro = sprintf('%03d', ($time - floor($time)) * 1000);

            $start_date = gmdate('Y-m-d\TH:i:s', $time);

            $end_date = $_POST['end_date'];

            $time = date_create($end_date)->getTimestamp();

            $tMicro = sprintf('%03d', ($time - floor($time)) * 1000);

            $end_date = gmdate('Y-m-d\TH:i:s', $time);

            if (empty($start_date) || empty($end_date) || !($start_date < $end_date)) {
                wp_send_json(array());
                die;
            }

            if (date_diff(date_create($start_date), date_create($end_date))->format('%R%a') > 120) {
                wp_send_json(array());
                die;
            }

            $date_diff = date_diff(date_create($start_date), date_create($end_date))->format('%R%a day');

            $data = $settings->ebay_conf($account_id);
            $eBayConfig = $data['eBayConfig'];
            $client = $data['client'];
            $res = '';
            if ($client) {
                $dt = new \DateTime();
                $currentDate = $dt->format('Y-m-d\TH:i:s');
                $dt->modify('-500 day');
                $endTime = $dt->format('Y-m-d\TH:i:s');

                $pageNumber = 0;
                //$items=array();
                $responce = [];
                $errorMsg = '<br/>';
                $tmpWholeData = [];
                do {
                    $pagenumber = $pageNumber ? $pageNumber + 1 : 1;
                    /****/
                    $params = ['Version' => 891,
                              'DetailLevel' => 'ReturnAll',
                              'Pagination' => [
                                  'EntriesPerPage' => '100',
                                  'PageNumber' => ($pagenumber ? $pagenumber : 1),
                              ],
                              'CreateTimeFrom' => $start_date,
                              'CreateTimeTo' => $end_date,
                              'OrderStatus' => 'Completed',
                          ];

                    $count = 0;
                    $results = $client->GetOrders($params);

                    if (isset($results->OrderArray->Order)) {
                        $eBayOrders = json_decode(json_encode($results->OrderArray->Order), true);
                        $eBayOrders = isset($eBayOrders[0]) ? $eBayOrders : [0 => $eBayOrders];
                        $res = $orders->save_raw_orders($eBayOrders);
                        if ($res == 1) {
                            $res = __('All Orders Imported successfully!', 'wc_ebay');
                        } else {
                            $res = __('All Orders are already imported from ebay!', 'wc_ebay');
                        }
                        $response['error_msg'] = false;
                        $response['data'] = $res;
                        $pageNumber = (int) $results->PageNumber;
                    } else if($results->Ack == 'Failure') {
                        $response = [
                              'data' => $notifications['items'],
                              'error_msg' => true,
                              ];
                    } else {
                        $response = [
                              'data' => 'No Order found',
                              'error_msg' => false,
                              'msg' => 'No Order found',
                              ];
                    }
                } while ($results->ReturnedOrderCountActual == 100);
            }

            wp_send_json($response);

            die;
        }
    }
}

if (!function_exists('ebay_import_orders')) {
    function ebay_import_orders()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $orders = new Orders();

            $raw_orders = json_decode(stripslashes($_POST['raw_data']));

            $result = $orders->save_orders($raw_orders);

            wp_send_json($result);

            die;
        }
    }
}

if (!function_exists('get_ebay_import_orders_api_data')) {
    function get_ebay_import_orders_api_data()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $result = Orders::wc_ebay_get_api_raw_data();

            wp_send_json($result);

            wp_die;
        }
    }
}

if (!function_exists('import_message_from_ebay')) {
    function import_message_from_ebay()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $account_id = $_POST['ebay_accid'] ?? 0;
            $result = Messenger::wc_ebay_get_api_raw_data($account_id);

            wp_send_json($result);

            wp_die;
        }
    }
}
if (!function_exists('get_message_ebay')) {
    function get_message_ebay()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $id = $_POST['id'];
            $result = Messenger::get_message_ebay($id);

            wp_send_json($result);

            wp_die;
        }
    }
}
if (!function_exists('reply_message_ebay')) {
    function reply_message_ebay()
    {
        if (check_ajax_referer('api-ajaxnonce', 'nonce', false)) {
            $id = $_POST['id'];
            $content = $_POST['content'];
            $result = Messenger::reply_message_ebay($id, $content);

            wp_send_json($result);

            wp_die;
        }
    }
}



if (!function_exists('wc_ebay_manage_account')) {
    function wc_ebay_manage_account()
    {
        if (isset($_GET['page']) && $_GET['page'] == 'manage-account' && isset($_GET['action']) && $_GET['action'] == 'account' && !empty($_GET['aid'])) {
            require_once EBAY_DIR.'/templates/ebay-tabs.php';
        } elseif (isset($_GET['page']) && $_GET['page'] == 'manage-account' && isset($_GET['action']) && $_GET['action'] == 'add') {
            require_once EBAY_DIR.'/templates/ebay-tabs.php';
        } else {
            require_once EBAY_DIR.'/templates/account-list.php';
        }
    }
}

function ebay_condition_id_meta_box($post_type)
{
    global $post;
    $post_types = array('product');     //limit meta box to certain post types
    $product = wc_get_product($post->ID);
    if (in_array($post_type, $post_types)) {
        add_meta_box(
          'ebay_conditionID', __('eBay Condition Id', 'wc_ebay'), 'ebay_condition_id_meta_box_content', $post_type, 'side', 'high'
      );
    }
}

function ebay_condition_id_meta_box_content($post)
{
    global $wpdb;

    $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

    $condition_data = $cat_id = array();

    $terms = get_the_terms($post->ID, 'product_cat');

    if (is_array($terms)) {
        foreach ($terms as $term) {
            array_push($cat_id, $term->term_id);
        }
    }
    if ($cat_id) {
        foreach ($cat_id as $c_id) {
            $id = $wpdb->get_row("SELECT category_condition FROM $category_map_table WHERE category_id='$c_id'");

            if (!empty($id)) {
                $condition_data = $id->category_condition;
                break;
            }
        }
    }

    if ($condition_data) {
        $condID = get_post_meta($post->ID, 'ebay_condition_id', true);
        wp_nonce_field('ebay_condition_id_meta_box_nonce', 'ebay_condition_id_nonce');
        echo '<select name="ebay_condition_id" class="widefat" id="ebay_condition_id_select"><option value="">--Select--</option>';
        foreach (maybe_unserialize($condition_data) as $key => $value) {
            $selected = $value['id'] == $condID ? 'selected' : '';
            echo "<option value={$value['id']} {$selected}>{$value['name']}</option>";
        }
        echo '</select>';
    } else {
        echo __('No mapped category selected.', 'wc_ebay');
    }
}

function ebay_condition_id_meta_box_save($post_id)
{
    if (!isset($_POST['ebay_condition_id_nonce']) || !wp_verify_nonce($_POST['ebay_condition_id_nonce'], 'ebay_condition_id_meta_box_nonce')) {
        return;
    }

    if (isset($_POST['ebay_condition_id']) && $_POST['ebay_condition_id']) {
        update_post_meta($post_id, 'ebay_condition_id', $_POST['ebay_condition_id']);
    } else {
        update_post_meta($post_id, 'ebay_condition_id', '');
    }
}

// Customize function.
if ( ! function_exists( 'ebay_send_slack' ) ) {
    // Customize Register action.
    add_action( 'ebay_send_slack', 'ebay_send_slack', null, 2 );

    /**
     * Customize: Panel.
     *
     * Customize function.
     *
     * @param  object WP_Customize  Instance of the WP_Customize_Manager class.
     * @since  1.0.0
     */
    function ebay_send_slack( $msg, $type = 'info' ) {
        $curl = new Ebay\Curl();
        $url = get_option('ebay_slack_info_url');
        $icon = 'collision';
        $type = strtolower($type);
        if ($type == 'warning') {
            $icon = 'warning';
            $url = get_option('ebay_slack_error_url');
        } elseif ($type == 'failure' || $type == 'error') {
            $icon = 'bangbang';
            $url = get_option('ebay_slack_error_url');
        }
        if ($url) {
            $curl->to($url)
                ->withHeader('Content-type', 'application/json')
                ->withData(json_encode(['text' => ":" . $icon . ": " . $msg]))
                ->post();
        }
    }
}

// Customize function.
if ( ! function_exists( 'ebay_export_product_to_random_account' ) ) {
    // Customize Register action.
    add_action( 'ebay_export_product_to_random_account', 'ebay_export_product_to_random_account' );

    /**
     * Customize: Panel.
     *
     * Customize function.
     *
     * @param  object WP_Customize  Instance of the WP_Customize_Manager class.
     * @since  1.0.0
     */
    function ebay_export_product_to_random_account( $args ) {
        global $wpdb;
        $product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';
        $wpdb->insert(
            $product_map_table,
            array(
                'ebay_product_id' => time(),
                'ebay_product_name' => 'Ahihi',
                'product_type' => 'tshirt',
                'store_product_id' => time(),
                'store_category' => 'No Category',
            )
        );
    }
}

// Customize function.
if ( ! function_exists( 'do_update_ebay_account_status_auto' ) ) {
    add_action( 'update_ebay_account_status_auto', 'do_update_ebay_account_status_auto' );
    function do_update_ebay_account_status_auto($id) {
        global $wpdb;
        $table = $wpdb->prefix.'woocommerce_ebay_account';
        $wpdb->update(
            $table,
            array(
                'status' => 0,
                'update_status_at' => time()
            ),
            array('id' => $id)
        );
    }
}
if ( ! function_exists( 'do_update_ebay_account_status_pending' ) ) {
    add_action( 'update_ebay_account_status_pending', 'do_update_ebay_account_status_pending' );
    function do_update_ebay_account_status_pending($id) {
        global $wpdb;
        $table = $wpdb->prefix.'woocommerce_ebay_account';
        $wpdb->update(
            $table,
            array(
                'status' => 1,
                'update_status_at' => time()
            ),
            array('id' => $id)
        );
    }
}
if ( ! function_exists( 'do_update_ebay_account_status_full' ) ) {
    add_action( 'update_ebay_account_status_full', 'do_update_ebay_account_status_full' );
    function do_update_ebay_account_status_full($id) {
        global $wpdb;
        $table = $wpdb->prefix.'woocommerce_ebay_account';
        $wpdb->update(
            $table,
            array(
                'status' => 2,
                'update_status_at' => time()
            ),
            array('id' => $id)
        );
    }
}

if ( ! function_exists( 'do_update_ebay_account_status_msg' ) ) {
    add_action( 'update_ebay_account_status_msg', 'do_update_ebay_account_status_msg', 10, 2 );
    function do_update_ebay_account_status_msg($id, $msg) {
        global $wpdb;
        $table = $wpdb->prefix.'woocommerce_ebay_account';
        $wpdb->update(
            $table,
            array(
                'status_msg' => $msg
            ),
            array('id' => $id)
        );
    }
}


if ( ! function_exists( 'do_update_ebay_account_cron_at' ) ) {
    add_action( 'update_ebay_account_cron_at', 'do_update_ebay_account_cron_at' );
    function do_update_ebay_account_cron_at($id) {
        global $wpdb;
        $table = $wpdb->prefix.'woocommerce_ebay_account';
        $wpdb->update(
            $table,
            array(
                'cron_at' => time()
            ),
            array('id' => $id)
        );
    }
}

if ( ! function_exists( 'do_update_ebay_account_fetch_chat_at' ) ) {
    add_action( 'update_ebay_account_fetch_chat_at', 'do_update_ebay_account_fetch_chat_at' );
    function do_update_ebay_account_fetch_chat_at($id) {
        global $wpdb;
        $table = $wpdb->prefix.'woocommerce_ebay_account';
        $wpdb->update(
            $table,
            array(
                'fetch_chat_at' => time()
            ),
            array('id' => $id)
        );
    }
}


// Customize function.
if ( ! function_exists( 'force_upgrade_db' ) ) {
    // Customize Register action.
    add_action( 'force_upgrade_db', 'force_upgrade_db' );

    /**
     * Customize: Panel.
     *
     * Customize function.
     *
     * @param  object WP_Customize  Instance of the WP_Customize_Manager class.
     * @since  1.0.0
     */
    function force_upgrade_db() {
        global $wpdb;
        $table_name = $wpdb->prefix.'woocommerce_ebay_account';
        $queryStr = "show columns from $table_name;";
        $results = $wpdb->get_results($queryStr);
        $check = array();
        foreach ($results as $key => $result) {
            if ($result->Field == 'status' || $result->Field == 'fetch_chat_at' || $result->Field == 'update_status_at' || $result->Field == 'cron_at' || $result->Field == 'status_msg') {
                $check[] = 1;
            }
        }
        if (!count($check)) return true;
        $charset_collate = $wpdb->get_charset_collate();
        $sql = "CREATE TABLE {$wpdb->prefix}woocommerce_ebay_account (
            id bigint(20) NOT NULL auto_increment,
            ebay_global_site_id bigint(20),
            ebay_userid varchar(50),
            store_name varchar(50),
            access_token longtext,
            dev_id varchar(100),
            app_id varchar(100),
            cer_id varchar(100),
            postal_code varchar(100),
            ebay_paypal longtext,
            status tinyint,
            fetch_chat_at bigint(20),
            update_status_at bigint(20),
            cron_at bigint(20),
            status_msg longtext,
            UNIQUE KEY id (id)
            ) $charset_collate;";
        require_once ABSPATH.'wp-admin/includes/upgrade.php';
            if (!wp_next_scheduled('update_products')) {
                wp_schedule_event(time(), 'time_interval', 'update_products');
            }
        dbDelta($sql);
        // var_dump($check);die;
        // var_dump($columns);die;
    }
    
    do_action('force_upgrade_db');
}




?>
