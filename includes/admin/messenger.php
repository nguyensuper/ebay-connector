<?php
/**
 * @author     Lcn
 */
class Messenger
{
    public $wc_product_id;

    public function __construct()
    {
    }
    
    public function reply_message_ebay($id, $content) {
        global $wpdb;
        $table_name = $wpdb->prefix.'woocommerce_ebay_messenger';
        $query = "SELECT * FROM $table_name where id = $id";
        $message = $wpdb->get_results($query)[0];
        
        $account_id = $message->account_id;
        $settings = new Categories();
        $config = $settings->ebay_conf($account_id);
        $client = $config['client'];
        $params = [
            'ErrorLanguage' => 'en_US',
            'WarningLevel' => 'High',
            'ItemID' => $message->ebay_item_id,
            'MemberMessage' => [
                'Body' => $content,
                'ParentMessageID' => $message->ebay_external_message_id
            ]
        ];
        $results = $client->AddMemberMessageRTQ($params);
        if (isset($results->Ack) && ($results->Ack == 'Success' || $results->Ack == 'Warning')) {
            $result = $wpdb->update(
                    $table_name,
                    array(
                        'status' => 2
                    ), array(
                      'id' => $id,
                    ));
            return ['Ack' => 'Success'];
        } else {
            $msg = [];
            if (isset($results->Errors) && is_array($results->Errors)) {
                foreach ($results->Errors as $err) {
                    if (isset($err->SeverityCode) && $err->SeverityCode == 'Error') {
                        $msg[] = '<b>'.$product_title.' - </b>'.$err->LongMessage;
                    }
                }
            } elseif (isset($results->Errors)) {
                if (isset($results->Errors->SeverityCode) && $results->Errors->SeverityCode == 'Error') {
                    $msg[] = '<b>'.$product_title.' - </b>'.$results->Errors->LongMessage;
                }
            }
            return ['Ack' => 'Error', 'msg' => implode(', ', $msg)];
        }
    }
    
    public static function get_message_ebay($id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix.'woocommerce_ebay_messenger';
        $query = "SELECT * FROM $table_name where id = $id";
        $res = $wpdb->get_results($query)[0];
        
        $content = $res->ebay_messenger_content;
        $content = str_replace("<a", "<span", $content);
        $content = str_replace("</a", "</span", $content);
        $content = str_replace("\n", "", $content);
        $content = str_replace('<span style="padding: 0px 10px">Reply</span>', '', $content);
        $content = str_replace('<span style="padding: 0px 10px">View order details</span>', '', $content);
        $res->ebay_messenger_content = $content;
        if ($res->status == 0) {
            $result = $wpdb->update(
                    $table_name,
                    array(
                        'status' => 1
                    ), array(
                      'id' => $id,
                    ));
        }
        return $res;
    }

    public static function wc_ebay_get_api_raw_data($account_id)
    {
        // $this->makeTimeInt()
        global $wpdb;
        $table_name = $wpdb->prefix.'woocommerce_ebay_messenger';
        $account_id = !$account_id && isset($_GET['aid']) ? $_GET['aid'] : 0;
        
        $table_acc_name = $wpdb->prefix.'woocommerce_ebay_account';
        
        $queryAcc = "SELECT * FROM $table_acc_name where (status = 0 or status is null)";
        
        if ($account_id) {
            $queryAcc .= " and id = $account_id";
        }

        $accounts = $wpdb->get_results($queryAcc);
        
        if (!count($accounts)) {
            return ['error' => [['Ack' => 'error', 'msg' => 'No active account found!']]];
        }
        $error = [];
        $response = ['count' => 0, 'error' => $error];
        foreach ($accounts as $key => $account) {
            $account_id = $account->id;
        
            $settings = new Categories();
            $config = $settings->ebay_conf($account_id);
            $client = $config['client'];
            
            $dt = new \DateTime();
            $currentDate = $dt->format('Y-m-d\TH:i:s\Z');

            // $startTime = $dt->format('Y-m-d\TH:i:s\Z');
            // update_option( 'last_fech_setting', 0, true );

            $startTime = $account->fetch_chat_at ?? 0;
            
            if ($startTime) {
                $dt = new \DateTime();
                $dt->setTimestamp($startTime);
                $startTime = $dt->format('Y-m-d\TH:i:s\Z');
            }
            
            $params = [
                'EndTime' => $currentDate,
                'ErrorLanguage' => 'en_US',
                'WarningLevel' => 'High',
                // 'DetailLevel' => 'ReturnMessages'
                'DetailLevel' => 'ReturnHeaders'
            ];
            if ($startTime) {
                $params['StartTime'] = $startTime;
            }
            $errCode = [];
            $messengers = $client->GetMyMessages($params);
            if ($messengers->Ack != 'Success') {
                if (isset($messengers->Errors)) {
                    $errors = is_array($messengers->Errors) ? $messengers->Errors : [$messengers->Errors];
                    foreach ($errors as $err) {
                        if (isset($err->SeverityCode)) {
                            $type = $err->SeverityCode == 'Error' ? 'error' : 'warning';
                            $error[] = ['Ack' => $type, 'msg' => $account->store_name . " - " . $err->LongMessage];
                            do_action( 'ebay_send_slack',
                                      $account->store_name . " - " . $err->LongMessage,
                                      $err->SeverityCode);
                        }
                        if (isset($err->ErrorCode)) {
                            $errCode[$err->ErrorCode] = ['type' => $err->SeverityCode, 'msg' => $err->LongMessage . "(Fetching messenger)"];
                        }
                    }
                }
            }
            if (isset($messengers->Messages->Message)) {
                $arrIds = [];
                $count = 0;
                $mess = is_array($messengers->Messages->Message) ? $messengers->Messages->Message : [$messengers->Messages->Message];
                foreach ($mess as $key => $message) {
                    $mId = trim((string) $message->MessageID);
                    $query = "SELECT count(1) as 'count' FROM $table_name where ebay_messenger_id = $mId";
                    $res = (int) $wpdb->get_results($query)[0]->count ?? 0;
                    if ($res) continue;
                    $arrIds[(int) ($count / 10)][] = (string) $message->MessageID ?? '';
                    $count++;
                }
                foreach ($arrIds as $key => $ids) {
                    $par = $params;
                    $par['DetailLevel'] = 'ReturnMessages';
                    foreach ($ids as $key => $id) {
                        $par['MessageIDs']['MessageID'][] = trim($id);   
                    }
                    $messages       = $client->GetMyMessages($par);
                    if ($messages->Ack != 'Success') {
                        if (isset($messages->Errors)) {
                            $errors = is_array($messages->Errors) ? $messages->Errors : [$messages->Errors];
                            foreach ($errors as $err) {
                                if (isset($err->SeverityCode)) {
                                    $type = $err->SeverityCode == 'Error' ? 'error' : 'warning';
                                    $error[] = ['Ack' => $type, 'msg' => $account->store_name . " - " . $err->LongMessage];
                                    do_action( 'ebay_send_slack',
                                      $account->store_name . " - " . $err->LongMessage,
                                      $err->SeverityCode);
                                }
                                if (isset($err->ErrorCode)) {
                                    $errCode[$err->ErrorCode] = ['type' => $err->SeverityCode, 'msg' => $err->LongMessage . "(Fetching messenger)"];
                                }
                            }
                        }
                    }
                    if (!isset($messages->Messages->Message)) {
                        continue;
                    }
                    $mess = is_array($messages->Messages->Message) ? $messages->Messages->Message : [$messages->Messages->Message];
                    foreach ($mess as $key => $message) {
                        $status = 0;
                        if (((string) $message->Read ?? '') == 'true') {
                            $status = 1;
                        }
                        if (((string) $message->Replied ?? '') == 'true') {
                            $status = 2;
                        }
                        $dt = new \DateTime();
                        $currentDate = $dt->format('d-m-Y H:i:s');
                        $dt2 = new \DateTime();
                        $receiveDate = explode('.', (string) $message->ReceiveDate ?? '')[0] ?? 0;
                        $date2 = $dt2->createFromFormat('Y-m-d\TH:i:s', $receiveDate);
                        $dt3 = new \DateTime();
                        $expirationDate = explode('.', (string) $message->ReceiveDate ?? '')[0] ?? 0;
                        $date3 = $dt3->createFromFormat('Y-m-d\TH:i:s', $expirationDate);
                        $dataInsert = [
                            'ebay_sender' => (string) $message->Sender ?? '',
                            'ebay_messenger_id' => (string) $message->MessageID ?? '',
                            'ebay_external_message_id' => (string) $message->ExternalMessageID ?? '',
                            'ebay_messenger_subject' => (string) trim($message->Subject ?? ''),
                            'ebay_messenger_content' => (string) trim($message->Text ?? ''),
                            'ebay_messenger_type' => (string) $message->MessageType ?? '',
                            'status' => $status,
                            'account_id' => $account_id,
                            "store_product_id"=> 0,
                            'ebay_item_id' => (string) $message->ItemID ?? '',
                            'ebay_messenger_received_at' => $date2 ? strtotime($date2->format('Y/m/d H:i:s')) : 0,
                            'ebay_messenger_expiration_at' => $date3 ? strtotime($date3->format('Y/m/d H:i:s')) : 0,
                            'created_at' => $currentDate
                        ];
                        if($wpdb->insert(
                            $table_name,
                            $dataInsert
                        )) {
                            $response['count'] = $response['count'] + 1;
                        }
                    }
                }
            }
            if (count($errCode)) {
                $action = 'update_ebay_account_status_pending';
                do_action( $action, $account_id );
                $msg = json_encode($errCode);
                do_action( 'update_ebay_account_status_msg', $account_id, $msg );
            } else {
                do_action('update_ebay_account_fetch_chat_at', $account_id);
            }
        }
        $response['error'] = $error;
        if (isset($response['count']) && $response['count'] > 0) {
            do_action( 'ebay_send_slack',
                         $response['count'] . " messages was imported!");
        }
        return $response;
        
    }

}
