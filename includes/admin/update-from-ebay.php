<?php

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WC_Ebay_Map_Products')) {
    class Update_From_Ebay
    {
        public function __construct()
        {
        }

        public function woo_update($product_id, $account_id = 0)
        {
            global $wpdb;
            $account_id = $account_id ? $account_id : $_GET['aid'];

            $settings = new Categories();
            $helper = new Helper();

            $table = $wpdb->prefix.'postmeta';
            $product_raw_table = $wpdb->prefix.'woocommerce_ebay_product_raw_data';
            $product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';

            $ebay_product_id = $this->getEbayId($product_id);

            try {
                $data = $settings->ebay_conf($account_id);

                $eBayConfig = $data['eBayConfig'];
                $client = $data['client'];
                if ($client) {
                    /**for get ebay category condition and variations **/

                    $params = [
                        'Version' => 891,
                        'ErrorLanguage' => 'en_US',
                        'ItemID' => $ebay_product_id,
                    ];

                    $results = $client->GetItem($params);
                    $return_data = array();
                    if (isset($results->Ack) && $results->Ack == 'Success') {
                        $item_data = $results->Item;
                        $data = maybe_unserialize($item_data);
                        $variation_arr['var_rule'] = array();
                        $variation_arr['var_price'] = array();

                        $sku_id = $item_data->ItemID;

                        if ($product_id != null) {
                            $data = json_decode(json_encode($data), true);

                            $ebayCatId = $data['PrimaryCategory']['CategoryID'];

                            $ebayCatName = explode(':', $data['PrimaryCategory']['CategoryName']);

                            $ebayCatName = $ebayCatName[count($ebayCatName) - 1];
                            $catMapData = array();
                            if (isset($data['Storefront']) && $data['Storefront']) {
                                unset($data['Storefront']['StoreURL']);
                                foreach ($data['Storefront'] as $key => $value) {
                                    $catMapDataID = $helper->getStoreBayCatMapData($value);
                                    if ($catMapDataID) {
                                        $catMapData[] = $catMapDataID;
                                    }
                                }
                            }
                            $eBayWcId = $catMapData ? $catMapData : $helper->getWcCatId();

                            /**get Weight of product **/

                            $weight = 0;

                            if (is_array($data['ShippingDetails']['CalculatedShippingRate'])) {
                                foreach ($data['ShippingDetails']['CalculatedShippingRate'] as $weightData) {
                                    if (isset($weightData['unit']) && $weightData['unit'] == 'lbs') {
                                        $weight = $weightData['_'];
                                    }
                                }
                            }

                            $weight = $weight ? $weight : 1;

                            /* get product variation**/
                            if (isset($data['Variations']) && count($data['Variations']) > 0) {
                                $superAttIds = [];

                                foreach ($data['Variations']['Variation'] as $variation) {
                                    $attributeData = [];
                                    if (isset($variation['VariationSpecifics']['NameValueList']) && isset($variation['VariationSpecifics']['NameValueList'][0])) {
                                        foreach ($variation['VariationSpecifics']['NameValueList'] as $nameValue) {
                                            $attributeCode = str_replace(
                                                    ' ',
                                                    '_',
                                                    $nameValue['Name']
                                                );
                                            $attributeCode = preg_replace(
                                                    '/[^A-Za-z0-9\_]/',
                                                    '',
                                                    $attributeCode
                                                );
                                            $mageAttrCode = substr('conf_'.strtolower($attributeCode), 0, 30);
                                            array_push($superAttIds, $mageAttrCode);
                                        }
                                    } else {
                                        if (
                                                isset($variation['VariationSpecifics']['NameValueList'])
                                                && isset($variation['VariationSpecifics']['NameValueList']['Name'])
                                            ) {
                                            $attributeCode = str_replace(
                                                    ' ',
                                                    '_',
                                                    $variation['VariationSpecifics']['NameValueList']['Name']
                                                );
                                            $attributeCode = preg_replace(
                                                    '/[^A-Za-z0-9\_]/',
                                                    '',
                                                    $attributeCode
                                                );
                                            $mageAttrCode = substr('conf_'.strtolower($attributeCode), 0, 30);
                                            array_push(
                                                    $superAttIds,
                                                    $mageAttrCode
                                                );
                                        }
                                    }
                                    break;
                                }
                                // if (isset($data['SKU'])) {
                                //     $mainSKU = $data['SKU'];
                                // } else {
                                $mainSKU = $data['ItemID'];
                                // }

                                $mainSKU = strtolower(preg_replace('/\s+/', '-', $mainSKU));

                                $wholedata = [
                                        'type_id' => 'variable',
                                        'supperattr' => $superAttIds,
                                        'status' => 1,
                                        'category' => $eBayWcId,
                                        'name' => $data['Title'],
                                        'short_description' => ' ',
                                        'sku' => $mainSKU,
                                        'price' => $data['SellingStatus']['CurrentPrice']['_'],
                                        'currency_id' => $data['SellingStatus']['CurrentPrice']['currencyID'],
                                        'is_in_stock' => 1,
                                        'tax_class_id' => 0,
                                        'weight' => $weight,
                                    ];
                                /* Assigne values to store product according to eBay product**/

                                $count = 0;
                                /* Variations of product **/
                                $associatePro = [];
                                if (isset($data['Variations']['Variation'][0]) === false) {
                                    $data['Variations']['Variation'] = [0 => $data['Variations']['Variation']];
                                }

                                foreach ($data['Variations']['Variation'] as $key => $variation) {
                                    ++$count;
                                    $subt = '';
                                    $attributeData = [];

                                    if (
                                            isset($variation['VariationSpecifics']['NameValueList'])
                                            && isset($variation['VariationSpecifics']['NameValueList'][0])
                                        ) {
                                        foreach ($variation['VariationSpecifics']['NameValueList'] as $nameValue) {
                                            if ($nameValue['Value'] == '') {
                                                continue;
                                            }
                                            $attributeCode = str_replace(
                                                    ' ',
                                                    '_',
                                                    $nameValue['Name']
                                                );
                                            $attributeCode = preg_replace(
                                                    '/[^A-Za-z0-9\_]/',
                                                    '',
                                                    $attributeCode
                                                );
                                            $mageAttrCode = substr('conf_'.strtolower($attributeCode), 0, 30);
                                            $attributeData[$mageAttrCode] = $nameValue['Value'];
                                        }
                                    } else {
                                        if (
                                                isset($variation['VariationSpecifics'])
                                                && isset($variation['VariationSpecifics']['NameValueList'])
                                                && $variation['VariationSpecifics']['NameValueList']['Value'] == ''
                                            ) {
                                            continue;
                                        }
                                        if (
                                                isset($variation['VariationSpecifics'])
                                                && isset($variation['VariationSpecifics']['NameValueList'])
                                                && $variation['VariationSpecifics']['NameValueList']['Name']
                                            ) {
                                            $attributeCode = str_replace(
                                                    ' ',
                                                    '_',
                                                    $variation['VariationSpecifics']['NameValueList']['Name']
                                                );
                                            $attributeCode = preg_replace(
                                                    '/[^A-Za-z0-9\_]/',
                                                    '',
                                                    $attributeCode
                                                );
                                            $mageAttrCode = substr('conf_'.strtolower($attributeCode), 0, 30);
                                            $attributeData[$mageAttrCode] = $variation['VariationSpecifics']['NameValueList']['Value'];
                                        }
                                    }

                                    if (
                                            isset($variation['StartPrice'])
                                            && isset($variation['StartPrice']['_'])
                                        ) {
                                        $itemPrice = $variation['StartPrice']['_'];
                                        $currencyId = $variation['StartPrice']['currencyID'];
                                    } else {
                                        $itemPrice = $variation['_'];
                                        $currencyId = $variation['currencyID'];
                                    }
                                    if (isset($variation['SKU'])) {
                                        $associateSku = strtolower(preg_replace('/\s+/', '-', $variation['SKU']));
                                    } else {
                                        $associateSku = $data['ItemID'].'-'.$count;
                                        $associateSku = strtolower(preg_replace('/\s+/', '-', $associateSku));
                                    }

                                    $quictProData = [
                                            'status' => 1,
                                            'sku' => $associateSku,
                                            'price' => $itemPrice,
                                            'currency_id' => $currencyId,
                                            'qty' => (int) $variation['Quantity'] - (int) $variation['SellingStatus']['QuantitySold'],
                                            'is_in_stock' => intval($variation['SellingStatus']['QuantitySold']) > 0 ? 1 : 0,
                                            'tax_class_id' => 0,
                                            'weight' => $weight,
                                            'visibility' => 1,
                                        ];

                                    foreach ($attributeData as $mageAttrCode => $value) {
                                        $quictProData['variation'][$mageAttrCode] = $value;
                                    }

                                    if (count($attributeData) == 0) {
                                        continue;
                                    }

                                    array_push(
                                            $associatePro,
                                            $quictProData
                                        );
                                }

                                $wholedata['assocate_pro'] = $associatePro;
                            } else {
                                if (isset($data['SKU'])) {
                                    $sku = strtolower(preg_replace('/\s+/', '-', $data['SKU']));
                                } else {
                                    $sku = strtolower(preg_replace('/\s+/', '-', $data['ItemID']));
                                }

                                /**For without variation product**/
                                $wholedata = [
                                        'type_id' => 'simple',
                                        'status' => 1,
                                        'producttypecustom' => 'customproductsimple',
                                        'category' => $eBayWcId,
                                        'name' => $data['Title'],
                                        'short_description' => ' ',
                                        'sku' => $sku,
                                        'price' => $data['SellingStatus']['CurrentPrice']['_'],
                                        'currency_id' => $data['SellingStatus']['CurrentPrice']['currencyID'],
                                        'stock' => $data['Quantity'],
                                        'is_in_stock' => 1,
                                        'tax_class_id' => 0,
                                        'weight' => $weight,
                                    ];
                            }

                            $imageArr = array();

                            if (isset($data['PictureDetails']['PictureURL'])) {
                                if (!is_array($data['PictureDetails']['PictureURL'])) {
                                    // if ($i == 0) {
                                    $defaultImage = $data['PictureDetails']['GalleryURL'];
                                    // }
                                    array_push($imageArr, $data['PictureDetails']['PictureURL']);
                                } else {
                                    foreach ($data['PictureDetails']['PictureURL'] as $value) {
                                        // if ($i == 0) {
                                        //     $defaultImage = $value;
                                        // }
                                        $defaultImage = $data['PictureDetails']['GalleryURL'];
                                        array_push($imageArr, $value);
                                    }
                                }

                                $wholedata['image_data'] = [
                                        'default' => $defaultImage,
                                        'images' => $imageArr,
                                    ];
                            }

                            $postid = $product_id;

                            if ($wholedata['type_id'] == 'variable' && !empty($postid)) {
                                $product_title = 'Product #'.$postid.' Variation';

                                $post_name = 'product-'.$postid.'-variation';

                                foreach ($wholedata['assocate_pro'] as $single_pro) {
                                    $var_postid = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $table WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $single_pro['sku']));

                                    if (isset($single_pro['visiblity']) && $single_pro['visiblity'] == 1) {
                                        $visibility = 'visible';
                                    } else {
                                        $visibility = '';
                                    }

                                    if (isset($single_pro['is_in_stock']) && $single_pro['is_in_stock'] == 1) {
                                        $manage_stock = 'yes';
                                    } else {
                                        $manage_stock = 'no';
                                    }
                                    if ($single_pro['qty'] > 0) {
                                        $stock_status = 'instock';
                                    } else {
                                        $stock_status = 'outofstock';
                                    }

                                    update_post_meta($var_postid, '_visibility', $visibility);

                                    add_post_meta($var_postid, '_sku', $single_pro['sku']);

                                    add_post_meta($var_postid, '_featured', 'no');

                                    add_post_meta($var_postid, '_sale_price', '');

                                    add_post_meta($var_postid, '_sale_price_dates_from', '');

                                    add_post_meta($var_postid, '_sale_price_dates_to', '');

                                    add_post_meta($var_postid, '_regular_price', $single_pro['price']);

                                    add_post_meta($var_postid, '_price', $single_pro['price']);

                                    add_post_meta($var_postid, '_downloadable', 'no');

                                    update_post_meta($var_postid, '_backorders', 'no');

                                    update_post_meta($var_postid, '_stock_status', $stock_status);

                                    update_post_meta($var_postid, '_stock', $single_pro['qty']);

                                    update_post_meta($var_postid, '_manage_stock', $manage_stock);

                                    update_post_meta($var_postid, '_virtual', 'no');

                                    update_post_meta($var_postid, '_variation_description', '');

                                    if (isset($single_pro['weight'])) {
                                        update_post_meta($var_postid, '_weight', ('' === $single_pro['weight']) ? '' : wc_format_decimal($single_pro['weight']));
                                    }

                                    update_post_meta($var_postid, '_length', '');

                                    update_post_meta($var_postid, '_width', '');

                                    update_post_meta($var_postid, '_height', '');

                                    $wcattributes = $single_pro['variation'];

                                    if (!empty($wcattributes)) {
                                        foreach ($wcattributes as $wcattribute_key => $wcattribute_value) {
                                            update_post_meta($var_postid, 'attribute_'.$wcattribute_key, $wcattribute_value);

                                            array_push($variation_arr['var_rule'], array('name' => $wcattribute_key, 'value' => $wcattribute_value));
                                        }
                                    }

                                    array_push($variation_arr['var_price'], array('price' => $single_pro['price'], 'var_id' => $var_postid));
                                }
                            }
                            $product_attributes = array();

                            if (!empty($variation_arr['var_rule'])) {
                                foreach ($variation_arr['var_rule'] as $attribute) {
                                    if (empty($attribute['name']) || empty($attribute['value'])) {
                                        continue;
                                    }

                                    $rep_str = $attribute['value'];

                                    $rep_str = preg_replace('/\s+/', ' ', $rep_str);

                                    $attribute['name'] = str_replace(' ', '-', $attribute['name']);

                                    $attribute['value'] = str_replace('|', '|', $rep_str);

                                    $attribute['is_visible'] = 1;

                                    $attribute['is_variation'] = 1;

                                    $attribute['is_taxonomy'] = (int) $attribute['is_taxonomy'];

                                    $product_attributes[str_replace(' ', '-', $attribute['name'])] = $attribute;
                                }
                                if (!empty($product_attributes)) {
                                    update_post_meta($postid, '_product_attributes', $product_attributes);
                                } else {
                                    update_post_meta($postid, '_product_attributes', array());
                                }

                                $variation_arr['var_price'];

                                $price = wp_list_pluck($variation_arr['var_price'], 'price');

                                $min_price = min($price);

                                $min_key = array_search(min($price), $price);

                                $min_variation_id = $variation_arr['var_price'][$min_key]['var_id'];

                                $max_price = max($price);

                                $max_key = array_search(max($price), $price);

                                $max_variation_id = $variation_arr['var_price'][$max_key]['var_id'];

                                update_post_meta($postid, '_min_variation_price', $min_price);
                                update_post_meta($postid, '_min_variation_regular_price', $min_price);
                                update_post_meta($postid, '_max_variation_price', $max_price);
                                update_post_meta($postid, '_max_variation_regular_price', $max_price);
                                update_post_meta($postid, '_min_price_variation_id', $min_variation_id);
                                update_post_meta($postid, '_min_regular_price_variation_id', $min_variation_id);
                                update_post_meta($postid, '_max_price_variation_id', $max_variation_id);
                                update_post_meta($postid, '_max_regular_price_variation_id', $max_variation_id);
                            }

                            if (isset($wholedata['is_in_stock']) && $wholedata['is_in_stock'] == 1) {
                                $manage_stock = 'yes';
                                $stock_status = 'instock';
                            } else {
                                $manage_stock = 'no';
                                $stock_status = 'outofstock';
                            }

                            $visibility = 'visible';

                            update_post_meta($postid, '_visibility', $visibility);

                            add_post_meta($postid, '_sku', $wholedata['sku']);

                            add_post_meta($postid, '_featured', 'no');

                            add_post_meta($postid, '_regular_price', $wholedata['price']);

                            add_post_meta($postid, '_sale_price', '');

                            add_post_meta($postid, '_sale_price_dates_from', '');

                            add_post_meta($postid, '_sale_price_dates_to', '');

                            add_post_meta($postid, '_price', $wholedata['price']);

                            add_post_meta($postid, '_price', $wholedata['price']);

                            add_post_meta($postid, '_downloadable', 'no');

                            update_post_meta($postid, '_backorders', 'no');

                            update_post_meta($postid, '_stock_status', $stock_status);

                            if ($wholedata['type_id'] != 'variable') :

                                    update_post_meta($postid, '_stock', $wholedata['stock']);

                            endif;

                            update_post_meta($postid, '_manage_stock', $manage_stock);

                            update_post_meta($postid, '_virtual', 'no');

                            if (isset($wholedata['weight'])) {
                                update_post_meta($postid, '_weight', ('' === $wholedata['weight']) ? '' : wc_format_decimal($wholedata['weight']));
                            }

                            update_post_meta($postid, '_length', '');

                            update_post_meta($postid, '_width', '');

                            update_post_meta($postid, '_height', '');
                        }

                        $product = wc_get_product($product_id);

                        $product_title = $product->get_title();
                        $return_data = array('Ack' => strtolower($results->Ack), 'msg' => array('<b>'.$product_title.' - </b>'));
                    } else {
                        if (isset($results->Errors) && is_array($results->Errors)) {
                            foreach ($results->Errors as $err) {
                                $msg[] = '<b>'.$product_title.' - </b>'.$err->LongMessage;
                            }

                            $return_data = array('Ack' => strtolower($results->Ack), 'msg' => $msg);
                        } elseif (isset($results->Errors)) {
                            $msg = array('<b>'.$product_title.' - </b>'.$results->Errors->LongMessage);

                            $return_data = array('Ack' => strtolower($results->Ack), 'msg' => $msg);
                        }
                    }

                    return $return_data;
                }
            } catch (\Exception $e) {
                $result['msg'] = $e->getMessage();

                echo $result;
            }
        }

        public function getMappedCategory($cat_id)
        {
            global $wpdb;

            $final_cat_id = '';

            $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

            foreach ($cat_id as $c_id) {
                $id = $wpdb->get_row("SELECT ebay_category_id FROM $category_map_table WHERE category_id='$c_id'");

                if (!empty($id)) {
                    $final_cat_id = $id->ebay_category_id;

                    break;
                }
            }

            return $final_cat_id;
        }

        public function getEbayId($product_id)
        {
            global $wpdb;

            $product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';

            $result = $wpdb->get_var(
                $wpdb->prepare(
                    "Select ebay_product_id from $product_map_table where store_product_id = %d",
                    $product_id
                )
            );

            return $result;
        }

        public function getMappedSpecifics($cat_id)
        {
            global $wpdb;

            $cat_specifics = '';

            $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

            foreach ($cat_id as $c_id) {
                $id = $wpdb->get_row("SELECT category_specification FROM $category_map_table WHERE category_id='$c_id'");

                if (!empty($id)) {
                    $cat_specifics = $id->category_specification;

                    break;
                }
            }

            return $cat_specifics;
        }
    }
}
