<?php
/**
 * @category   Webkul
 *
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
class Orders
{
    public $wc_product_id;

    public function __construct()
    {
    }

    public static function wc_ebay_get_api_raw_data()
    {
        global $wpdb;

        $order_raw_table = $wpdb->prefix.'woocommerce_ebay_order_raw_data';

        $raw_array = array();

        $raw_array = $wpdb->get_results("SELECT * FROM $order_raw_table", ARRAY_A);

        return $raw_array;
    }

    public function save_raw_orders($eBayOrders)
    {
        global $wpdb;

        $order_raw_table = $wpdb->prefix.'woocommerce_ebay_order_raw_data';

        $values = $order_id = $order_id_array = array();

        $place_holders = array();

        $order_id_array = $wpdb->get_results("SELECT order_id FROM $order_raw_table", ARRAY_A);
        $order_final_arr = wp_list_pluck($order_id_array, 'order_id');

        $query = "INSERT INTO $order_raw_table ( order_id, order_data ) VALUES ";

        foreach ($eBayOrders as $key => $eBayOrder) {
            if (isset($eBayOrder['OrderID'])) {
                $order_id = $eBayOrder['OrderID'];
            } else {
                $order_id = 'N/A';
            }

            if ((empty($order_final_arr) || (!empty($order_final_arr) && !in_array($order_id, $order_final_arr))) && in_array($order_id, $eBayOrder)) {
                array_push($values, $order_id);

                array_push($values, maybe_serialize($eBayOrder));

                $place_holders[] = "( '%s', '%s' )";

                $order[] = $order_id;
            }
        }

        if (!empty($values)) {
            $query .= implode(', ', $place_holders);

            $res = $wpdb->query($wpdb->prepare("$query ", $values));
        } else {
            $res = 0;
        }

        return $res;
    }

    public function save_orders($eBayOrders)
    {
        global $wpdb;
        $items = [];
        $counter = $skip_counter = 0;
        $errorMsg = '';
        $table = $wpdb->prefix.'posts';
        foreach ($eBayOrders as $data) {
            $order_id = $data->order_id;

            $order_data = $data->order_data;
            $data = maybe_unserialize($order_data);

            $res = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $table WHERE post_type='shop_order' AND ID='%s'", $order_id));

            if ($res == null) {
                $firstname = 'Guest';
                $lastname = 'User';
                $shipPrice = 0;
                $shipMethod = '';

                $eBayOrder = json_decode(json_encode($data), true);

                if (isset($eBayOrder['ShippingServiceSelected']['ShippingService'])) {
                    $shipMethod = $eBayOrder['ShippingServiceSelected']['ShippingService'];
                    $shipPrice = $eBayOrder['ShippingServiceSelected']['ShippingServiceCost'][_];
                }

                $transactionList = $eBayOrder['TransactionArray']['Transaction'];
                if (!isset($transactionList[0])) {
                    $transactionList = [0 => $transactionList];
                }
                $flagSetName = true;
                $invalidOrder = false;
                $orderItems = [];

                foreach ($transactionList as $transaction) {
                    if (isset($transaction['Buyer']['UserFirstName']) && $flagSetName) {
                        $firstname = $transaction['Buyer']['UserFirstName'];
                        $lastname = $transaction['Buyer']['UserLastName'];
                        $flagSetName = false;
                    }
                    $productId = $this->getWcProduct_Id($transaction['Item']['ItemID']);

                    if (!empty($productId)) {
                        $productId = $productId->store_product_id;
                        if ($this->getWcProduct_Type() == 'variable') {
                            $nameValueList = $transaction['Variation']['VariationSpecifics']['NameValueList'];
                            if (!isset($nameValueList[0])) {
                                $nameValueList = [0 => $nameValueList];
                            }
                            $productId = $helper->getConfAssoProductId($productId, $nameValueList);
                        }

                        if ($productId) {
                            $orderItems[] = [
                                            'product_id' => $productId,
                                            'qty' => $transaction['QuantityPurchased'],
                                            'price' => $transaction['TransactionPrice']['_'],
                                        ];
                        } else {
                            $errorMsg = $errorMsg.' eBay order id : <b>'
                                                .$eBayOrder['OrderID']
                                                ."</b> not sync because Product <b>'"
                                                .$transaction['Item']['Title']
                                                .$transaction['Item']['ItemID']
                                                ."'</b> not Synced on Woocommerce <br />";
                            $invalidOrder = true;
                        }
                    } else {
                        $errorMsg = 'Either product is not Synced on Woocommerce Store or Product is deleted from Ebay';
                        $invalidOrder = true;
                    }
                }
                if ($invalidOrder) {
                    continue;
                }
                /****/
                foreach ($eBayOrder['ShippingAddress'] as $key => $value) {
                    if ($value == '') {
                        $eBayOrder['ShippingAddress'][$key] = __('');
                        $eBayOrder['ShippingAddress']['save_in_address_book'] = 0;
                    }
                }
                if (!isset($eBayOrder['ShippingAddress']['Country'])) {
                    $eBayOrder['ShippingAddress']['Country'] = __('');
                }

                $tempOrder = [
                        'ebay_order_id' => $eBayOrder['OrderID'],
                        'order_status' => $eBayOrder['OrderStatus'],
                        'email' => $eBayOrder['SellerEmail'],
                        'created_date' => $eBayOrder['CreatedTime'],
                        'shipping_address' => [
                                        'firstname' => $firstname,
                                        'lastname' => $lastname,
                                        'street' => $eBayOrder['ShippingAddress']['Street1']."\r\n".$eBayOrder['ShippingAddress']['Street2'],
                                        'city' => $eBayOrder['ShippingAddress']['CityName'],
                                        'country_id' => $eBayOrder['ShippingAddress']['Country'],
                                        'country_name' => $eBayOrder['ShippingAddress']['CountryName'],
                                        'region' => $eBayOrder['ShippingAddress']['StateOrProvince'],
                                        'postcode' => $eBayOrder['ShippingAddress']['PostalCode'],
                                        'telephone' => $eBayOrder['ShippingAddress']['Phone'],
                                        'fax' => $eBayOrder['ShippingAddress']['Phone'],
                                        'vat_id' => '',
                                        'save_in_address_book' => 1, ],
                        'items' => $orderItems,
                        'shipping_service' => ['method' => $shipMethod, 'cost' => $shipPrice],
                        ];

                if ($this->getWcProduct_Type()) {
                    $orderData = $this->create_order($tempOrder);
                }

                ++$counter;
            } else {
                ++$skip_counter;
            }
        }

        return array('new' => $counter, 'skip' => $skip_counter, 'errmsg' => $errorMsg);
    }

    public function getWcProduct_Id($ebay_productId)
    {
        global $wpdb;

        $product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';

        $product_id = $wpdb->get_row("SELECT store_product_id FROM $product_map_table WHERE ebay_product_id='$ebay_productId'");

        if (!empty($product_id)) {
            $this->wc_product_id = $product_id->store_product_id;
        }

        return $product_id;
    }

    public function getWcProduct_Type()
    {
        global $wpdb;

        $product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';

        $product_type = $wpdb->get_row("SELECT product_type FROM $product_map_table WHERE store_product_id='$this->wc_product_id'");

        return $product_type;
    }

    public function create_order($orderData)
    {
        global $wpdb;

        $table_name = $wpdb->prefix.'woocommerce_ebay_order_map';

        if (!$orderData['shipping_address']['street'] || !$orderData['shipping_address']['country_id']) {
            return ['error' => 1, 'msg' => __('order id ').$orderData['ebay_order_id'].__(' not contain address')];
        }
        $exists = email_exists($orderData['email']);
        $customerAddress = array(
            'first_name' => $orderData['shipping_address']['firstname'],
            'last_name' => $orderData['shipping_address']['lastname'],
            'company' => '',
            'email' => $orderData['email'],
            'phone' => $orderData['shipping_address']['telephone'],
            'address_1' => $orderData['shipping_address']['street'],
            'address_2' => $orderData['shipping_address']['street'],
            'city' => $orderData['shipping_address']['city'],
            'state' => $orderData['shipping_address']['region'],
            'postcode' => $orderData['shipping_address']['postcode'],
            'country' => $orderData['shipping_address']['country_name'],
        );
        if (!$exists) {
            $username = explode('@', $orderData['email']);
            $user_id = wc_create_new_customer($orderData['email'], $username[0], $orderData['email']);

            // If we can't find/create an user, it logs an error.
            if (is_wp_error($user_id)) {
                error_log("Failed creating user {$email}.");
            }
            // Prepare the customer address for the order.
        } else {
            $user = get_user_by('email', $orderData['email']);
            $user_id = $user->ID;
        }
        if (strpos($orderData['ebay_order_id'], '-')) {
            $ebay_order_id = substr($orderData['ebay_order_id'], (strpos($orderData['ebay_order_id'], '-') + 1));
        } else {
            $ebay_order_id = $orderData['ebay_order_id'];
        }

        $res = $wpdb->get_row("SELECT ebay_order_id FROM $table_name WHERE ebay_order_id =' $ebay_order_id'");

        if (!$res) {
            $order = wc_create_order(['customer_id' => $user_id]);

            foreach ($orderData['items'] as $item) {
                $myProduct = new WC_Product($item['product_id']);

                if ($myProduct) {
                    $order->add_product($myProduct, $item['qty']);
                }
            }
            if (isset($orderData['shipping_service'])) {
                $item = new WC_Order_Item_Fee();
                $item->set_order_id($order->get_id());
                $item->set_name($orderData['shipping_service']['method']);
                $item->set_total($orderData['shipping_service']['cost']);
                $order->add_item($item);
            }
            $order->set_address($customerAddress, 'billing');
            $order->set_address($customerAddress, 'shipping');
            $order->calculate_totals();
            $order->update_status($orderData['order_status'], 'Imported order', true);

            // Confirm the order payment and add a note to it.
            $order->add_order_note('Order automatically imported from Ebay Api.');
            $order->payment_complete();

            $order_id = $order->id;

            $result = $wpdb->insert(
                    $table_name,
                    array(
                        'ebay_order_id' => $orderData['ebay_order_id'],
                        'store_order_id' => $order_id,
                        'ebay_order_status' => $orderData['order_status'],
                        'created_at' => $order->order_date,
                    )
                );
        } else {
            $result = 0;
        }
    }
}
