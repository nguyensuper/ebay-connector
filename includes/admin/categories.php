<?php


class Categories extends EbayProtected{

    public function __construct() {
        // Blank.
    }

    public function ebay_conf( $account_id ){

		$client = null;

	    $eBayConfig = $this->geteBayConfiguration( $account_id );

	    if ( $eBayConfig ) {
            $session = new Ebay\eBaySession(
                $eBayConfig['dev'],
                $eBayConfig['app'],
                $eBayConfig['cert']
            );
            $session->token = $eBayConfig['token'];
            $session->site = $eBayConfig['globalsites'];
            $session->location = $eBayConfig['location'];

			$client = new Ebay\eBaySOAP( $session );

        }

        return array( 'client' => $client, 'eBayConfig' => $eBayConfig );
	}

	public function geteBayConfiguration( $ebayAccountId = false ) {

        $obj            = new Ebay_Account( $ebayAccountId );

        $token          = $obj->get_access_token();

        $ebay_mode      = parent::get_ebay_mode();

        $gloabal_site   = $obj->get_ebay_global_site_id();

        $cer_id         = get_option('ebay_cert_id');//$obj->get_cer_id();

        $ebayuser_id    = $obj->get_user_id();

        $dev_id         = get_option('ebay_dev_id');//$obj->get_dev_id();

        $app_id         = get_option('ebay_app_id');//$obj->get_app_id();

        if( ! empty( $token ) && ! empty( $ebay_mode ) && ! empty( $cer_id ) && ! empty( $dev_id ) && ! empty( $app_id ) && ! empty( $ebayuser_id ) ) {

            $eBayConfig = [
                    'globalsites' => $gloabal_site,
                    'ebayuserid' => $ebayuser_id,
                    'token' => $token,
                    'dev' => $dev_id,
                    'app' => $app_id,
                    'cert' => $cer_id,
                    'mode' => $ebay_mode,
                    'location' => 'https://api.ebay.com/ws/api.dll',
                    // 'location' => 'https://api.ebay.com/wsapi',
                ];
            if ($eBayConfig['mode'] == '1') {
                $eBayConfig['location'] = 'https://api.sandbox.ebay.com/ws/api.dll';
                // $eBayConfig['location'] = 'https://api.sandbox.ebay.com/wsapi';
            }
            foreach ($eBayConfig as $value) {
                if ( ! is_object( $value ) && trim( $value ) == '' ) {
                    return false;
                }
            }

        }
        else{

            $eBayConfig='';

        }

        return $eBayConfig;
    }

    public function ebay_add_account_validate( $post ){

        $client = null;

        $eBayConfig = $this->geteAccountBayConfiguration( $post );

        if ( $eBayConfig ) {
            $session = new Ebay\eBaySession(
                $eBayConfig['dev'],
                $eBayConfig['app'],
                $eBayConfig['cert']
            );
            $session->token = $eBayConfig['token'];
            $session->site = $eBayConfig['globalsites'];
            $session->location = $eBayConfig['location'];

            $client = new Ebay\eBaySOAP( $session );

        }

        return array( 'client' => $client);
    }
    
    public function getClient()
    {
        $session = new Ebay\eBaySession(
            get_option('dev_id'),
            get_option('app_id'),
            get_option('cert_id')
        );

        $client = new Ebay\eBaySOAP( $session );
        return $client;
    }

    public function geteAccountBayConfiguration( $post = false ) {

        $token          = $post['ebay_access_token'];

        $ebay_mode      = parent::get_ebay_mode();

        $gloabal_site   = $post['ebay_global_sites'];

        $cer_id         = $post['ebay_certication_id'];

        $ebayuser_id    = $post['ebay_userid'];

        $dev_id         = $post['ebay_dev_id'];

        $app_id         = $post['ebay_app_id'];

        if( ! empty( $token ) && ! empty( $ebay_mode ) && ! empty( $cer_id ) && ! empty( $dev_id ) && ! empty( $app_id ) ) {

            $eBayConfig = [
                'globalsites' => $gloabal_site,
                'ebayuserid' => $ebayuser_id,
                'token' => $token,
                'dev' => $dev_id,
                'app' => $app_id,
                'cert' => $cer_id,
                'mode' => $ebay_mode,
                'location' => 'https://api.ebay.com/ws/api.dll',
                // 'location' => 'https://api.ebay.com/wsapi',
            ];
            if ($eBayConfig['mode'] == '1') {
                $eBayConfig['location'] = 'https://api.sandbox.ebay.com/ws/api.dll';
                // $eBayConfig['location'] = 'https://api.sandbox.ebay.com/wsapi';
            }
            foreach ($eBayConfig as $key => $value) {
                if ( ! is_object( $value ) && trim( $value ) == '' && $key !== 'ebayuserid' ) {
                    return false;
                }
            }

        }
        else{

            $eBayConfig='';

        }

        return $eBayConfig;
    }

	public function get_client( $client_id ) {

        global $wpdb;

        $token  = get_user_meta( $client_id, 'cache_access_token',true );

        return $token;

    }


    public static function wc_ebay_get_api_category_data() {

        global $wpdb;

        $category_raw_table = $wpdb->prefix . 'woocommerce_ebay_category_raw_data';

        $raw_array = array();

        $raw_array = $wpdb->get_results( "SELECT * FROM $category_raw_table", ARRAY_A );

        return $raw_array;

    }

    public static function import_categories_to_db( $categories ) {
        global $wpdb;

        $counter = $skip_counter = 0;

        $category_raw_table = $wpdb->prefix . 'woocommerce_ebay_category_raw_data';

        if( ! empty( $categories ) ) {

            foreach ( $categories as $category ) {

                $item_data = $category->item_data;
                $data = maybe_unserialize( $item_data );

                $cat = get_term_by( 'id', $category->id, 'product_cat' );

                if ( $cat == false ) {

                    $CategoryID = $data->CategoryID;
                    $CategoryName = $data->CategoryName;
                    $slug = preg_replace('/\s+/', '', $CategoryName );
                    $res = wp_insert_term(
                        $CategoryName, // the term
                        'product_cat', // the taxonomy
                        array(
                            'description'=> '',
                            'slug' => $slug
                        )
                    );

                    if (isset($data->ChildCategory) && ! is_wp_error($res)) {
                        if (is_array($data->ChildCategory)) {
                            $data = $data->ChildCategory;
                            foreach ($data as $key => $value) {
                                $CategoryID = $value->CategoryID;
                                $CategoryName = $value->Name;
                                $slug = preg_replace('/\s+/', '', $CategoryName);
                                $res_cat = wp_insert_term(
                                    $CategoryName, // the term
                                    'product_cat', // the taxonomy
                                    array(
                                        'description' => '',
                                        'slug'        => $slug,
                                        'parent'      => $res['term_id']
                                    )
                                );
                                if (isset($value->ChildCategory) && ! is_wp_error($res_cat)) {
                                    $parent = $res_cat['term_id'];
                                    if (is_array($value->ChildCategory)) {
                                        foreach ($value->ChildCategory as $key => $value) {
                                            $CategoryID = $value->CategoryID;
                                            $CategoryName = $value->Name;
                                            $slug = preg_replace('/\s+/', '', $CategoryName);
                                            wp_insert_term(
                                                $CategoryName, // the term
                                                'product_cat', // the taxonomy
                                                array(
                                                    'description' => '',
                                                    'slug'        => $slug,
                                                    'parent'      => $parent
                                                )
                                            );
                                        }
                                    } else {
                                        $CategoryID = $value->ChildCategory->CategoryID;
                                        $CategoryName = $value->ChildCategory->Name;
                                        $slug = preg_replace('/\s+/', '', $CategoryName);
                                        wp_insert_term(
                                            $CategoryName, // the term
                                            'product_cat', // the taxonomy
                                            array(
                                                'description' => '',
                                                'slug'        => $slug,
                                                'parent'      => $parent
                                            )
                                        );
                                    }
                                }
                            }
                        } else {
                            $CategoryID = $data->ChildCategory->CategoryID;
                            $CategoryName = $data->ChildCategory->Name;
                            $slug = preg_replace('/\s+/', '', $CategoryName);
                            wp_insert_term(
                                $CategoryName, // the term
                                'product_cat', // the taxonomy
                                array(
                                    'description' => '',
                                    'slug'        => $slug,
                                    'parent'      => $res['term_id']
                                )
                            );
                        }
                    }

                    if ($res && ! is_wp_error($res)) {
                        $delete_raw = $wpdb->delete(
                            $category_raw_table,
                            array(
                              'id'   => $category->id
                            ),
                            array( '%s' )
                        );
                        ++$counter;

                    } else {

                        ++$skip_counter;

                    }

                } else {

                    ++$skip_counter;

                }


            }
        }

        return array('new'=>$counter,'skip'=>$skip_counter);

    }

    function import_child_categories_to_db($data, $parent) {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $CategoryID = $value->CategoryID;
                $CategoryName = $value->Name;
                $slug = preg_replace('/\s+/', '', $CategoryName);
                wp_insert_term(
                    $CategoryName, // the term
                    'product_cat', // the taxonomy
                    array(
                        'description' => '',
                        'slug'        => $slug,
                        'parent'      => $parent
                    )
                );
            }
        } else {
            $CategoryID = $data->CategoryID;
            $CategoryName = $data->Name;
            $slug = preg_replace('/\s+/', '', $CategoryName);
            wp_insert_term(
                $CategoryName, // the term
                'product_cat', // the taxonomy
                array(
                    'description' => '',
                    'slug'        => $slug,
                    'parent'      => $parent
                )
            );
        }
    }

    public function check_access_token($token, $userid = 0){

        global $wpdb;

        $account_table = $wpdb->prefix . 'woocommerce_ebay_account';
        if ($userid) {
            $raw_array = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $account_table where access_token=%s and id!=%s",$token, $userid ), ARRAY_A );
        } else {
            $raw_array = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $account_table where access_token=%s",$token ), ARRAY_A );
        }
        return $raw_array;
    }

    public static function wc_ebay_insert_categories_in_bulk($raw_data) {

        global $wpdb;

        $categories_raw_table = $wpdb->prefix . 'woocommerce_ebay_category_raw_data';

        $resultsItems = json_decode(stripslashes($raw_data));

        $values = $cat_arr = $cat_array = array();

        $place_holders = array();

        $cat_array = $wpdb->get_results( "SELECT id FROM $categories_raw_table", ARRAY_A );

        $cat_final_arr = wp_list_pluck($cat_array,'id');

        $query = "INSERT INTO $categories_raw_table ( id, item_data ) VALUES ";

        foreach ( $resultsItems as $data ) {
            $data->CategoryName = $data->Name;
            unset($data->Name);

            if (isset($data->CategoryID)) {

                $CategoryID = $data->CategoryID;

            }

            if( ( empty( $cat_final_arr ) || ( ! empty( $cat_final_arr ) && ! in_array( $CategoryID, $cat_final_arr )) ) && ! in_array( $CategoryID, $cat_arr ) ) {

                array_push( $values, $CategoryID );

                array_push( $values, maybe_serialize( $data ) );

                $place_holders[] = "( '%s', '%s' )";

                $cat_arr[] = $CategoryID;

            }
        }

        if( ! empty( $values ) ) {

            $query .= implode(', ', $place_holders);

            $res = $wpdb->query( $wpdb->prepare( "$query ", $values ) );

        } else {

            $res = 0;
        }

        return $res;

    }

}
