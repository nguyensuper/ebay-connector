<?php

class Products
{
    public static function wc_ebay_get_api_raw_data()
    {
        global $wpdb;

        $product_raw_table = $wpdb->prefix.'woocommerce_ebay_product_raw_data';

        $raw_array = array();

        $raw_array = $wpdb->get_results("SELECT * FROM $product_raw_table ", ARRAY_A);

        return $raw_array;
    }

    public static function wc_ebay_insert_data_in_bulk($resultsItems)
    {
        global $wpdb;

        $resultsItems = json_decode(stripslashes($resultsItems));

        $product_raw_table = $wpdb->prefix.'woocommerce_ebay_product_raw_data';

        $values = $sku = $sku_array = array();

        $place_holders = array();
        $mainSKU = '';
        $sku_array = $wpdb->get_results("SELECT id FROM $product_raw_table", ARRAY_A);
        $sku_final_arr = wp_list_pluck($sku_array, 'id');

        $query = "INSERT INTO $product_raw_table ( id, item_data ) VALUES ";
        foreach ($resultsItems as $data) {
            // if (isset($data->SKU)) {
            //     $mainSKU = $data->SKU;
            // } else {
            $mainSKU = $data->ItemID;
            // }

            $mainSKU = strtolower(preg_replace('/\s+/', '-', $mainSKU));

            if (!empty($data) && !empty($mainSKU) && (empty($sku_final_arr) || (!empty($sku_final_arr) && !in_array($mainSKU, $sku_final_arr))) && !in_array($mainSKU, $sku)) {
                array_push($values, $mainSKU);

                array_push($values, maybe_serialize($data));

                $place_holders[] = "( '%s', '%s' )";

                $sku[] = $mainSKU;
            }
        }

        if (!empty($values)) {
            $query .= implode(', ', $place_holders);

            $res = $wpdb->query($wpdb->prepare("$query ", $values));
        } else {
            $res = 0;
        }

        return $res;
    }

    public static function import_products($item)
    {
        global $wpdb;
        $table = $wpdb->prefix.'postmeta';
        $product_raw_table = $wpdb->prefix.'woocommerce_ebay_product_raw_data';
        $product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';
        $helper = new Helper();
        $variation_arr['var_rule'] = array();
        $variation_arr['var_price'] = array();
        $counter = $skip_counter = 0;
        foreach ($item as $resultsItems) {
            $item_data = $resultsItems->item_data;
            $data = maybe_unserialize($item_data);

            $sku_id = $resultsItems->id;
            $product_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $table WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku_id));

            if ($product_id == null) {
                $data = json_decode(json_encode($data), true);

                $ebayCatId = $data['PrimaryCategory']['CategoryID'];

                $ebayCatName = explode(':', $data['PrimaryCategory']['CategoryName']);

                $ebayCatName = $ebayCatName[count($ebayCatName) - 1];

                if (isset($data['Storefront']) && $data['Storefront']) {
                    unset($data['Storefront']['StoreURL']);
                    foreach ($data['Storefront'] as $key => $value) {
                        $catMapDataID = $helper->getStoreBayCatMapData($value);
                        if ($catMapDataID) {
                            $catMapData[] = $catMapDataID;
                        }
                    }
                }

                $eBayWcId = $catMapData ? $catMapData : $helper->getWcCatId();

                /**get Weight of product **/

                $weight = 0;

                if (is_array($data['ShippingDetails']['CalculatedShippingRate'])) {
                    foreach ($data['ShippingDetails']['CalculatedShippingRate'] as $weightData) {
                        if (isset($weightData['unit']) && $weightData['unit'] == 'lbs') {
                            $weight = $weightData['_'];
                        }
                    }
                }

                $weight = $weight ? $weight : 1;

                /* get product variation**/
                if (isset($data['Variations']) && count($data['Variations']) > 0) {
                    $superAttIds = [];

                    foreach ($data['Variations']['Variation'] as $variation) {
                        $attributeData = [];
                        if (isset($variation['VariationSpecifics']['NameValueList']) && isset($variation['VariationSpecifics']['NameValueList'][0])) {
                            foreach ($variation['VariationSpecifics']['NameValueList'] as $nameValue) {
                                $attributeCode = str_replace(
                                            ' ',
                                            '_',
                                            $nameValue['Name']
                                        );
                                $attributeCode = preg_replace(
                                            '/[^A-Za-z0-9\_]/',
                                            '',
                                            $attributeCode
                                        );
                                $mageAttrCode = substr('conf_'.strtolower($attributeCode), 0, 30);
                                array_push($superAttIds, $mageAttrCode);
                            }
                        } else {
                            if (isset($variation['VariationSpecifics']['NameValueList'])
                                        && isset($variation['VariationSpecifics']['NameValueList']['Name'])) {
                                $attributeCode = str_replace(
                                            ' ',
                                            '_',
                                            $variation['VariationSpecifics']['NameValueList']['Name']
                                        );
                                $attributeCode = preg_replace(
                                            '/[^A-Za-z0-9\_]/',
                                            '',
                                            $attributeCode
                                        );
                                $mageAttrCode = substr('conf_'.strtolower($attributeCode), 0, 30);
                                array_push(
                                            $superAttIds,
                                            $mageAttrCode
                                        );
                            }
                        }
                        break;
                    }
                    // if (isset($data['SKU'])) {
                    //     $mainSKU = $data['SKU'];
                    // } else {
                    $mainSKU = $data['ItemID'];
                    // }

                    $mainSKU = strtolower(preg_replace('/\s+/', '-', $mainSKU));

                    $data['Description'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['Description']);
                    $data['Description'] = preg_replace('#<link(.*?)>(.*?)</link>#is', '', $data['Description']);
                    $data['Description'] = preg_replace('#<style(.*?)>(.*?)</style>#is', '', $data['Description']);
                    $data['Description'] = strip_tags($data['Description']);
                    $wholedata = [
                                'type_id' => 'variable',
                                'supperattr' => $superAttIds,
                                'status' => 1,
                                'category' => $eBayWcId,
                                'name' => $data['Title'],
                                'description' => $data['Description'],
                                'short_description' => ' ',
                                'sku' => $mainSKU,
                                'price' => $data['SellingStatus']['CurrentPrice']['_'],
                                'currency_id' => $data['SellingStatus']['CurrentPrice']['currencyID'],
                                'is_in_stock' => 1,
                                'tax_class_id' => 0,
                                'weight' => $weight,
                            ];
                    /* Assigne values to store product according to eBay product**/
                    foreach ($proCatSpec as $key => $value) {
                        $wholedata[$key] = $value;
                    }
                    $count = 0;
                    /* Variations of product **/
                    $associatePro = [];
                    if (isset($data['Variations']['Variation'][0]) === false) {
                        $data['Variations']['Variation'] = [0 => $data['Variations']['Variation']];
                    }

                    foreach ($data['Variations']['Variation'] as $key => $variation) {
                        ++$count;
                        $subt = '';
                        $attributeData = [];

                        if (isset($variation['VariationSpecifics']['NameValueList'])
                                    && isset($variation['VariationSpecifics']['NameValueList'][0])) {
                            foreach ($variation['VariationSpecifics']['NameValueList'] as $nameValue) {
                                if ($nameValue['Value'] == '') {
                                    continue;
                                }
                                $attributeCode = str_replace(
                                            ' ',
                                            '_',
                                            $nameValue['Name']
                                        );
                                $attributeCode = preg_replace(
                                            '/[^A-Za-z0-9\_]/',
                                            '',
                                            $attributeCode
                                        );
                                $mageAttrCode = substr('conf_'.strtolower($attributeCode), 0, 30);
                                $attributeData[$mageAttrCode] = $nameValue['Value'];
                            }
                        } else {
                            if (isset($variation['VariationSpecifics'])
                                        && isset($variation['VariationSpecifics']['NameValueList'])
                                        && $variation['VariationSpecifics']['NameValueList']['Value'] == '') {
                                continue;
                            }
                            if (isset($variation['VariationSpecifics'])
                                        && isset($variation['VariationSpecifics']['NameValueList'])
                                        && $variation['VariationSpecifics']['NameValueList']['Name']) {
                                $attributeCode = str_replace(
                                            ' ',
                                            '_',
                                            $variation['VariationSpecifics']['NameValueList']['Name']
                                        );
                                $attributeCode = preg_replace(
                                            '/[^A-Za-z0-9\_]/',
                                            '',
                                            $attributeCode
                                        );
                                $mageAttrCode = substr('conf_'.strtolower($attributeCode), 0, 30);
                                $attributeData[$mageAttrCode] = $variation['VariationSpecifics']['NameValueList']['Value'];
                            }
                        }

                        if (isset($variation['StartPrice'])
                                    && isset($variation['StartPrice']['_'])) {
                            $itemPrice = $variation['StartPrice']['_'];
                            $currencyId = $variation['StartPrice']['currencyID'];
                        } else {
                            $itemPrice = $variation['_'];
                            $currencyId = $variation['currencyID'];
                        }
                        if (isset($variation['SKU'])) {
                            $associateSku = strtolower(preg_replace('/\s+/', '-', $variation['SKU']));
                        } else {
                            $associateSku = $data['ItemID'].'-'.$count;
                            $associateSku = strtolower(preg_replace('/\s+/', '-', $associateSku));
                        }

                        $quictProData = [
                                        'status' => 1,
                                        'sku' => $associateSku,
                                        'price' => $itemPrice,
                                        'currency_id' => $currencyId,
                                        'qty' => (int) $variation['Quantity'] - (int) $variation['SellingStatus']['QuantitySold'],
                                        'is_in_stock' => intval($variation['SellingStatus']['QuantitySold']) > 0 ? 1 : 0,
                                        'tax_class_id' => 0,
                                        'weight' => $weight,
                                        'visibility' => 1,
                                    ];

                        foreach ($attributeData as $mageAttrCode => $value) {
                            $quictProData['variation'][$mageAttrCode] = $value;
                        }

                        if (count($attributeData) == 0) {
                            continue;
                        }

                        array_push(
                                    $associatePro,
                                    $quictProData
                                );
                    }

                    $wholedata['assocate_pro'] = $associatePro;
                } else {
                    if (isset($data['SKU'])) {
                        $sku = strtolower(preg_replace('/\s+/', '-', $data['SKU']));
                    } else {
                        $sku = strtolower(preg_replace('/\s+/', '-', $data['ItemID']));
                    }

                    $data['Description'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['Description']);
                    $data['Description'] = preg_replace('#<link(.*?)>(.*?)</link>#is', '', $data['Description']);
                    $data['Description'] = preg_replace('#<style(.*?)>(.*?)</style>#is', '', $data['Description']);
                    $data['Description'] = strip_tags($data['Description']);

                    /**For without variation product**/
                    $wholedata = ['type_id' => 'simple',
                                'status' => 1,
                                'producttypecustom' => 'customproductsimple',
                                'category' => $eBayWcId,
                                'name' => $data['Title'],
                                'description' => $data['Description'],
                                'short_description' => ' ',
                                'sku' => $sku,
                                'price' => $data['SellingStatus']['CurrentPrice']['_'],
                                'currency_id' => $data['SellingStatus']['CurrentPrice']['currencyID'],
                                'stock' => $data['Quantity'],
                                'is_in_stock' => 1,
                                'tax_class_id' => 0,
                                'weight' => $weight,
                            ];

                    foreach ($proCatSpec as $key => $value) {
                        $wholedata[$key] = $value;
                    }
                }

                $imageArr = array();

                if (isset($data['PictureDetails']['PictureURL'])) {
                    if (!is_array($data['PictureDetails']['PictureURL'])) {
                        // if ($i == 0) {
                        $defaultImage = $data['PictureDetails']['GalleryURL'];
                        // }
                        array_push($imageArr, $data['PictureDetails']['PictureURL']);
                    } else {
                        foreach ($data['PictureDetails']['PictureURL'] as $value) {
                            // if ($i == 0) {
                            //     $defaultImage = $value;
                            // }
                            $defaultImage = $data['PictureDetails']['GalleryURL'];
                            array_push($imageArr, $value);
                        }
                    }

                    $wholedata['image_data'] = [
                                'default' => $defaultImage,
                                'images' => $imageArr,
                            ];
                }

                $product_data = array(
                            'post_author' => 1,

                            'post_date' => '',

                            'post_date_gmt' => '',

                            'post_content' => $wholedata['description'],

                            'post_content_filtered' => $wholedata['short_description'],

                            'post_title' => $wholedata['name'],

                            'post_excerpt' => $wholedata['short_description'],

                            'post_status' => 'publish',

                            'post_type' => 'product',

                            'ping_status' => 'open',

                            'post_name' => $wholedata['name'],
                        );

                // // Create a simple WooCommerce product

                $postid = wp_insert_post($product_data);

                if ($wholedata['type_id'] == 'variable' && !empty($postid)) {
                    $product_title = 'Product #'.$postid.' Variation';

                    $post_name = 'product-'.$postid.'-variation';

                    foreach ($wholedata['assocate_pro'] as $single_pro) {
                        $varproduct_data = array(
                                        'post_parent' => $postid,

                                        'post_author' => 1,

                                        'post_date' => '',

                                        'post_date_gmt' => '',

                                        'post_content' => $wholedata['description'],

                                        'post_content_filtered' => $wholedata['short_description'],

                                        'post_title' => $product_title,

                                        'post_excerpt' => $wholedata['short_description'],

                                        'post_status' => 'publish',

                                        'post_type' => 'product_variation',

                                        'ping_status' => 'open',

                                        'post_name' => $post_name,
                                    );

                        $var_postid = wp_insert_post($varproduct_data);
                        if (isset($single_pro['visiblity']) && $single_pro['visiblity'] == 1) {
                            $visibility = 'visible';
                        } else {
                            $visibility = '';
                        }

                        if (isset($single_pro['is_in_stock']) && $single_pro['is_in_stock'] == 1) {
                            $manage_stock = 'yes';
                        } else {
                            $manage_stock = 'no';
                        }
                        if ($single_pro['qty'] > 0) {
                            $stock_status = 'instock';
                        } else {
                            $stock_status = 'outofstock';
                        }

                        update_post_meta($var_postid, '_visibility', $visibility);

                        add_post_meta($var_postid, '_sku', $single_pro['sku']);

                        add_post_meta($var_postid, '_featured', 'no');

                        add_post_meta($var_postid, '_sale_price', '');

                        add_post_meta($var_postid, '_sale_price_dates_from', '');

                        add_post_meta($var_postid, '_sale_price_dates_to', '');

                        add_post_meta($var_postid, '_regular_price', $single_pro['price']);

                        add_post_meta($var_postid, '_price', $single_pro['price']);

                        add_post_meta($var_postid, '_downloadable', 'no');

                        update_post_meta($var_postid, '_backorders', 'no');

                        update_post_meta($var_postid, '_stock_status', $stock_status);

                        update_post_meta($var_postid, '_stock', $single_pro['qty']);

                        update_post_meta($var_postid, '_manage_stock', $manage_stock);

                        update_post_meta($var_postid, '_virtual', 'no');

                        update_post_meta($var_postid, '_variation_description', '');

                        if (isset($single_pro['weight'])) {
                            update_post_meta($var_postid, '_weight', ('' === $single_pro['weight']) ? '' : wc_format_decimal($single_pro['weight']));
                        }

                        update_post_meta($var_postid, '_length', '');

                        update_post_meta($var_postid, '_width', '');

                        update_post_meta($var_postid, '_height', '');

                        $wcattributes = $single_pro['variation'];

                        if (!empty($wcattributes)) {
                            foreach ($wcattributes as $wcattribute_key => $wcattribute_value) {
                                update_post_meta($var_postid, 'attribute_'.$wcattribute_key, $wcattribute_value);

                                array_push($variation_arr['var_rule'], array('name' => $wcattribute_key, 'value' => $wcattribute_value));
                            }
                        }

                        array_push($variation_arr['var_price'], array('price' => $single_pro['price'], 'var_id' => $var_postid));
                    }
                }
                $product_attributes = array();

                if (!empty($variation_arr['var_rule'])) {
                    foreach ($variation_arr['var_rule'] as $attribute) {
                        if (empty($attribute['name']) || empty($attribute['value'])) {
                            continue;
                        }

                        $rep_str = $attribute['value'];

                        $rep_str = preg_replace('/\s+/', ' ', $rep_str);

                        $attribute['name'] = str_replace(' ', '-', $attribute['name']);

                        $attribute['value'] = str_replace('|', '|', $rep_str);

                        $attribute['is_visible'] = 1;

                        $attribute['is_variation'] = 1;

                        $attribute['is_taxonomy'] = (int) $attribute['is_taxonomy'];

                        $product_attributes[str_replace(' ', '-', $attribute['name'])] = $attribute;
                    }
                    if (!empty($product_attributes)) {
                        update_post_meta($postid, '_product_attributes', $product_attributes);
                    } else {
                        update_post_meta($postid, '_product_attributes', array());
                    }

                    $variation_arr['var_price'];

                    $price = wp_list_pluck($variation_arr['var_price'], 'price');

                    $min_price = min($price);

                    $min_key = array_search(min($price), $price);

                    $min_variation_id = $variation_arr['var_price'][$min_key]['var_id'];

                    $max_price = max($price);

                    $max_key = array_search(max($price), $price);

                    $max_variation_id = $variation_arr['var_price'][$max_key]['var_id'];

                    update_post_meta($postid, '_min_variation_price', $min_price);
                    update_post_meta($postid, '_min_variation_regular_price', $min_price);
                    update_post_meta($postid, '_max_variation_price', $max_price);
                    update_post_meta($postid, '_max_variation_regular_price', $max_price);
                    update_post_meta($postid, '_min_price_variation_id', $min_variation_id);
                    update_post_meta($postid, '_min_regular_price_variation_id', $min_variation_id);
                    update_post_meta($postid, '_max_price_variation_id', $max_variation_id);
                    update_post_meta($postid, '_max_regular_price_variation_id', $max_variation_id);
                }

                if (isset($wholedata['is_in_stock']) && $wholedata['is_in_stock'] == 1) {
                    $manage_stock = 'yes';
                    $stock_status = 'instock';
                } else {
                    $manage_stock = 'no';
                    $stock_status = 'outofstock';
                }

                $visibility = 'visible';

                update_post_meta($postid, '_visibility', $visibility);

                add_post_meta($postid, '_sku', $wholedata['sku']);

                add_post_meta($postid, '_featured', 'no');

                add_post_meta($postid, '_regular_price', $wholedata['price']);

                add_post_meta($postid, '_sale_price', '');

                add_post_meta($postid, '_sale_price_dates_from', '');

                add_post_meta($postid, '_sale_price_dates_to', '');

                add_post_meta($postid, '_price', $wholedata['price']);

                add_post_meta($postid, '_price', $wholedata['price']);

                add_post_meta($postid, '_downloadable', 'no');

                update_post_meta($postid, '_backorders', 'no');

                update_post_meta($postid, '_stock_status', $stock_status);

                if ($wholedata['type_id'] != 'variable') :

                                update_post_meta($postid, '_stock', $wholedata['stock']);

                endif;

                update_post_meta($postid, '_manage_stock', $manage_stock);

                update_post_meta($postid, '_virtual', 'no');

                if (array_key_exists('image_data', $wholedata) && !empty($wholedata['image_data']['default'])) :

                                $attach_id = media_sideload_image($wholedata['image_data']['default'], $postid, '', 'id');

                if (!is_object($attach_id)) {
                    update_post_meta($postid, '_thumbnail_id', $attach_id);
                }

                $gallery_ids = array();
                if (isset($wholedata['image_data']['images'])) {
                    foreach ($wholedata['image_data']['images'] as $key => $value) {
                        $attach_id = media_sideload_image($value, $postid, '', 'id');

                        $gallery_ids[] = $attach_id;
                    }

                    if ($gallery_ids) {
                        if (!is_object($gallery_ids[0])) {
                            update_post_meta($postid, '_product_image_gallery', implode(',', $gallery_ids));
                        }
                    }
                }

                endif;

                if (isset($wholedata['weight'])) {
                    update_post_meta($postid, '_weight', ('' === $wholedata['weight']) ? '' : wc_format_decimal($wholedata['weight']));
                }

                update_post_meta($postid, '_length', '');

                update_post_meta($postid, '_width', '');

                update_post_meta($postid, '_height', '');

                $category_name = [];

                if (!empty($eBayWcId) && is_array($eBayWcId)) {
                    foreach ($eBayWcId as $eBayWcStoreId) {
                        if ($term = get_term_by('id', $eBayWcStoreId, 'product_cat')) {
                            $category_name[] = $term->name;
                        }

                        $pro_cat_id = array('object_id' => $postid, 'term_taxonomy_id' => $eBayWcStoreId);

                        $d = $wpdb->insert("$wpdb->term_relationships", $pro_cat_id);
                    }
                }

                wp_set_object_terms($postid, $wholedata['type_id'], 'product_type', false);

                $result = $wpdb->insert(
                                $product_map_table,
                                array(
                                    'ebay_product_id' => $data['ItemID'],
                                    'ebay_product_name' => $wholedata['name'],
                                    'product_type' => $wholedata['type_id'],
                                    'store_product_id' => $postid,
                                    'store_category' => !empty($category_name) ? implode(', ', $category_name) : 'No Category',
                                )
                            );

                if ($result) {
                    $delete_raw = $wpdb->delete(
                                    $product_raw_table,
                                    array(
                                        'id' => $sku_id,
                                    ),
                                    array('%s')
                                );
                } ?>


                            <?php

                            ++$counter;
            } else {
                ?>

                        <?php
                        $delete_raw = $wpdb->delete(
                            $product_raw_table,
                            array(
                                'id' => $sku_id,
                            ),
                            array('%s')
                        );

                ++$skip_counter;
            }
        }

        return array('new' => $counter, 'skip' => $skip_counter);
    }
}
