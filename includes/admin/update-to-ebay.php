<?php

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WC_Ebay_Map_Products')) {
    class Update_To_Ebay
    {
        public function __construct()
        {
        }

        public function ebay_update($product_id, $account_id = 0)
        {
            $account_id = $account_id ? $account_id : $_GET['aid'];

            $ebayprotected = new EbayProtected();

            $default_cat_id = $ebay_def_stock = '';

            $settings = new Categories();

            $account = new Ebay_Account($account_id);

            $paypal_id = $account->get_ebay_paypal();//$ebayprotected->get_ebay_paypal_id();

            $default_cat_id = $ebayprotected->get_default_cat();

            $ebay_def_stock = $ebayprotected->get_default_stock();
            $ebay_order_status = $ebayprotected->get_ebay_order_status();

            $returns_policy_options_define = $ebayprotected->get_returns_policy_options_define();
            $returns_policy_options_refundOption = $ebayprotected->get_returns_policy_options_refundOption();
            $returns_policy_options_return_days = $ebayprotected->get_returns_policy_options_return_days();
            $returns_policy_options_pay_by = $ebayprotected->get_ebay_returns_policy_options_pay_by();
            $returns_policy_options_other_info = $ebayprotected->get_ebay_returns_policy_options_other_info();

            $ebay_listing_options_duration = $ebayprotected->get_ebay_ebay_listing_options_duration();

            $dispatch_options_time = $ebayprotected->get_ebay_dispatch_options_time();

            $shipping_options_ship_service_priority = $ebayprotected->get_ebay_shipping_options_ship_service_priority();
            $shipping_options_ship_service = $ebayprotected->get_ebay_shipping_options_ship_service();
            $shipping_options_ship_service_cost = $ebayprotected->get_ebay_shipping_options_ship_service_cost();
            $international_shipping_options_ship_service_priority = $ebayprotected->get_ebay_international_shipping_options_ship_service_priority();
            $international_shipping_options_ship_service = $ebayprotected->get_ebay_international_shipping_options_ship_service();
            $international_shipping_options_ship_service_cost = $ebayprotected->get_ebay_international_shipping_options_ship_service_cost();
            $international_shipping_options_ship_service_additional_cost = $ebayprotected->get_ebay_international_shipping_options_ship_service_additional_cost();
            $ship_to_location = $ebayprotected->get_ship_country_code_product_export();
            $location = $ebayprotected->get_item_location_pincode();
            $global_shipping = $ebayprotected->get_global_shipping();
            $shipping_options_ship_service_additional_cost = $ebayprotected->get_ebay_shipping_options_ship_service_additional_cost();
            $shipping_options_ship_time_min = $ebayprotected->get_ebay_shipping_options_ship_time_min();
            $shipping_options_ship_time_max = $ebayprotected->get_ebay_shipping_options_ship_time_max();
            $shipping_options_ship_free = $ebayprotected->get_ebay_shipping_options_ship_free();
            $country_code_product_export = $ebayprotected->get_country_code_product_export();
            $country_code_product_export = !empty($country_code_product_export) ? $country_code_product_export : 'US';
            $_product = wc_get_product($product_id);

            $_product_category = '';

            $product_title = $_product->get_title();
            $eBayDefaultSetting = [
                'PayPalEmailAddress' => $paypal_id,
                'ListingDuration' => $ebay_listing_options_duration,
                'PostalCode' => $account->get_postal_code(),
                'DispatchTimeMax' => $dispatch_options_time,
                'Country' => $country_code_product_export,

                'DefaultOrderStatus' => $ebay_order_status,
                'DefaultProQty' => $ebay_def_stock,
                'ShippingDetails' => [
                    'ShippingServiceOptions' => [[
                        'ShippingServicePriority' => $shipping_options_ship_service_priority,
                        'ShippingService' => $shipping_options_ship_service,
                        'ShippingServiceCost' => $shipping_options_ship_service_cost,
                        'ShippingServiceAdditionalCost' => $shipping_options_ship_service_additional_cost,
                        'ShippingTimeMin' => $shipping_options_ship_time_min,
                        'ShippingTimeMax' => $shipping_options_ship_time_max,
                        'FreeShipping' => 0,
                        ]],
                        'InternationalShippingServiceOption' => [[
                            'ShippingServicePriority' => $international_shipping_options_ship_service_priority,
                            'ShippingService' => $international_shipping_options_ship_service,
                            'ShippingServiceCost' => $international_shipping_options_ship_service_cost,
                            'ShippingServiceAdditionalCost' => $international_shipping_options_ship_service_additional_cost,
                            'ShipToLocation' => $ship_to_location,
                            ]],
                            'GlobalShipping' => $global_shipping,
                        ],
                        'ReturnPolicy' => [
                             'ReturnsAcceptedOption' => "ReturnsNotAccepted"
                        ],
                    ];

            $cat_id = array();

            $formatted_attributes = array();

            $attributes = $_product->get_attributes();

            $attachment_ids = $_product->get_gallery_image_ids();

            if (!empty($attachment_ids)) :

                        foreach ($attachment_ids as $attachment_id) {
                            $image_link[] = wp_get_attachment_url($attachment_id);
                        }

            endif;

            foreach ($attributes as $attr => $attr_deets) {
                $attribute_label = wc_attribute_label($attr);

                if (isset($attributes[$attr]) || isset($attributes['pa_'.$attr])) {
                    $attribute = isset($attributes[$attr]) ? $attributes[$attr] : $attributes['pa_'.$attr];

                    if ($attribute['is_taxonomy']) {
                        $formatted_attributes[$attribute_label] = implode(', ', wc_get_product_terms($_product->get_id(), $attribute['name'], array('fields' => 'names')));
                    } else {
                        $formatted_attributes[$attribute_label] = $attribute['value'];
                    }
                }
            }

            if (has_post_thumbnail($product_id)) {
                $attachment_ids[0] = get_post_thumbnail_id($product_id);

                $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full');
            }

            if (!empty($attachment)) {
                $pictureurl = $attachment[0];
            } else {
                $pictureurl = '';
            }

            $image_arr = array(
                      //  'GalleryType' => 'Plus',
                        'GalleryURL' => $pictureurl,
                        'PhotoDisplay' => 'PicturePack',
                    );

            array_unshift($image_link, $pictureurl);

            if (!empty($image_link)) {
                $image_arr['PictureURL'] = $image_link;
            } else {
                $image_arr = array();
            }

            $stock = $_product->get_stock_quantity();

            if ($stock <= 0 || empty($stock)) {
                $stock = $ebay_def_stock;
            }

            $price = $_product->get_price();

            $product_desc = $_product->get_description();

            $terms = get_the_terms($product_id, 'product_cat');

            if (is_array($terms)) {
                foreach ($terms as $term) {
                    $_product_category = $term->name.', '.$_product_category;
                    $cat_id[] = $term->term_id;
                }
            }

            $return_data = '';
            $ebay_cat_id = $this->getMappedCategory($cat_id);
            if (empty($ebay_cat_id)) {
                $result['msg'] = 'Product category is not mapped with Ebay category';

                $return_data = array('Ack' => strtolower('Failure'), 'msg' => array('<b>'.$product_title.' - </b>'.$result['msg']));

                return $return_data;
            }

            $condId = get_post_meta($product_id, 'ebay_condition_id', true);

            if (empty($condId)) {
                $result['msg'] = __('Condition ID is not set for this product', 'wc_ebay');

                $return_data = array('Ack' => strtolower('Failure'), 'msg' => array('<b>'.$product_title.' - </b>'.$result['msg']));

                return $return_data;
            }

            $nameValueList = array();

            $ebay_cat_specifics = $this->getMappedSpecifics($cat_id);

            $ebay_product_id = $this->getEbayId($product_id);

            $ebay_cat_specifics = maybe_unserialize($ebay_cat_specifics);

            foreach ($_product->get_attributes() as $key => $value) {
                $attr_data = $value->get_data();
                foreach ($ebay_cat_specifics as $specific_key => $specific_value) {
                    if ($specific_value == $attr_data['id']) {
                        $specific_name = explode('[', get_taxonomy($attr_data['name'])->labels->singular_name);
                        $nameValueList[] = [
                            'Name' => $specific_name[0],
                            'Value' => get_term($attr_data['options'][0])->name,
                        ];
                    }
                }
            }

            ///
            ///
            ///
                 ////
            ///
            $classname     = WC_Product_Factory::get_product_classname( $product_id, "variable" );
            $product      = new $classname( $product_id );


            $variation_attributes = wc_list_pluck( array_filter( $product->get_attributes(), 'wc_attributes_array_filter_variation' ), 'get_slugs' );
             $possible_attributes  = array_reverse( wc_array_cartesian( $variation_attributes ) );


            
            $VariationSpecificsSet = array();

            $output = array();
            if( $variation_attributes ) {
                foreach ( $variation_attributes as $name => $options ) {
                   
                    $type = str_replace( "pa_", "", $name ); 
                    $output[$name] = $type;
                    foreach( $options as $key => $value ) {
                        
                        if( $type == 'size' ){
                            $value = strtoupper( $value );
                        }

                        $options[$key] = ucfirst( $value );
                    }

                    $VariationSpecificsSet[] = array(
                        'Name' => ucfirst( $type ),
                        'Value' => $options
                    );
                }
            }


            $Variation = array();
            if( $product && $product->get_id() ) {
             
                $variations = $product->get_children();

                foreach ( $variations as $key => $value ) {
                    $variation  = new \WC_Product_Variation( $value );
                    $sp = array();

                    $tmp = array();
                    foreach( $output as $name => $type ) {
                        $value =  $variation->get_attribute( $name );

                        if( $type == 'size' ) {
                            $value = strtoupper( $value );
                        }else {
                            $value = ucfirst( $value );
                        }

                        $tmp[] = $value;

                        $sp[] = array(
                            'Name' => ucfirst( $type ),
                            'Value' => $value
                        );
                    }
                    
                    $Variation[] = array( 
                        'SKU'        =>  str_replace( "-", "_", $product->get_sku() )."_".implode( "_", $tmp ),
                        'StartPrice' =>  $variation->get_regular_price(),
                        'Quantity'   => 2,
                        'VariationSpecifics' => $sp
                    );
                  
                }
                
            }


           //   echo '<pre> ha cong tine'  . print_r( $Variation,1 ); die;
          //  echo '<pre>' . print_r( $possible_attributes ,1 ); 
           //  echo '<pre>' . print_r( $VariationSpecificsSet ,1 ); 

       //     die("ha cong tien");

            ///
            ///
            ///
            $variations = array(
                'VariationSpecificsSet' => $VariationSpecificsSet,
                'Variation' => $Variation
            
            );

            ///
            $item = [
                'ItemID' => $ebay_product_id,
                'ListingType' => 'FixedPriceItem',
                'PaymentMethods' => 'PayPal',
                'PayPalEmailAddress' => $eBayDefaultSetting['PayPalEmailAddress'],
                'Country' => $eBayDefaultSetting['Country'],
                'ListingDuration' => $eBayDefaultSetting['ListingDuration'],
                'Title' => $product_title,
                'Subtitle' => $product_title,
                'PictureDetails' => $image_arr,
                'Description' => $product_desc,
             //   'Quantity' => $stock,
                'Location' => $location,
                'PostalCode' => $eBayDefaultSetting['PostalCode'],
                'DispatchTimeMax' => $eBayDefaultSetting['DispatchTimeMax'],
                'ItemSpecifics' => $nameValueList,
                'ReturnPolicy' => $eBayDefaultSetting['ReturnPolicy'],
                'ShippingDetails' => $eBayDefaultSetting['ShippingDetails'],
              //  'StartPrice' => $price,
                'ConditionID' => $condId,
                'ProductListingDetails' => [
                    'EAN' => 'Does Not Apply',
                    [
                        'name' => 'EAN',
                        'value' => 'Does Not Apply',
                    ],
                ],

                'PrimaryCategory' => array('CategoryID' => $ebay_cat_id),
                'CategoryMappingAllowed' => true,
                'Variations' => $variations
            ];

            try {


                $data = $settings->ebay_conf($account_id);

                $eBayConfig = $data['eBayConfig'];
                $client = $data['client'];
                if ($client) {
                    /**for get ebay category condition and variations **/

                    $params = [
                        'Version' => 891,
                        'ErrorLanguage' => 'en_US',
                        'Item' => $item,
                    ];


                   // echo '<Pre>' . print_r( $params ,1 );die;
                    $results = $client->ReviseItem($params);

                    if (isset($results->Ack) && $results->Ack == 'Success') {
                        $return_data = array('Ack' => strtolower($results->Ack), 'msg' => array('<b>'.$product_title.' - </b>'));
                    } else {
                        if (isset($results->Errors) && is_array($results->Errors)) {
                            foreach ($results->Errors as $err) {
                                $msg[] = '<b>'.$product_title.' - </b>'.$err->LongMessage;
                            }

                            $return_data = array('Ack' => strtolower($results->Ack), 'msg' => $msg);
                        } elseif (isset($results->Errors)) {
                            $msg = array('<b>'.$product_title.' - </b>'.$results->Errors->LongMessage);

                            $return_data = array('Ack' => strtolower($results->Ack), 'msg' => $msg);
                        }
                    }

                    return $return_data;
                }
            } catch (\Exception $e) {
                $result['msg'] = $e->getMessage();

                echo $result;
            }
        }

        public function getMappedCategory($cat_id)
        {
            global $wpdb;

            $final_cat_id = '';

            $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

            foreach ($cat_id as $c_id) {
                $id = $wpdb->get_row("SELECT ebay_category_id FROM $category_map_table WHERE category_id='$c_id'");

                if (!empty($id)) {
                    $final_cat_id = $id->ebay_category_id;

                    break;
                }
            }

            return $final_cat_id;
        }

        public function getEbayId($product_id)
        {
            global $wpdb;

            $product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';

            $result = $wpdb->get_var($wpdb->prepare(
                    "Select ebay_product_id from $product_map_table where store_product_id = %d", $product_id
                    )
            );

            return $result;
        }

        public function getMappedSpecifics($cat_id)
        {
            global $wpdb;

            $cat_specifics = '';

            $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

            foreach ($cat_id as $c_id) {
                $id = $wpdb->get_row("SELECT category_specification FROM $category_map_table WHERE category_id='$c_id'");

                if (!empty($id)) {
                    $cat_specifics = $id->category_specification;

                    break;
                }
            }

            return $cat_specifics;
        }
    }
}
