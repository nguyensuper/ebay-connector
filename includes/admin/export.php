<?php

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WC_Ebay_Export_Product')) {
    class Export_To_Ebay
    {
        public function __construct()
        {
        }

        public function ebay_export($product_id, $account_id = 0, $cron = false)
        {
            $account_id = $account_id ? $account_id : $_GET['aid'];

            $ebayprotected = new EbayProtected();

            $currency = get_woocommerce_currency();

            $default_cat_id = $ebay_def_stock = '';

            $settings = new Categories();

            $account = new Ebay_Account($account_id);

            $paypal_id = $account->get_ebay_paypal();//$ebayprotected->get_ebay_paypal_id();
            
            $brand = $account->get_brand();

            $default_cat_id = $ebayprotected->get_default_cat();

            $ebay_def_stock = $ebayprotected->get_default_stock();
            $ebay_order_status = $ebayprotected->get_ebay_order_status();

            $returns_policy_options_define = $ebayprotected->get_returns_policy_options_define();
            $returns_policy_options_refundOption = $ebayprotected->get_returns_policy_options_refundOption();
            $returns_policy_options_return_days = $ebayprotected->get_returns_policy_options_return_days();
            $returns_policy_options_pay_by = $ebayprotected->get_ebay_returns_policy_options_pay_by();
            $returns_policy_options_other_info = $ebayprotected->get_ebay_returns_policy_options_other_info();

            $ebay_listing_options_duration = $ebayprotected->get_ebay_ebay_listing_options_duration();

            $dispatch_options_time = $ebayprotected->get_ebay_dispatch_options_time();

            $shipping_options_ship_service_priority = $ebayprotected->get_ebay_shipping_options_ship_service_priority();
            $shipping_options_ship_service = $ebayprotected->get_ebay_shipping_options_ship_service();
            $shipping_options_ship_service_cost = $ebayprotected->get_ebay_shipping_options_ship_service_cost();
            $international_shipping_options_ship_service_priority = $ebayprotected->get_ebay_international_shipping_options_ship_service_priority();
            $international_shipping_options_ship_service = $ebayprotected->get_ebay_international_shipping_options_ship_service();
            $international_shipping_options_ship_service_cost = $ebayprotected->get_ebay_international_shipping_options_ship_service_cost();
            $international_shipping_options_ship_service_additional_cost = $ebayprotected->get_ebay_international_shipping_options_ship_service_additional_cost();
            $ship_to_location = $ebayprotected->get_ship_country_code_product_export();
            $location = $ebayprotected->get_item_location_pincode();
            $global_shipping = $ebayprotected->get_global_shipping();
            $shipping_options_ship_service_additional_cost = $ebayprotected->get_ebay_shipping_options_ship_service_additional_cost();
            $shipping_options_ship_time_min = $ebayprotected->get_ebay_shipping_options_ship_time_min();
            $shipping_options_ship_time_max = $ebayprotected->get_ebay_shipping_options_ship_time_max();
            $shipping_options_ship_free = $ebayprotected->get_ebay_shipping_options_ship_free();
            $country_code_product_export = $ebayprotected->get_country_code_product_export();
            $country_code_product_export = !empty($country_code_product_export) ? $country_code_product_export : 'US';
            $_product = wc_get_product($product_id);
            $_product_type = $_product->get_type();
            $_product_name = $_product->get_title();
            $_product_category = '';
            $product_title = $_product->get_title();

            $eBayDefaultSetting = [
                'PayPalEmailAddress' => $paypal_id,
                'ListingDuration' => $ebay_listing_options_duration,
                'PostalCode' => $account->get_postal_code(),
                'DispatchTimeMax' => $dispatch_options_time,
                'Country' => $country_code_product_export,
                'Currency' => $currency,
                'DefaultOrderStatus' => $ebay_order_status,
                'DefaultProQty' => $ebay_def_stock,
                'ShippingDetails' => [
                    'ShippingServiceOptions' => [[
                        'ShippingServicePriority' => $shipping_options_ship_service_priority,
                        'ShippingService' => $shipping_options_ship_service,
                        'ShippingServiceCost' => $shipping_options_ship_service_cost,
                        'ShippingServiceAdditionalCost' => $shipping_options_ship_service_additional_cost,
                        'ShippingTimeMin' => $shipping_options_ship_time_min,
                        'ShippingTimeMax' => $shipping_options_ship_time_max,
                        'FreeShipping' => 0,
                    ]],
                    'InternationalShippingServiceOption' => [[
                        'ShippingServicePriority' => $international_shipping_options_ship_service_priority,
                        'ShippingService' => $international_shipping_options_ship_service,
                        'ShippingServiceCost' => $international_shipping_options_ship_service_cost,
                        'ShippingServiceAdditionalCost' => $international_shipping_options_ship_service_additional_cost,
                        'ShipToLocation' => $ship_to_location,
                    ]],
                    'GlobalShipping' => $global_shipping,
                ],
                'ReturnPolicy' => [
                            'ReturnsAcceptedOption' => "ReturnsNotAccepted"
                            ],
            ];
            $image_link = [];
            $sizeArr = [];
            $colorArr = [];
            $cat_id = array();

            $formatted_attributes = array();

            $attributes = $_product->get_attributes();

            $attachment_ids = $_product->get_gallery_image_ids();

            if (!empty($attachment_ids)) :

                foreach ($attachment_ids as $attachment_id) {
                    $image_link[] = wp_get_attachment_url($attachment_id);
                }

            endif;

            foreach ($attributes as $attr => $attr_deets) {
                $attribute_label = wc_attribute_label($attr);

                if (isset($attributes[$attr]) || isset($attributes['pa_'.$attr])) {
                    $attribute = isset($attributes[$attr]) ? $attributes[$attr] : $attributes['pa_'.$attr];

                    if ($attribute['is_taxonomy']) {
                        $formatted_attributes[$attribute_label] = implode(', ', wc_get_product_terms($_product->get_id(), $attribute['name'], array('fields' => 'names')));
                    } else {
                        $formatted_attributes[$attribute_label] = $attribute['value'];
                    }
                }
            }

            if (has_post_thumbnail($product_id)) {
                $attachment_ids[0] = get_post_thumbnail_id($product_id);

                $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full');
            }

            if (!empty($attachment)) {
                $pictureurl = $attachment[0];
            } else {
                $pictureurl = '';
            }

            $image_arr = array(
     //           'GalleryType' => 'Plus',
                'GalleryURL' => $pictureurl,
                'PhotoDisplay' => 'PicturePack',
            );
            if (!is_array($pictureurl) && $pictureurl) {
                $pictureurl = [$pictureurl];
            }
            if (is_array($image_link) && is_array($pictureurl)) {
                array_unshift($image_link, $pictureurl);
            }

            if (!empty($image_link)) {
                $image_arr['PictureURL'] = $image_link;
            } else {
                $image_arr = array();
            }

            $stock = $_product->get_stock_quantity();
            if ($stock <= 0 || empty($stock)) {
                $stock = $ebay_def_stock;
            }

            $price = $_product->get_price();

            $product_desc = trim($_product->get_description());
            if (!$product_desc) {
                wp_delete_post($product_id);
                $result['msg'] = 'Description empty';
                $return_data = array('Ack' => strtolower('Failure'), 'msg' => array('<b>'.$product_title.' - </b>'.$result['msg']));

                return $return_data;
            }

            $terms = get_the_terms($product_id, 'product_cat');

            if (is_array($terms)) {
                foreach ($terms as $term) {
                    $_product_category = $term->name.', '.$_product_category;
                    $cat_id[] = $term->term_id;
                }
            }

            $return_data = '';
            $ebay_cat_id = 15687;
            if (preg_match('/(Hoodie|hoodie|Sweatshirt|sweatshirt)/', $product_title)) {
                $ebay_cat_id = 155183;
            }
            // 155183 Hoodies & Sweatshirts
            // $ebay_cat_id = $this->getMappedCategory($cat_id);
            if (empty($ebay_cat_id)) {
                $result['msg'] = 'Product category is not mapped with Ebay category';

                $return_data = array('Ack' => strtolower('Failure'), 'msg' => array('<b>'.$product_title.' - </b>'.$result['msg']));

                return $return_data;
            }

            // $condId = get_post_meta($product_id, 'ebay_condition_id', true);
            $condId = 1500;

            if (empty($condId)) {
                $result['msg'] = __('Condition ID is not set for this product', 'wc_ebay');

                $return_data = array('Ack' => strtolower('Failure'), 'msg' => array('<b>'.$product_title.' - </b>'.$result['msg']));

                return $return_data;
            }

            // $nameValueList = array();

            // $ebay_cat_specifics = $this->getMappedSpecifics($cat_id);
            // $ebay_cat_specifics = maybe_unserialize($ebay_cat_specifics);
            // foreach ($_product->get_attributes() as $key => $value) {
            //     $attr_data = $value->get_data();
            //     if (is_array($ebay_cat_specifics)) {
            //         foreach ($ebay_cat_specifics as $specific_key => $specific_value) {
            //             if ($specific_value == $attr_data['id']) {
            //                 $specific_name = explode('[', get_taxonomy($attr_data['name'])->labels->singular_name);
            //                 $nameValueList[] = [
            //                     'Name' => $specific_name[0],
            //                     'Value' => get_term($attr_data['options'][0])->name,
            //                 ];
            //             }
            //         }
            //     }
            // }

            ////
            ///
            $classname     = WC_Product_Factory::get_product_classname( $product_id, "variable" );
            $product      = new $classname( $product_id );


            $variation_attributes = wc_list_pluck( array_filter( $product->get_attributes(), 'wc_attributes_array_filter_variation' ), 'get_slugs' );
             $possible_attributes  = array_reverse( wc_array_cartesian( $variation_attributes ) );


            
            $VariationSpecificsSet = array();

            $output = array();
            if( $variation_attributes ) {
                foreach ( $variation_attributes as $name => $options ) {
                   
                    $type = str_replace( "pa_", "", $name ); 
                    $output[$name] = $type;
                    foreach( $options as $key => $value ) {
                        
                        if( $type == 'size' ){
                            $value = strtoupper( $value );
                            if (!in_array(strtoupper($value), $sizeArr)) {
                                $sizeArr[] = strtoupper($value);
                            }
                        }
                        if( $type == 'color' ){
                            if (!in_array(strtoupper($value), $colorArr)) {
                                $colorArr[] = $value;
                            }
                        }

                        $options[$key] = ucfirst( $value );
                    }

                    $VariationSpecificsSet[] = array(
                        'Name' => ucfirst( $type ),
                        'Value' => $options
                    );
                }
            }

            $VariationSpecificsSetX = array();
            $Variation = array();
            if( $product && $product->get_id() ) {
             
                $variations = $product->get_children();

                foreach ( $variations as $key => $value ) {
                    if (count($Variation) >= 200) break;
                    $variation  = new \WC_Product_Variation( $value );
                    $sp = array();

                    $tmp = array();
                    foreach( $output as $name => $type ) {
                        $value =  $variation->get_attribute( $name );

                        if( $type == 'size' ) {
                            $value = strtoupper( $value );
                        }else {
                            $value = ucfirst( $value );
                        }
                        
                        if ($type == 'type') {
                            $type = "Type()";
                        }

                        $tmp[] = $value;

                        $sp[] = array(
                            'Name' => ucfirst( $type ),
                            'Value' => $value
                        );
                        if (!isset($VariationSpecificsSetX[ucfirst( $type )]) || !in_array($value, $VariationSpecificsSetX[ucfirst( $type )])) {
                            $VariationSpecificsSetX[ucfirst( $type )][] = $value;
                        }
                    }
                    $Variation[] = array( 
                        'SKU'        =>  str_replace( "-", "_", $product->get_sku() )."_".implode( "_", $tmp ),
                        'StartPrice' =>  $variation->get_regular_price(),
                        'Quantity'   => 2,
                        'VariationSpecifics' => ['NameValueList' => $sp],
                        'VariationProductListingDetails' => [
                                    'EAN'  => 'Does not apply',
                                    'ISBN' => 'Does not apply',
                                    'UPC'  => 'Does not apply',
                                ]
                    );
                  
                }
                
            }


           //   echo '<pre> ha cong tine'  . print_r( $Variation,1 ); die;
          //  echo '<pre>' . print_r( $possible_attributes ,1 ); 
           //  echo '<pre>' . print_r( $VariationSpecificsSet ,1 ); 

       //     die("ha cong tien");
            $VariationSpecificsSet = [];
            foreach ($VariationSpecificsSetX as $name => $values) {
                $VariationSpecificsSet[] = [
                    'Name' => $name,
                    'Value' => $values
                ];
            }
            $variations = array(
                'VariationSpecificsSet' => ['NameValueList' => $VariationSpecificsSet],
                'Variation' => $Variation
            
            );
            // dd($Variation[0]);
 //echo '<pre> ha cong tine'  . print_r( $variations,1 ); die;
            $item = [
                'ListingType'        => 'FixedPriceItem',
                'Currency'           => $eBayDefaultSetting['Currency'],
                'PaymentMethods'     => 'PayPal',
                'PayPalEmailAddress' => $eBayDefaultSetting['PayPalEmailAddress'],
                'Country'            => $eBayDefaultSetting['Country'],
                'ListingDuration'    => $eBayDefaultSetting['ListingDuration'],
                'Title'              => $product_title,
                // 'Subtitle'           => $product_title,
                'PictureDetails'     => $image_arr,
                'Description'        => $product_desc,
                // 'Quantity'        => $stock,
                'Location'           => $location,
                'PostalCode'         => $eBayDefaultSetting['PostalCode'],
                'DispatchTimeMax'    => $eBayDefaultSetting['DispatchTimeMax'],
                // 'ItemSpecifics'      => ['NameValueList' => $nameValueList],
                'ReturnPolicy'       => $eBayDefaultSetting['ReturnPolicy'],
                'ShippingDetails'    => $eBayDefaultSetting['ShippingDetails'],
                //  'StartPrice'     => $price,
                'ConditionID'        => $condId,

                'PrimaryCategory' => ['CategoryID' => $ebay_cat_id],
                'CategoryMappingAllowed' => true,
                'Variations' => $variations 
            ];
            $typeItemSpecifics = "T-Shirt, Tank Top";
            $sleeveItemSpecifics = "Short";
            if (preg_match('/(Hoodie|hoodie|Sweatshirt|sweatshirt)/', $product_title)) {
                $typeItemSpecifics = "Hoodies, Sweatshirts";
                $sleeveItemSpecifics = "Length";
            }
            $item['ItemSpecifics'] = [
                'NameValueList' => [
                    ['Name' => 'Brand', 'Value' => $brand ? $brand : ucfirst($_SERVER['HTTP_HOST'])],
                    ['Name' => 'Style', 'Value' => 'Regular'],
                    ['Name' => 'Size Type', 'Value' => 'Regular'],
                    ['Name' => 'Sizes', 'Value' => implode($sizeArr, ', ')],
                    ['Name' => 'Material', 'Value' => '100% Cotton'],
                    ['Name' => 'Colors', 'Value' => implode($colorArr, ', ')],
                    ['Name' => 'Sleeve Length', 'Value' => $sleeveItemSpecifics],
                    ['Name' => 'Pattern', 'Value' => 'Does not apply'],
                    ['Name' => 'Fit', 'Value' => 'Regular'],
                    ['Name' => 'Neckline', 'Value' => 'Round Neck'],
                    ['Name' => 'Features', 'Value' => 'Feature'],
                    ['Name' => 'Type', 'Value' => $typeItemSpecifics],
                    ['Name' => 'Features', 'Value' => 'Feature'],
                    ['Name' => 'Department', 'Value' => 'Men, Women, Youth'],
                    ['Name' => 'Vintage', 'Value' => 'Yes'],
                    ['Name' => 'Occasion', 'Value' => 'Does not apply'],
                    ['Name' => 'Sleeve Type', 'Value' => $sleeveItemSpecifics]
                ]
            ];
            try {
                $data = $settings->ebay_conf($account_id);

                $eBayConfig = $data['eBayConfig'];
                $client = $data['client'];
                if ($client) {
                    /**for get ebay category condition and variations **/

                    $params = [
                        'Version' => 891,
                        'ErrorLanguage' => 'en_US',
                        'Item' => $item,
                    ];
                    // if ($_SERVER['HTTP_HOST'] == 'wordpress.local') dd($params);
                    $results = $client->AddItem($params);
                    if (isset($_GET['rul']) && $_GET['rul'] == 'su') {
                        var_dump($results);
                    }
                    // if ($_SERVER['HTTP_HOST'] == 'wordpress.local') dd($results);
                    if (isset($results->Ack) && ($results->Ack == 'Success' || $results->Ack == 'Warning')) {
                        global $wpdb;

                        $ebay_id = $results->ItemID;

                        $product_map_table = $wpdb->prefix.'woocommerce_ebay_product_map';

                        $result = $wpdb->insert(
                          $product_map_table,

                          array(
                              'ebay_product_id' => $ebay_id,
                              'ebay_product_name' => $_product_name,
                              'product_type' => $_product_type,
                              'store_product_id' => $product_id,
                              'store_category' => $_product_category,
                          )
                      );
                    } else {
                        // dump($results);
                    }
                    if ($cron) return $results;
                    $msg = [];
                    if (isset($results->Ack) && ($results->Ack == 'Success' || $results->Ack == 'Warning')) {
                        $msg[] = '<b>'.$product_title.' - Was exported!</b>';
                        do_action( 'ebay_send_slack', $product_title . "was exported to ebay" );
                    }
                    if (isset($results->Errors) && is_array($results->Errors)) {
                        foreach ($results->Errors as $err) {
                            if (isset($err->SeverityCode)) {
                                $msg[] = '<b>'.$product_title.' - </b>'.$err->LongMessage;
                            }
                            if (isset($err->SeverityCode)) {
                                do_action( 'ebay_send_slack',
                                          $product_title . " - " . $err->LongMessage,
                                          $err->SeverityCode);
                                
                            }
                        }
                    } elseif (isset($results->Errors)) {
                        if (isset($results->Errors->SeverityCode)) {
                            $msg[] = '<b>'.$product_title.' - </b>'.$results->Errors->LongMessage;
                        }
                        if (isset($results->Errors->SeverityCode)) {
                                do_action( 'ebay_send_slack',
                                          $product_title . " - " . $results->Errors->LongMessage,
                                          $results->Errors->SeverityCode);
                                
                            }
                    }
                    $return_data = array('Ack' => strtolower($results->Ack), 'msg' => $msg);
                    return $return_data;
                }
            } catch (\Exception $e) {
                $result['Ack'] = 'failure';
                $result['msg'] = $e->getMessage();
                return $result;
            }
        }

        public function getMappedCategory($cat_id)
        {
            global $wpdb;

            $final_cat_id = '';

            $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

            foreach ($cat_id as $c_id) {
                $id = $wpdb->get_row("SELECT ebay_category_id FROM $category_map_table WHERE category_id='$c_id'");

                if (!empty($id)) {
                    $final_cat_id = $id->ebay_category_id;

                    break;
                }
            }

            return $final_cat_id;
        }

        public function getMappedSpecifics($cat_id)
        {
            global $wpdb;

            $cat_specifics = '';

            $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

            foreach ($cat_id as $c_id) {
                $id = $wpdb->get_row("SELECT category_specification FROM $category_map_table WHERE category_id='$c_id'");

                if (!empty($id)) {
                    $cat_specifics = $id->category_specification;

                    break;
                }
            }

            return $cat_specifics;
        }
    }
}
