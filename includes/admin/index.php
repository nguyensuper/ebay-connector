<?php


class Helper {

    public $cat_id;

    public function getStoreBayCatMapData($ebayCatId) {

        global $wpdb;

        $table_name=$wpdb->prefix.'woocommerce_ebay_category_map';

        $res = $wpdb->get_row( "SELECT category_id FROM $table_name WHERE ebay_category_id = $ebayCatId" );

        if ( $res !== 0 && $res !== null ) {

            return $res->category_id;

        } else {

            return false;

        }

     }


    public function getWcCatId() {

        return get_option('ebay_default_category');

    }

}
