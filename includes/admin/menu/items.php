<?php

if( ! defined ( 'ABSPATH' ) )

  exit;


$hook =

add_menu_page(

  __("WooCommerce Multi eBay account", "wc_ebay"),

  __("WooCommerce Multi eBay account", "wc_ebay"),

  'manage_options',

  'manage-account',

  'wc_ebay_manage_account',

  'dashicons-share-alt',

  55

);

add_submenu_page(

  'manage-account',

  __("Ebay Messenger", "wc_ebay"),

  __("Ebay Messenger", "wc_ebay"),

  'manage_options',

  'wc-ebay-messgener',

  'ebay_messgener_page'

);

add_submenu_page(

  'manage-account',

  __("Ebay Settings", "wc_ebay"),

  __("Ebay Settings", "wc_ebay"),

  'manage_options',

  'wc-ebay-settings',

  'ebay_settings_page'

);

add_action('load-$hook', 'wc_ebay_screen_option');
