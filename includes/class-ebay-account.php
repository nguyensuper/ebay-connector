<?php

if( ! defined ( 'ABSPATH' ) )

  exit;

if ( ! class_exists( 'Ebay_Account' ) ) :

    class Ebay_Account{

        private $store_name;
        private $app_id;
        private $dev_id;
        private $cer_id;
        private $access_token;
        private $user_id;
        private $global_siteid;
        private $postal_code;
        private $ebay_paypal;
        private $brand;

        public function __construct($eid) {

           // need something

           global $wpdb;

           $table_name=$wpdb->prefix.'woocommerce_ebay_account';

           $store_data = $wpdb->get_row( "SELECT * FROM $table_name where id=$eid");

           if( $store_data ) {

            $this->store_name = $store_data->store_name;
            $this->app_id = $store_data->app_id;
            $this->dev_id = $store_data->dev_id;
            $this->cer_id = $store_data->cer_id;
            $this->user_id = $store_data->ebay_userid;
            $this->access_token = $store_data->access_token;
            $this->global_siteid = $store_data->ebay_global_site_id;
            $this->postal_code = $store_data->postal_code;
            $this->ebay_paypal = $store_data->ebay_paypal;
            $this->brand = $store_data->brand;

           }

        }

        public function get_store_name(){

            return $this->store_name;
        }
        public function get_access_token(){

            return $this->access_token;
        }
        public function get_app_id(){

            return $this->app_id;
        }
        public function get_dev_id(){

            return $this->dev_id;
        }
        public function get_cer_id(){

            return $this->cer_id;
        }
        public function get_ebay_paypal() {
            
            return $this->ebay_paypal;
        }
        public function get_brand() {
            
            return $this->brand;
        }
        public function get_user_id(){

            return $this->user_id;
        }
        public function get_ebay_global_site_id(){

            return $this->global_siteid;
        }

        public function get_postal_code(){

            return $this->postal_code;
        }

    }


endif;

?>