<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class EbayProtected
{
    public $ebay_mode;
    public $ebay_paypal_id;
    public $ebay_email_id;
    public $item_location_pincode;
    public $global_shipping;
    public $cron_interval;
    public $start_cron;
    public $ebay_default_category;
    public $ebay_default_stock;
    public $ebay_order_status;
    public $ebay_accounts;

    public $returns_policy_options_define;
    public $returns_policy_options_refundOption;

    public $returns_policy_options_return_days;
    public $returns_policy_options_pay_by;
    public $returns_policy_options_other_info;
    public $ebay_listing_options_duration;
    public $dispatch_options_time;
    public $shipping_options_ship_service_priority;
    public $international_shipping_options_ship_service_priority;
    public $shipping_options_ship_service;
    public $international_shipping_options_ship_service;
    public $shipping_options_ship_service_cost;
    public $international_shipping_options_ship_service_cost;
    public $shipping_options_ship_service_additional_cost;
    public $international_shipping_options_ship_service_additional_cost;
    public $shipping_options_ship_time_min;
    public $shipping_options_ship_time_max;
    public $shipping_options_ship_free;
    public $country_code_product_export;
    public $ship_country_code_product_export;

    public function __construct()
    {
        // setup default access token secret key and client key
    }

    public function get_ebay_mode()
    {
        if (!empty(get_option('ebaygeneral_settings_mode'))) {
            $this->ebay_mode = get_option('ebaygeneral_settings_mode');
        } else {
            $this->ebay_mode = '';
        }

        return $this->ebay_mode;
    }

    public function get_ebay_paypal_id()
    {
        if (!empty(get_option('ebay_paypal_id'))) {
            $this->ebay_paypal_id = get_option('ebay_paypal_id');
        } else {
            $this->ebay_paypal_id = '';
        }

        return $this->ebay_paypal_id;
    }

    public function get_ebay_email_id()
    {
        if (!empty(get_option('ebay_email_id'))) {
            $this->ebay_email_id = get_option('ebay_email_id');
        } else {
            $this->ebay_email_id = '';
        }

        return $this->ebay_email_id;
    }

    public function cron_interval()
    {
        if (!empty(get_option('cron_interval'))) {
            $this->cron_interval = get_option('cron_interval');
        } else {
            $this->cron_interval = '';
        }

        return $this->cron_interval;
    }

    public function get_default_cat()
    {
        if (!empty(get_option('ebay_default_category'))) {
            $this->ebay_default_category = get_option('ebay_default_category');
        } else {
            $this->ebay_default_category = '';
        }

        return $this->ebay_default_category;
    }

    public function get_default_stock()
    {
        if (!empty(get_option('ebay_default_stock'))) {
            $this->ebay_default_stock = get_option('ebay_default_stock');
        } else {
            $this->ebay_default_stock = '';
        }

        return $this->ebay_default_stock;
    }

    public function get_start_cron()
    {
        if (!empty(get_option('start_cron'))) {
            $this->start_cron = get_option('start_cron');
        } else {
            $this->start_cron = '';
        }

        return $this->start_cron;
    }

    public function get_returns_policy_options_define()
    {
        if (!empty(get_option('returns_policy_options_define'))) {
            $this->returns_policy_options_define = get_option('returns_policy_options_define');
        } else {
            $this->returns_policy_options_define = '';
        }

        return $this->returns_policy_options_define;
    }

    public function get_returns_policy_options_refundOption()
    {
        if (!empty(get_option('returns_policy_options_refundOption'))) {
            $this->returns_policy_options_refundOption = get_option('returns_policy_options_refundOption');
        } else {
            $this->returns_policy_options_refundOption = '';
        }

        return $this->returns_policy_options_refundOption;
    }

    public function get_returns_policy_options_return_days()
    {
        if (!empty(get_option('returns_policy_options_return_days'))) {
            $this->returns_policy_options_return_days = get_option('returns_policy_options_return_days');
        } else {
            $this->returns_policy_options_return_days = '';
        }

        return $this->returns_policy_options_return_days;
    }

    public function get_ebay_order_status()
    {
        if (!empty(get_option('ebay_order_status'))) {
            $this->ebay_order_status = get_option('ebay_order_status');
        } else {
            $this->ebay_order_status = '';
        }

        return $this->ebay_order_status;
    }

    public function get_ebay_accounts()
    {
        if (!empty(get_option('ebay_accounts'))) {
            $this->ebay_accounts = get_option('ebay_accounts');
        } else {
            $this->ebay_accounts = '';
        }

        return $this->ebay_accounts;
    }

    public function get_ebay_returns_policy_options_pay_by()
    {
        if (!empty(get_option('returns_policy_options_pay_by'))) {
            $this->returns_policy_options_pay_by = get_option('returns_policy_options_pay_by');
        } else {
            $this->returns_policy_options_pay_by = '';
        }

        return $this->returns_policy_options_pay_by;
    }

    public function get_ebay_returns_policy_options_other_info()
    {
        if (!empty(get_option('returns_policy_options_other_info'))) {
            $this->returns_policy_options_other_info = get_option('returns_policy_options_other_info');
        } else {
            $this->returns_policy_options_other_info = '';
        }

        return $this->returns_policy_options_other_info;
    }

    public function get_ebay_ebay_listing_options_duration()
    {
        if (!empty(get_option('ebay_listing_options_duration'))) {
            $this->ebay_listing_options_duration = get_option('ebay_listing_options_duration');
        } else {
            $this->ebay_listing_options_duration = '';
        }

        return $this->ebay_listing_options_duration;
    }

    public function get_ebay_dispatch_options_time()
    {
        if (!empty(get_option('dispatch_options_time'))) {
            $this->dispatch_options_time = get_option('dispatch_options_time');
        } else {
            $this->dispatch_options_time = '';
        }

        return $this->dispatch_options_time;
    }

    public function get_ebay_shipping_options_ship_service_priority()
    {
        if (!empty(get_option('shipping_options_ship_service_priority'))) {
            $this->shipping_options_ship_service_priority = get_option('shipping_options_ship_service_priority');
        } else {
            $this->shipping_options_ship_service_priority = '';
        }

        return $this->shipping_options_ship_service_priority;
    }

    public function get_ebay_international_shipping_options_ship_service_priority()
    {
        if (!empty(get_option('international_shipping_options_ship_service_priority'))) {
            $this->international_shipping_options_ship_service_priority = get_option('international_shipping_options_ship_service_priority');
        } else {
            $this->international_shipping_options_ship_service_priority = '';
        }

        return $this->international_shipping_options_ship_service_priority;
    }

    public function get_ebay_shipping_options_ship_service()
    {
        if (!empty(get_option('shipping_options_ship_service'))) {
            $this->shipping_options_ship_service = get_option('shipping_options_ship_service');
        } else {
            $this->shipping_options_ship_service = '';
        }

        return $this->shipping_options_ship_service;
    }

    public function get_ebay_international_shipping_options_ship_service()
    {
        if (!empty(get_option('international_shipping_options_ship_service'))) {
            $this->international_shipping_options_ship_service = get_option('international_shipping_options_ship_service');
        } else {
            $this->international_shipping_options_ship_service = '';
        }

        return $this->international_shipping_options_ship_service;
    }

    public function get_ebay_shipping_options_ship_service_cost()
    {
        if (!empty(get_option('shipping_options_ship_service_cost'))) {
            $this->shipping_options_ship_service_cost = get_option('shipping_options_ship_service_cost');
        } else {
            $this->shipping_options_ship_service_cost = '';
        }

        return $this->shipping_options_ship_service_cost;
    }

    public function get_ebay_international_shipping_options_ship_service_cost()
    {
        if (!empty(get_option('international_shipping_options_ship_service_cost'))) {
            $this->international_shipping_options_ship_service_cost = get_option('international_shipping_options_ship_service_cost');
        } else {
            $this->international_shipping_options_ship_service_cost = '';
        }

        return $this->international_shipping_options_ship_service_cost;
    }

    public function get_ebay_shipping_options_ship_service_additional_cost()
    {
        if (!empty(get_option('shipping_options_ship_service_additional_cost'))) {
            $this->shipping_options_ship_service_additional_cost = get_option('shipping_options_ship_service_additional_cost');
        } else {
            $this->shipping_options_ship_service_additional_cost = '';
        }

        return $this->shipping_options_ship_service_additional_cost;
    }

    public function get_ebay_international_shipping_options_ship_service_additional_cost()
    {
        if (!empty(get_option('international_shipping_options_ship_service_additional_cost'))) {
            $this->international_shipping_options_ship_service_additional_cost = get_option('international_shipping_options_ship_service_additional_cost');
        } else {
            $this->international_shipping_options_ship_service_additional_cost = '';
        }

        return $this->international_shipping_options_ship_service_additional_cost;
    }

    public function get_ebay_shipping_options_ship_time_min()
    {
        if (!empty(get_option('shipping_options_ship_time_min'))) {
            $this->shipping_options_ship_time_min = get_option('shipping_options_ship_time_min');
        } else {
            $this->shipping_options_ship_time_min = '';
        }

        return $this->shipping_options_ship_time_min;
    }

    public function get_ebay_shipping_options_ship_time_max()
    {
        if (!empty(get_option('shipping_options_ship_time_max'))) {
            $this->shipping_options_ship_time_max = get_option('shipping_options_ship_time_max');
        } else {
            $this->shipping_options_ship_time_max = '';
        }

        return $this->shipping_options_ship_time_max;
    }

    public function get_ebay_shipping_options_ship_free()
    {
        if (get_option('shipping_options_ship_free') == 0) {
            $this->shipping_options_ship_free = get_option('shipping_options_ship_free');
        } else {
            $this->shipping_options_ship_free = '';
        }

        return $this->shipping_options_ship_free;
    }

    public function get_country_code_product_export()
    {
        if (get_option('country_code_product_export') == 0) {
            $this->country_code_product_export = get_option('countryCodeType');
        } else {
            $this->country_code_product_export = '';
        }

        return $this->country_code_product_export;
    }

    public function get_ship_country_code_product_export()
    {
        if (get_option('shipCountryCodeType') == 0) {
            $this->ship_country_code_product_export = get_option('shipCountryCodeType');
        } else {
            $this->ship_country_code_product_export = '';
        }

        return $this->ship_country_code_product_export;
    }

    public function get_item_location_pincode()
    {
        if (get_option('item_location_pincode') == 0) {
            $this->item_location_pincode = get_option('item_location_pincode');
        } else {
            $this->item_location_pincode = '';
        }

        return $this->item_location_pincode;
    }

    public function get_global_shipping()
    {
        if (get_option('global_shipping') == '0') {
            $this->global_shipping = false;
        } else {
            $this->global_shipping = true;
        }

        return $this->global_shipping;
    }
    
    public function get_app_id()
    {
        return get_option('ebay_app_id');
    }
    
    public function get_dev_id()
    {
        return get_option('ebay_dev_id');
    }
    
    public function get_cert_id()
    {
        return get_option('ebay_cert_id');
    }
    
    public function get_ru_name()
    {
        return get_option('ebay_ru_name');
    }
}

new EbayProtected();
