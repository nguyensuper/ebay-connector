 <div id="ebay_account" class="panel woocommerce_options_panel ebay_fields">

        <?php

        do_action( 'save_ebay_account', $_POST );?>

        <?php
            $domain = 'https://auth.ebay.com';
            $domainapi = 'https://api.ebay.com';
            if (get_option('ebaygeneral_settings_mode') == 1) {
                $domain = 'https://auth.sandbox.ebay.com';
                // $domainapi = 'https://api.sandbox.ebay.com';
            }
            
              $store_name = $ebay_access_token = $ebay_account = $ebay_app_id = $ebay_certication_id = $ebay_dev_id = $ebay_global_site_id = $ebay_userid = $postal_code = $ebay_paypal = $brand = '';

              
                if (isset($_GET['code']) && $_GET['code']) {
                    $ebay_access_token=$_GET['code'];
                }
                if( !empty( $eid ) ) {
                    $ebay_account = new Ebay_Account($eid);

                    $store_name=$ebay_account->get_store_name();
                    $ebay_userid=$ebay_account->get_user_id();
                    $ebay_app_id=$ebay_account->get_app_id();
                    $ebay_dev_id=$ebay_account->get_dev_id();
                    $postal_code=$ebay_account->get_postal_code();
                    $ebay_global_site_id=$ebay_account->get_ebay_global_site_id();
                    $ebay_certication_id=$ebay_account->get_cer_id();
                    if (!isset($_GET['code']) || !$_GET['code']) {
                        $ebay_access_token=$ebay_account->get_access_token();
                    }
                    $ebay_paypal = $ebay_account->get_ebay_paypal();
                    $brand = $ebay_account->get_brand();
                }

                $options= array(
                    0=>'eBay United States',
                    2=>'eBay Canada (English)',
                    3=>'eBay UK',
                    15=>'eBay Australia',
                    16=>'eBay Austria',
                    23=>'eBay Belgium (French)',
                    71=>'eBay France',
                    77=>'eBay Germany',
                    100=>'eBay Motors',
                    101=>'eBay Italy',
                    123=>'eBay Belgium (Dutch)',
                    146=>'eBay Netherlands',
                    186=>'eBay Spain',
                    193=>'eBay Switzerland',
                    201=>'eBay Singapore',
                    203=>'eBay India',
                    205=>'eBay Ireland',
                    207=>'eBay Malaysia',
                    210=>'eBay Canada (French)',
                    211=>'eBay Philippines'
                );

                    ?>
                <div class="ebay_account_fields ">
                        <!--// Store name-->
                        <label><?php echo __( 'Store Name', 'woocommerce' ); ?></label>
                        <input  type="text" value=" <?php echo $store_name ? $store_name : '' ; ?> " <?php echo ! empty( $eid ) ? 'readonly="true"' : ''; ?> name="ebay_store_name" id="ebay_store_name">
                        <?php echo __( '<i>Name of the store on ebay.</i>', 'woocommerce' ); ?>

                        <!--// Type-->
                        <label for="ebay_global_sites"><?php echo __( 'Global Site', 'woocommerce' ); ?></label>
                        <select id="ebay_global_sites" id="ebay_global_sites" name="ebay_global_sites" >

                            <?php

                            foreach( $options as $option_key => $option_val ) :  ?>

                                <option value="<?php echo $option_key; ?>" <?php if($ebay_global_site_id==$option_key)echo "selected";?>><?php echo $option_val; ?></option>

                                <?php endforeach; ?>

                        </select>
                        <i><?php echo  __( 'Select your ebay region', 'woocommerce' ); ?></i>
                        
                        <label for="shop_postal_code"><?php echo __( 'Postal Code', 'woocommerce' ); ?></label>
                        <input  type="text" value="<?php echo $postal_code?$postal_code:'';?>" name="shop_postal_code" id="shop_postal_code">

                        <label for="ebay_userid"><?php echo __( 'Ebay User Id', 'woocommerce' ); ?></label>
                        <input  type="text" value="<?php echo $ebay_userid?$ebay_userid:'';?>" name="ebay_userid" id="ebay_userid" readonly="true">
                        <?php echo __( '<i>need To setup ebay user id.</i>', 'woocommerce' ); ?>

                        <label for="ebay_access_token"><?php echo __( 'Ebay Authentication Token', 'woocommerce' ); ?></label>
                        <textarea  type="text" name="ebay_access_token" id="ebay_access_token"><?php echo $ebay_access_token?$ebay_access_token:'';?></textarea>
                        <label for="ebay_access_token"></label>
                        <a href="<?php echo $domain ?>/oauth2/authorize?client_id=<?php echo get_option('ebay_app_id') ? get_option('ebay_app_id') : ''; ?>&response_type=code&redirect_uri=<?php echo get_option('ebay_ru_name') ? get_option('ebay_ru_name') : ''; ?>&scope=<?php echo $domainapi ?>/oauth/api_scope <?php echo $domainapi ?>/oauth/api_scope/sell.marketing.readonly <?php echo $domainapi ?>/oauth/api_scope/sell.marketing <?php echo $domainapi ?>/oauth/api_scope/sell.inventory.readonly <?php echo $domainapi ?>/oauth/api_scope/sell.inventory <?php echo $domainapi ?>/oauth/api_scope/sell.account.readonly <?php echo $domainapi ?>/oauth/api_scope/sell.account <?php echo $domainapi ?>/oauth/api_scope/sell.fulfillment.readonly <?php echo $domainapi ?>/oauth/api_scope/sell.fulfillment <?php echo $domainapi ?>/oauth/api_scope/sell.analytics.readonly <?php echo $domainapi ?>/oauth/api_scope/sell.finances&state=<?php echo ($_GET['aid'] ?? '') ?>">Get Token</a>
                        <label for="ebay_dev_id"><?php echo __( 'Developer Id', 'woocommerce' ); ?></label>
                        <input  type="text" value="<?php echo get_option('ebay_dev_id') ? get_option('ebay_dev_id') : ''; ?>" name="ebay_dev_id" id="ebay_dev_id" readonly="true">

                        <label for="ebay_app_id"><?php echo __( 'Application Id', 'woocommerce' ); ?></label>
                        <input  type="text" value="<?php echo get_option('ebay_app_id') ? get_option('ebay_app_id') : ''; ?>" name="ebay_app_id" id="ebay_app_id" readonly="true">

                        <label for="ebay_certication_id"><?php echo __( 'Certification Id', 'woocommerce' ); ?></label>
                        <input  type="text" value="<?php echo get_option('ebay_cert_id') ? get_option('ebay_cert_id') : ''; ?>" name="ebay_certication_id" id="ebay_certication_id" readonly="true">
                        
                        <label for="ebay_paypal"><?php echo __( 'Paypal email', 'woocommerce' ); ?></label>
                        <input  type="text" value="<?php echo $ebay_paypal?$ebay_paypal:'';?>" name="ebay_paypal" id="ebay_paypal">
                        
                        <label for="brand"><?php echo __( 'Brand', 'woocommerce' ); ?></label>
                        <input  type="text" value="<?php echo $brand?$brand:'';?>" name="brand" id="brand">

                   </div>

</div>