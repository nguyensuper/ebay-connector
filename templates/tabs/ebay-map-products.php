<?php

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WP_List_Table')) {
    require_once ABSPATH.'wp-admin/includes/class-wp-list-table.php';
}

if (!class_exists('WC_Ebay_Map_Products')) {
    class WC_Ebay_Map_Products extends WP_List_Table
    {
        public function __construct()
        {
            parent::__construct(array(
        'singular' => 'Ebay Product List',
        'plural' => 'Ebay Product List',
        'ajax' => false,
      ));
        }

        public function prepare_items()
        {
            global $wpdb;

            $columns = $this->get_columns();

            $sortable = $this->get_sortable_columns();

            $hidden = $this->get_hidden_columns();

            $this->process_bulk_action();

            $data = $this->table_data();

            $totalItems = count($data);

            $user = get_current_user_ID();

            $screen = get_current_screen();

            $perpage = $this->get_items_per_page('rule_per_page', 20);

            $this->_column_headers = array($columns, $hidden, $sortable);

            if (empty($per_page) || $per_page < 1) {
                $per_page = $screen->get_option('per_page', 'default');
            }

            function usort_preorder($a, $b)
            {
                $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'ebay_product_id';

                $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';

                $result = strcmp($a[$orderby], $b[$orderby]);

                return ($order === 'asc') ? $result : -$result;
            }

            usort($data, 'usort_preorder');

            $totalPages = ceil($totalItems / $perpage);

            $currentPage = $this->get_pagenum();

            $data = array_slice($data, (($currentPage - 1) * $perpage), $perpage);

            $this->set_pagination_args(array(
        'total_items' => $totalItems,

        'total_pages' => $totalPages,

        'per_page' => $perpage,
      ));

            $this->items = $data;
        }

        /**
         * Define the columns that are going to be used in the table.
         *
         * @return array $columns, the array of columns to use with the table
         */
        public function get_columns()
        {
            return $columns = array(
        'cb' => '<input type="checkbox" />',

        'store_product_id' => __('Store Product Id', 'wc_ebay'),

        'ebay_product_name' => __('Product Name', 'wc_ebay'),

        'product_type' => __('Product Type', 'wc_ebay'),

        'ebay_product_id' => __('Ebay Product Id', 'wc_ebay'),

        'category_name' => __('Category Name', 'wc_ebay'),
      );
        }

        public function column_default($item, $column_name)
        {
            switch ($column_name) {
        case 'store_product_id':

        case 'ebay_product_name':

        case 'product_type':

        case 'ebay_product_id':

        case 'category_name':

          return $item[$column_name];

        default:

          return print_r($item, true);
      }
        }

        /**
         * Decide which columns to activate the sorting functionality on.
         *
         * @return array $sortable, the array of columns that can be sorted by the user
         */
        public function get_sortable_columns()
        {
            return $sortable = array(
        'store_product_id' => array('store_product_id', true),

        'ebay_product_name' => array('ebay_product_name', true),
      );
        }

        public function get_hidden_columns()
        {
            return array();
        }

        public function column_cb($item)
        {
            return sprintf('<input type="checkbox" id="item_id" name="item_id[]" value="%s" />', $item['store_product_id']);
        }

        private function table_data()
        {
            global $wpdb;

            $table_name = $wpdb->prefix.'woocommerce_ebay_product_map';
            $data = array();
            $ebay_product_id = array();
            $store_product_id = array();
            $ebay_product_name = array();
            $product_type = array();
            $store_category = array();

            $s = isset($_POST['s']) ? $_POST['s'] : '';

            if (!empty($s)) {
                $res = $wpdb->get_results("SELECT * FROM $table_name where ebay_product_name LIKE '%$s%'");
            } else {
                $res = $wpdb->get_results("SELECT * FROM $table_name");
            }

            $i = 0;

            if (!empty($res)) {
                foreach ($res as $key => $value) {
                    $ebay_product_id[] = $value->ebay_product_id;

                    $store_product_id[] = $value->store_product_id;

                    $ebay_product_name[] = $value->ebay_product_name;

                    $product_type[] = $value->product_type;

                    $store_category[] = $value->store_category;

                    $data[] = array(
            'store_product_id' => $store_product_id[$i],

            'ebay_product_name' => $ebay_product_name[$i],

            'product_type' => $product_type[$i],

            'ebay_product_id' => $ebay_product_id[$i],

            'category_name' => $store_category[$i],
          );

                    ++$i;
                }
            }

            return $data;
        }

        public function get_bulk_actions()
        {
            $actions = array(
        'update_to_ebay' => 'Update to ebay Store',
        'update_from_ebay' => 'Update From Ebay Store',
        'delete' => 'Delete',
      );

            return $actions;
        }

        public function process_bulk_action()
        {
            global $wpdb;
            $update_to_ebay = new Update_To_Ebay();
            $update_from_ebay = new Update_From_Ebay();
            $return_data = array();
            if ($this->current_action() == 'update_to_ebay') {
                $wk_data = isset($_POST['item_id']) ? $_POST['item_id'] : '';

                if (!empty($wk_data)) {
                    foreach ($wk_data as $data) {
                        $temp_return_data = $update_to_ebay->ebay_update(intval($data));

                        if (!empty($temp_return_data)) {
                            $return_data[] = $temp_return_data;
                        }
                    }

                    if (!empty($return_data)) {
                        foreach ($return_data as $value) {
                            if ($value['Ack'] == 'success') :

                foreach ($value['msg'] as $msg) : ?>

                <div class="notice notice-success is-dismissible">

                  <p><?php echo $msg.'updated successfully to Ebay.!'; ?></p>
                  <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                </div>

              <?php endforeach; else :

            foreach ($value['msg'] as $msg) : ?>

                <div class="notice notice-error is-dismissible">
                  <p><?php echo $msg; ?></p>
                  <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                </div>

              <?php

            endforeach;

                            endif;
                        }
                    }
                }
            }
            if ($this->current_action() == 'update_from_ebay') {
                $wk_data = isset($_POST['item_id']) ? $_POST['item_id'] : '';

                if (!empty($wk_data)) {
                    foreach ($wk_data as $data) {
                        $temp_return_data = $update_from_ebay->woo_update(intval($data));

                        if (!empty($temp_return_data)) {
                            $return_data[] = $temp_return_data;
                        }
                    }

                    if (!empty($return_data)) {
                        foreach ($return_data as $value) {
                            if ($value['Ack'] == 'success') :

                foreach ($value['msg'] as $msg) : ?>

                <div class="notice notice-success is-dismissible">

                  <p><?php echo $msg.'updated successfully '; ?></p>
                  <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                </div>

              <?php endforeach; else :

            foreach ($value['msg'] as $msg) : ?>

                <div class="notice notice-error is-dismissible">
                  <p><?php echo $msg; ?></p>
                  <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                </div>

              <?php

            endforeach;

                            endif;
                        }
                    }
                }
            } elseif ($this->current_action() == 'delete') {
                $count = 0;

                if (is_array($_POST['item_id'])) {
                    foreach ($_POST['item_id'] as $item) :

        wp_delete_post(intval($item));

                    ++$count;

                    endforeach; ?>
        <div class="notice notice-success my-acf-notice is-dismissible">
          <p><?php _e($count.' product(s) deleted successfully.', 'wc_ebay'); ?></p>
        </div>

      <?php
                }
            } elseif (isset($_GET['item_id']) && isset($_GET['perform']) && $_GET['perform'] == 'delete' && isset($_GET['tab']) && $_GET['tab'] == 'map_product') {
                $count = 0;

                $item = intval($_GET['item_id']);

                wp_delete_post($item);

                ++$count; ?>
      <div class="notice notice-success my-acf-notice is-dismissible">
        <p><?php _e($count.' product deleted successfully.', 'wc_ebay'); ?></p>
      </div>

    <?php
            }
        }

        public function column_store_product_id($item)
        {
            $aid = isset($_GET['aid']) ? $_GET['aid'] : '';

            $actions = array(
    'edit' => sprintf('<a href="post.php?post=%d&action=edit">Edit</a>', $item['store_product_id']),

    'delete' => sprintf('<a href="admin.php?page=manage-account&action=account&aid=%d&tab=map_product&item_id=%d&perform=delete">Delete</a>', $aid, $item['store_product_id']),
  );

            return sprintf('%1$s %2$s', $item['store_product_id'], $this->row_actions($actions));
        }
    }
}

$map_products = new WC_Ebay_Map_Products();
if (isset($_POST['s'])) {
    $map_products->prepare_items($_POST['s']);
} else {
    $map_products->prepare_items();
}

?>

<div class="ebay-tab-wrap">

  <div class="ebay-sync main-syncebay">

    <input type="hidden" name="ebay_accid" value="<?php echo $eid; ?>" />

    <button id="open_import_pro_from_ebay" title="Import Product From eBay" class="action-split button button-primary" data-ui-id="import-pro-from-ebay">Import Product From eBay</button>

    <button id="profiler_for_create_pro" title="Create Imported Products" class="action-split button button-primary" data-ui-id="profiler-for-create-pro">Create Imported Products</button>

  </div>

  <form method="POST">

    <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />

    <?php

    $map_products->search_box('Search', 'search-id');

    $map_products->display();

    ?>

  </form>

</div>