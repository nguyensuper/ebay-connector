<?php

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WP_List_Table')) {
    require_once ABSPATH.'wp-admin/includes/class-wp-list-table.php';
}

if (!class_exists('WC_Ebay_Export_Product')) {
    class WC_Ebay_Export_Product extends WP_List_Table
    {
        public function __construct()
        {
            parent::__construct(array(
        'singular' => 'Ebay Export Products',
        'plural' => 'Ebay Export Products',
        'ajax' => false,
      ));
        }

        public function prepare_items()
        {
            global $wpdb;

            $columns = $this->get_columns();

            $sortable = $this->get_sortable_columns();

            $hidden = $this->get_hidden_columns();

            $this->process_bulk_action();

            $data = $this->table_data();

            $totalItems = count($data);

            $user = get_current_user_ID();

            $screen = get_current_screen();

            $perpage = $this->get_items_per_page('rule_per_page', 20);

            $this->_column_headers = array($columns, $hidden, $sortable);

            if (empty($per_page) || $per_page < 1) {
                $per_page = $screen->get_option('per_page', 'default');
            }

            function usort_ereorder($a, $b)
            {
                $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'product_id';

                $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';

                $result = strcmp($a[$orderby], $b[$orderby]);

                return ($order === 'asc') ? $result : -$result;
            }

            usort($data, 'usort_ereorder');

            $totalPages = ceil($totalItems / $perpage);

            $currentPage = $this->get_pagenum();

            $data = array_slice($data, (($currentPage - 1) * $perpage), $perpage);

            $this->set_pagination_args(array(
        'total_items' => $totalItems,

        'total_pages' => $totalPages,

        'per_page' => $perpage,
      ));

            $this->items = $data;
        }

        /**
         * Define the columns that are going to be used in the table.
         *
         * @return array $columns, the array of columns to use with the table
         */
        public function get_columns()
        {
            return $columns = array(
        'cb' => '<input type="checkbox" />',

        'product_id' => __('Product Id', 'wc_ebay'),

        'product_name' => __('Product Name', 'wc_ebay'),

        'product_type' => __('Product Type', 'wc_ebay'),

        'sku' => __('Product Sku', 'wc_ebay'),
      );
        }

        public function column_default($item, $column_name)
        {
            switch ($column_name) {
        case 'product_id':

        case 'product_name':

        case 'product_type':

        case 'sku':

          return $item[$column_name];

        default:

          return print_r($item, true);
      }
        }

        /**
         * Decide which columns to activate the sorting functionality on.
         *
         * @return array $sortable, the array of columns that can be sorted by the user
         */
        public function get_sortable_columns()
        {
            return $sortable = array(
        'product_id' => array('product_id', true),
      );
        }

        public function get_hidden_columns()
        {
            return array();
        }

        public function column_cb($item)
        {
            return sprintf('<input type="checkbox" id="store_%s" name="item_id[]" value="%s" />', $item['product_id'], $item['product_id']);
        }

        public function get_bulk_actions()
        {
            $actions = array(
        'export_to_ebay' => 'Export to ebay',
        'delete' => 'Delete',
      );

            return $actions;
        }

        public function process_bulk_action()
        {
            $export_to_ebay = new Export_To_Ebay();
            $return_data = array();
            if ($this->current_action() == 'export_to_ebay') {
                $wk_data = isset($_POST['item_id']) ? $_POST['item_id'] : '';

                if (!empty($wk_data)) {
                    foreach ($wk_data as $data) {
                        $temp_return_data = $export_to_ebay->ebay_export(intval($data));

                        if (!empty($temp_return_data)) {
                            $return_data[] = $temp_return_data;
                        }
                    }

                    if (!empty($return_data)) {
                        foreach ($return_data as $value) {
                            if ($value['Ack'] == 'success') :

                foreach ($value['msg'] as $msg) : ?>

                <div class="notice notice-success is-dismissible">

                  <p><?php echo $msg.'exported successfully to Ebay.!'; ?></p>
                  <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                </div>

              <?php endforeach; else :

            foreach ($value['msg'] as $msg) : ?>

                <div class="notice notice-<?php echo $value['Ack'] == 'warning' ? 'warning' : 'error' ?> is-dismissible">
                  <p><?php echo $msg; ?></p>
                  <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
                </div>

              <?php

            endforeach;

                            endif;
                        }
                    }
                }
            } elseif ($this->current_action() == 'delete') {
                $count = 0;
                if (is_array($_POST['item_id'])) {
                    foreach ($_POST['item_id'] as $item) :

        wp_delete_post(intval($item));

                    ++$count;

                    endforeach; ?>
        <div class="notice notice-success my-acf-notice is-dismissible">
          <p><?php _e($count.' product(s) deleted successfully.', 'wc_ebay'); ?></p>
        </div>

      <?php
                }
            } elseif (isset($_GET['item_id']) && isset($_GET['perform']) && $_GET['perform'] == 'delete' && isset($_GET['tab']) && $_GET['tab'] == 'export_to_ebay') {
                $count = 0;

                $item = intval($_GET['item_id']);

                wp_delete_post($item);
                ++$count; ?>
      <div class="notice notice-success my-acf-notice is-dismissible">
        <p><?php _e($count.' product deleted successfully.', 'wc_ebay'); ?></p>
      </div>

    <?php
            }
        }

        private function table_data()
        {
            global $wpdb;

            $table_name = $wpdb->prefix.'woocommerce_ebay_product_map';

            $data = array();
            $id = array();
            $product_name = array();
            $product_type = array();
            $sku = array();
            $s = isset($_POST['s']) ? $_POST['s'] : '';

            $args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'post_status' => 'publish',
  );

            if (!empty($s)) {
                $args['s'] = $s;
            }

            $loop = new WP_Query($args);
            $i = 0;
            while ($loop->have_posts()) : $loop->the_post();

                $product_id = get_the_ID();

                $res = $wpdb->get_results("SELECT store_product_id FROM $table_name WHERE store_product_id = $product_id");
                $ebay_products = wp_list_pluck($res, 'store_product_id');

                if (empty($ebay_products) || !in_array($product_id, $ebay_products)) {
                    $id[] = $product_id;

                    $_product = wc_get_product($product_id);

                    $product_name[] = get_the_title();

                    $product_type[] = $_product->get_type();

                    $sku[] = $_product->get_sku();

                    $data[] = array(
                        'product_id' => $id[$i],

                        'product_name' => $product_name[$i],

                        'product_type' => $product_type[$i],

                        'sku' => $sku[$i],
                    );

                    ++$i;
                }

            endwhile;

            return $data;
        }

        public function column_product_id($item)
        {
            $aid = !empty($_GET['aid']) ? $_GET['aid'] : '';

            $actions = array(
    'edit' => sprintf('<a href="post.php?post=%d&action=edit">Edit</a>', $item['product_id']),

    'delete' => sprintf('<a href="admin.php?page=manage-account&action=account&aid=%d&tab=export_to_ebay&item_id=%d&perform=delete">Delete</a>', $aid, $item['product_id']),
  );

            return sprintf('%1$s %2$s', $item['product_id'], $this->row_actions($actions));
        }
    }
}

$export_product = new WC_Ebay_Export_Product();

if (isset($_POST['s'])) {
    $export_product->prepare_items($_POST['s']);
} else {
    $export_product->prepare_items();
}

?>

<div class="ebay-tab-wrap">

  <div class="ebay-sync">

    <input type="hidden" name="ebay_accid" value="<?php echo $eid; ?>" />

    <h2 id="ebay-expopro">Export Products To Ebay</h2>

  </div>

  <form method="POST">

    <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />

    <?php

    $export_product->search_box('Search Product', 'search-id');

    $export_product->display();

    ?>

  </form>

</div>