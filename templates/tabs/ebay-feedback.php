<?php

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('WP_List_Table')) {
    require_once ABSPATH.'wp-admin/includes/class-wp-list-table.php';
}

if (!class_exists('WC_Ebay_Feedback')) {
    class WC_Ebay_Feedback extends WP_List_Table
    {
        public $accounts = [];
        public function __construct()
        {
            parent::__construct(array(
                'singular' => 'Feedbacks To Ebay',
                'plural' => 'Feedbacks To Ebay',
                'ajax' => false,
            ));
        }
        
        public function getAllAccount() {
            global $wpdb;
            $table_acc_name = $wpdb->prefix.'woocommerce_ebay_account';
            $queryAcc       = "SELECT * FROM $table_acc_name";
            $res            = $wpdb->get_results($queryAcc);
            $accounts       = [];
            foreach ($res as $key => $acc) {
                $accounts[$acc->id] = $acc;
            }
            $this->accounts = $accounts;
        }
        
        public function time_elapsed_string( $datetime, $full = false ) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

        public function prepare_items()
        {
            global $wpdb;

            $this->getAllAccount();
            
            $columns = $this->get_columns();

            $sortable = $this->get_sortable_columns();

            $hidden = $this->get_hidden_columns();

            $this->process_bulk_action();

            $data = $this->table_data();

            $totalItems = count($data);

            $user = get_current_user_ID();

            $screen = get_current_screen();

            $perpage = $this->get_items_per_page('rule_per_page', 20);

            $this->_column_headers = array($columns, $hidden, $sortable);

            if (empty($per_page) || $per_page < 1) {
                $per_page = $screen->get_option('per_page', 'default');
            }

            function usort_ereorder($a, $b)
            {
                $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'ebay_messenger_received_at';

                $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';

                $result = strcmp($a[$orderby], $b[$orderby]);

                return ($order === 'asc') ? $result : -$result;
            }

            usort($data, 'usort_ereorder');

            $totalPages = ceil($totalItems / $perpage);

            $currentPage = $this->get_pagenum();

            $data = array_slice($data, (($currentPage - 1) * $perpage), $perpage);

            $this->set_pagination_args(array(
                'total_items' => $totalItems,

                'total_pages' => $totalPages,

                'per_page' => $perpage,
            ));

            $this->items = $data;
        }

        /**
         * Define the columns that are going to be used in the table.
         *
         * @return array $columns, the array of columns to use with the table
         */
        public function get_columns()
        {
            return $columns = array(
                'cb' => '<input type="checkbox" />',

                'id' => __('Id', 'wc_ebay'),

                'account_id' => __('Store', 'wc_ebay'),
                
                'ebay_sender' => __('Sender', 'wc_ebay'),

                'ebay_messenger_subject' => __('Subject', 'wc_ebay'),
                
                'status' => __('Status', 'wc_ebay'),

                'ebay_messenger_received_at' => __('Time', 'wc_ebay'),
            );
        }

        public function column_default($item, $column_name)
        {
            switch ($column_name) {
                case 'id':
                case 'ebay_sender':
                case 'ebay_messenger_subject':
                case 'status':
                case 'ebay_messenger_received_at':
                default:

            return print_r($item[$column_name], true);
            }
        }

        /**
         * Decide which columns to activate the sorting functionality on.
         *
         * @return array $sortable, the array of columns that can be sorted by the user
         */
        public function get_sortable_columns()
        {
            return $sortable = array(
                'id' => array('id', true),
                'ebay_messenger_received_at' => array('ebay_messenger_received_at', true),
            );
        }

        public function get_hidden_columns()
        {
            return array();
        }

        public function column_cb($item)
        {
            return sprintf('<input type="checkbox" id="store_%s" name="item_id[]" value="%s" />', $item['id'], $item['id']);
        }

        public function get_bulk_actions()
        {
            $actions = array(
                'read' => 'Read All',
            );

            return $actions;
        }

        public function process_bulk_action()
        {
            
            $export_to_ebay = new Export_To_Ebay();
            $return_data = array();
            if ($this->current_action() == 'read') {
                $count = 0;
                if (is_array($_POST['item_id'])) {
                     global $wpdb;
                    foreach ($_POST['item_id'] as $item) :


                      $table_name = $wpdb->prefix.'woocommerce_ebay_messenger';
                      $wpdb->update( $table_name, array('status' => 1), array( 'id' => $item, 'status' => 0 ) );

                    ++$count;

                    endforeach; ?>
        <div class="notice notice-success my-acf-notice is-dismissible">
          <p><?php _e($count.' messages readed successfully.', 'wc_ebay'); ?></p>
        </div>

      <?php
                }
            }
            /*
            if ($this->current_action() == 'delete') {
                $count = 0;
                if (is_array($_POST['item_id'])) {
                     global $wpdb;
                    foreach ($_POST['item_id'] as $item) :


                      $table_name = $wpdb->prefix.'woocommerce_ebay_messenger';
                      $wpdb->delete( $table_name, array( 'id' => $item ) );

                    ++$count;

                    endforeach; ?>
        <div class="notice notice-success my-acf-notice is-dismissible">
          <p><?php _e($count.' messages deleted successfully.', 'wc_ebay'); ?></p>
        </div>

      <?php
                }
            } elseif (isset($_GET['item_id']) && isset($_GET['perform']) && $_GET['perform'] == 'delete' && isset($_GET['tab']) && $_GET['tab'] == 'feed_back_to_ebay') {
                $count = 0;
                    global $wpdb;

                      $table_name = $wpdb->prefix.'woocommerce_ebay_messenger';
                      $wpdb->delete( $table_name, array( 'id' => $_GET['item_id'] ) );
                    ++$count;
 ?>
      <div class="notice notice-success my-acf-notice is-dismissible">
        <p><?php _e($count.' message deleted successfully.', 'wc_ebay'); ?></p>
      </div>

    <?php
            } else */
            if (isset($_GET['item_id']) && isset($_GET['aid']) && isset($_GET['perform']) && $_GET['perform'] == 'source' && isset($_GET['tab']) && $_GET['tab'] == 'feed_back_to_ebay') {
                    $settings = new Categories();

                    $config = $settings->ebay_conf($_GET['aid']);

                    $client = $config['client'];
                     $params = [
                        'ErrorLanguage' => 'en_US',
                        'WarningLevel' => 'High',
                        'DetailLevel' => 'ReturnMessages'
                    ];
                    $params['MessageIDs']['MessageID'][] = trim($_GET['item_id']);   
                    $messengers = $client->GetMyMessages($params);
                    var_dump($messengers);
            }
        }

        private function table_data()
        {
            global $wpdb;

            $table_name = $wpdb->prefix.'woocommerce_ebay_messenger';

            $data = array();
            $s = isset($_POST['s']) ? $_POST['s'] : '';
            $f = isset($_POST['f']) ? $_POST['f'] : -1;
            $ac = isset($_POST['ac']) ? $_POST['ac'] : '';
            $query = "SELECT * FROM $table_name where 1";
            if ($s) {
                $query .= " and ebay_messenger_subject like '%{$s}%'";
            }
            if ($f > -1) {
                $query .= " and status = {$f}";
            }
            if ($ac) {
                $query .= " and account_id = {$ac}";
            }
            $query .= " order by ebay_messenger_received_at asc";
            $messengers = $wpdb->get_results($query);

            foreach ($messengers as $key => $messenger) {
                $data[] = array(
                    "id"=> $messenger->id,
                    "ebay_sender"=> $messenger->ebay_sender,
                    "ebay_messenger_id"=> $messenger->ebay_messenger_id,
                    "ebay_messenger_subject"=> $messenger->ebay_messenger_subject,
                    "ebay_messenger_content"=> $messenger->ebay_messenger_content,
                    "ebay_messenger_type"=> $messenger->ebay_messenger_type,
                    "ebay_messenger_received_at"=> $messenger->ebay_messenger_received_at,
                    "ebay_messenger_expiration_at"=> $messenger->ebay_messenger_expiration_at,
                    "ebay_external_message_id"=> $messenger->ebay_external_message_id,
                    "ebay_item_id"=> $messenger->ebay_item_id,
                    "store_product_id"=> $messenger->store_product_id,
                    "status"=> $messenger->status,
                    "account_id"=> $messenger->account_id,
                    "created_at"=> $messenger->created_at
                );
            }

            return $data;
        }

  //       public function column_id($item)
  //       {
  //           $aid = !empty($_GET['aid']) ? $_GET['aid'] : '';

  //           $actions = array(
  //   'delete' => sprintf('<a href="admin.php?page=manage-account&action=account&aid=%d&tab=feed_back_to_ebay&item_id=%d&perform=delete">Delete</a>', $aid, $item['id']),
  // );

  //           return sprintf('%1$s %2$s', $item['id'], $this->row_actions($actions));
  //       }
        
        public function column_account_id($item)
        {
            $store_name = $this->accounts[$item['account_id']]->store_name ?? '';
            return $store_name;
        }
        
        public function column_ebay_messenger_subject($item)
        {
            $aid = !empty($_GET['aid']) ? $_GET['aid'] : '';

            $actions = array(
    'view' => sprintf('<a class="view-messgener" id="%d" href="admin.php?page=manage-account&action=account&aid=%d&tab=feed_back_to_ebay&item_id=%d&perform=view">View</a>', $item['id'], $aid, $item['id']),
  );
            if (isset($_GET['rul']) && $_GET['rul']=='su') {
                $actions['cource'] = sprintf('<a id="%d" href="admin.php?page=manage-account&action=account&aid=%d&tab=feed_back_to_ebay&item_id=%d&perform=source" target="_blank">Source</a>', $item['ebay_messenger_id'], $aid, $item['ebay_messenger_id']);
            }
            if ($item['status'] == 0) {
                return sprintf('<b>%1$s</b> %2$s', $item['ebay_messenger_subject'], $this->row_actions($actions));
            }
            return sprintf('%1$s %2$s', $item['ebay_messenger_subject'], $this->row_actions($actions));
        }
        
        public function column_status($item)
        {
            $res = "";
            switch ($item['status']) {
                case 1:
                    $res = "<label style='color: orange'>Readed</label>";
                    break;
                case 2:
                    $res = "<label style='color: green'>Replied</label>";
                    break;
                default:
                    $res = "<label style='color: red'>New</label>";
                    break;
            }
            return sprintf($res);
        }
        
        public function column_ebay_messenger_received_at($item)
        {
            if (empty($item['ebay_messenger_received_at'])) return 'N/A';
            // $dt = new \DateTime();
            // $date = $dt->createFromFormat('d-m-Y H:i:s', $item['created_at']);
            // if (!$date) return 'N/A';
            return $this->time_elapsed_string('@' . $item['ebay_messenger_received_at']);
        }
        
        
        
        
        
        protected function pagination( $which ) {
            if ( empty( $this->_pagination_args ) ) {
                return;
            }

            $total_items     = $this->_pagination_args['total_items'];
            $total_pages     = $this->_pagination_args['total_pages'];
            $infinite_scroll = false;
            if ( isset( $this->_pagination_args['infinite_scroll'] ) ) {
                $infinite_scroll = $this->_pagination_args['infinite_scroll'];
            }

            if ( 'top' === $which && $total_pages > 1 ) {
                $this->screen->render_screen_reader_content( 'heading_pagination' );
            }

            $output = '<span class="displaying-num">' . sprintf(
                /* translators: %s: Number of items. */
                _n( '%s item', '%s items', $total_items ),
                number_format_i18n( $total_items )
            ) . '</span>';

            $current              = $this->get_pagenum();
            $removable_query_args = wp_removable_query_args();

            $current_url = set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

            $current_url = remove_query_arg( $removable_query_args, $current_url );

            $page_links = array();

            $total_pages_before = '<span class="paging-input">';
            $total_pages_after  = '</span></span>';

            $disable_first = false;
            $disable_last  = false;
            $disable_prev  = false;
            $disable_next  = false;

            if ( $current == 1 ) {
                $disable_first = true;
                $disable_prev  = true;
            }
            if ( $current == 2 ) {
                $disable_first = true;
            }
            if ( $current == $total_pages ) {
                $disable_last = true;
                $disable_next = true;
            }
            if ( $current == $total_pages - 1 ) {
                $disable_last = true;
            }

            if ( $disable_first ) {
                $page_links[] = '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">&laquo;</span>';
            } else {
                $page_links[] = sprintf(
                    "<button class='first-page button' name='paged' value='%s' type='subbmit'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></button>",
                    1,
                    __( 'First page' ),
                    '&laquo;'
                );
            }

            if ( $disable_prev ) {
                $page_links[] = '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">&lsaquo;</span>';
            } else {
                $page_links[] = sprintf(
                    "<button class='prev-page button' name='paged' value='%s' type='subbmit'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></button>",
                    max( 1, $current - 1 ),
                    __( 'Previous page' ),
                    '&lsaquo;'
                );
            }

            if ( 'bottom' === $which ) {
                $html_current_page  = $current;
                $total_pages_before = '<span class="screen-reader-text">' . __( 'Current Page' ) . '</span><span id="table-paging" class="paging-input"><span class="tablenav-paging-text">';
            } else {
                $html_current_page = sprintf(
                    "%s<input class='current-page' id='current-page-selector' type='text' name='paged' value='%s' size='%d' aria-describedby='table-paging' /><span class='tablenav-paging-text'>",
                    '<label for="current-page-selector" class="screen-reader-text">' . __( 'Current Page' ) . '</label>',
                    $current,
                    strlen( $total_pages )
                );
            }
            $html_total_pages = sprintf( "<span class='total-pages'>%s</span>", number_format_i18n( $total_pages ) );
            $page_links[]     = $total_pages_before . sprintf(
                /* translators: 1: Current page, 2: Total pages. */
                _x( '%1$s of %2$s', 'paging' ),
                $html_current_page,
                $html_total_pages
            ) . $total_pages_after;

            if ( $disable_next ) {
                $page_links[] = '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">&rsaquo;</span>';
            } else {
                $page_links[] = sprintf(
                    "<button class='next-page button' name='paged' value='%s' type='subbmit'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></button>",
                    min( $total_pages, $current + 1 ),
                    __( 'Next page' ),
                    '&rsaquo;'
                );
            }

            if ( $disable_last ) {
                $page_links[] = '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">&raquo;</span>';
            } else {
                $page_links[] = sprintf(
                    "<button class='last-page button' name='paged' value='%s' type='subbmit'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></button>",
                    $total_pages,
                    __( 'Last page' ),
                    '&raquo;'
                );
            }

            $pagination_links_class = 'pagination-links';
            if ( ! empty( $infinite_scroll ) ) {
                $pagination_links_class .= ' hide-if-js';
            }
            $output .= "\n<span class='$pagination_links_class'>" . join( "\n", $page_links ) . '</span>';

            if ( $total_pages ) {
                $page_class = $total_pages < 2 ? ' one-page' : '';
            } else {
                $page_class = ' no-pages';
            }
            $this->_pagination = "<div class='tablenav-pages{$page_class}'>$output</div>";

            echo $this->_pagination;
        }
    }
}

$export_product = new WC_Ebay_Feedback();

if (isset($_POST['s'])) {
    $export_product->prepare_items($_POST['s']);
} else {
    $export_product->prepare_items();
}

?>
<?php
    if (!($_GET['aid'] ?? 0)) {
?>
    <div class="wc-product-import-section">

        <div class="wc-ebay-info-bar">

            <img class="wc-loader-image" src="<?php echo EBAY_API. '/assets/images/loader.gif'; ?>">

        </div>

        <div class="wc-product-import-section-body">

        </div>

    </div>
<?php
    }
?>
<div class="ebay-tab-wrap">
    <div id="messenger-popup">
        <div class="close">x</div>
        <div class="content"></div>
        <div class="wc-ebay-info-bar">

            <img class="wc-loader-image" style="margin-left: 48%" src="<?php echo EBAY_API. '/assets/images/loader.gif'; ?>">

        </div>
        <!-- <iframe width="100%" height="300px" src="google.com" frameborder="0"></iframe> -->
        <div class="reply">
            <form id="messenger-reply">
                <input type="hidden" value="" name="id">
                <textarea placeholder="Content for reply" name="content" width="100%" height="250"></textarea>
                <button type="submit" class="action-split button button-primary">Reply</button>
            </form>
        </div>
    </div>
  <div class="ebay-sync main-syncebay-mes">

    <input type="hidden" name="ebay_accid" value="<?php echo $_GET['aid'] ?? ''; ?>" />

    <h2 id="ebay-expopro">Feedbacks To Ebay</h2>
    
    <button id="import_message_from_ebay" title="Import Messages From eBay" class="action-split button button-primary" data-ui-id="import-oro-from-ebay">Import Messages From eBay</button>

  </div>

  <form method="POST">

    <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
    
    <?php

    $export_product->search_box('Search Product', 'search-id');
    ?>
    <select name="f" id="filter-status" style="float: right;">
        <option value="-1" <?php echo isset($_REQUEST['f']) && $_REQUEST['f'] == -1 ? 'selected="selected"' : '' ?>>-- Status --</option>
        <option value="0" <?php echo isset($_REQUEST['f']) && $_REQUEST['f'] == 0 ? 'selected="selected"' : '' ?>>New</option>
        <option value="1" <?php echo isset($_REQUEST['f']) && $_REQUEST['f'] == 1 ? 'selected="selected"' : '' ?>>Readed</option>
        <option value="2" <?php echo isset($_REQUEST['f']) && $_REQUEST['f'] == 2 ? 'selected="selected"' : '' ?>>Replied</option>
    </select>
    <select name="ac" id="filter-status-acc" style="float: right;">
        <option value="0">-- Store --</option>
        <?php
        if (count($export_product->accounts)) {
            foreach ($export_product->accounts as $key => $acc) {
                ?>
                <option value="<?php echo $acc->id; ?>" <?php echo isset($_REQUEST['ac']) && $_REQUEST['ac'] == $acc->id ? 'selected="selected"' : '' ?>><?php echo $acc->store_name ?></option>
                <?php
            }
        }
        ?>
    </select>
    <?php
    $export_product->display();

    ?>
    
  </form>

</div>