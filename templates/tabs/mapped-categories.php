<?php

if( ! defined ( 'ABSPATH' ) )

  exit;

$acc_id = $_GET['aid'];

$terms = get_terms(['taxonomy' => 'product_cat','hide_empty' => false, 'parent' => 0]);

$results = apply_filters('wp_ebay_import_categories', $_GET['aid']);

$resultArr ='';

$parent_categories = array();

if(!empty($results['success'])){

  $resultArr = $results['success'];

  if(!empty($resultArr) && count($resultArr)>0){


        foreach($resultArr as $cat_val){

            if($cat_val['category_pid']==$cat_val['category_id']){

                array_push($parent_categories,array(
                  'cat_name'     => $cat_val['cat_name'],
                  'category_id'  => $cat_val['category_id']
                ));
            }

        }

		}

}
 ?>

<script>

      $ebay=jQuery.noConflict();

      (function($ebay){

          var final_json_arr = <?php echo json_encode($resultArr) ?>;


          // Category map ajax request

          $ebay(document).on("click","#wc-leaf",function(){

              var ebay_leaf_id  = $ebay(document).find('.ebay-cat-wrapper').children("select").last().val();
              var is_ebay_leaf  = $ebay(document).find('.ebay-cat-wrapper').children("select").last().attr('data-leaf');
              var store_cat_id  = $ebay(document).find('.store-cat-wrapper').children("select").last().val();
              var is_store_leaf = $ebay(document).find('.store-cat-wrapper').children("select").last().attr('data-leaf');
              var cat_name  = $ebay(document).find('.ebay-cat-wrapper').children("select").last().find("option:selected").text();
              var ebay_accid = $ebay('input[name="ebay_accid"]').val();

              this_elm = $ebay(this)

              if ( store_cat_id.indexOf("store") >= 0 ) {

                  value = store_cat_id.split('-');

                  store_cat_id = value[1];

              } else {

                  store_cat_id='';
              }

              loader = api_script.loader_url;

              if( is_ebay_leaf == 'true' && is_store_leaf == 'true' && cat_name && store_cat_id && ebay_leaf_id ) {

                  var selectElm = '';

                      $ebay.ajax({
                          type: 'POST',
                          url: api_script.api_admin_ajax,
                          data: {'action': 'ebay_category_exist','nonce':api_script.api_nonce,'ebay_leaf_id':ebay_leaf_id,'store_cat_id':store_cat_id,'cat_name':cat_name,'acc_id':ebay_accid},
                          beforeSend: function(){
                            this_elm.after("<img class='img-loader' src='"+loader+"/assets/images/loader.gif'>");
                            this_elm.attr("disabled", "disabled")
                          },
                          success: function(response) {
                            if(response.success != undefined){

                                alert("Category mapped successfully");

                                window.location.href = document.location.href.replace('mapped_ebay', 'map_category');

                            }else{

                              if(response.msg){
                                alert(response.msg);
                                this_elm.next('.img-loader').remove()
                                this_elm.prop("disabled", false)
                              }

                            }

                          }

                        });



                } else {

                  alert("Please select leaf Category first.");
                }

            });


          // Ebay/Store Category change on select

          $ebay(document).on("change","select",function(){

                level = $ebay(this).attr('data-level');

                if( level ) {
                  level = parseInt( level ) + 1;
                }

                currentElm = this;

                if($ebay(this).parent().hasClass("cat-wrap")){

                  $ebay(this).next("select").nextAll("select").remove();

                  $ebay(this).next(".select2-container").nextAll(".select2-container").remove();

                }

                loader = api_script.loader_url;

                option = currentElm.value;

                if (option.indexOf("store") >= 0){

                  value = option.split('-');

                  value= value[1];

                  // Ebay Category selection

                  if(value){

                      $ebay.ajax({
                          type: 'POST',
                          url: api_script.api_admin_ajax,
                          data: {"action": "wc_ebay_get_store_childs","nonce":api_script.api_nonce,"child_id":value},
                          beforeSend: function(){
                            $ebay(".store-cat-wrapper .select2-selection:last .select2-selection__arrow").append("<img class='img-loader' src='"+loader+"/assets/images/loader.gif'>");
                          },
                          success: function(response) {
                            console.log(response);
                            var selectElmOption = '';

                            var selectElm = '';

                            if( response.length > 0){
                              selectElmOption += '<option value="">--Select--</option>';
                              $ebay.each( response, function( i, val ) {

                                  selectElmOption += '<option value="store-'+val.cat_id+'">'+val.cat_name+'</option>';

                              });


                              if(selectElmOption){

                                selectElm += '<select >'+selectElmOption+'</select>';

                                $ebay(".store-cat-wrapper .select2-selection:last .select2-selection__arrow .img-loader").remove();

                                $ebay(".store-cat-wrapper").append(selectElm);

                                $ebay(document).find(".store-cat-wrapper select:last").select2();

                              }
                              else{

                                $ebay(".store-cat-wrapper .select2-selection:last .select2-selection__arrow .img-loader").remove();

                                $ebay(document).find(".store-cat-wrapper .select2-selection--single:last").css("border","1px solid #2bbd5f");

                                selectElm = '';

                              }



                            } else {

                              $ebay(document).find(".store-cat-wrapper select:last").attr("data-leaf","true");

                              $ebay(".store-cat-wrapper .select2-selection:last .select2-selection__arrow .img-loader").remove();

                                $ebay(document).find(".store-cat-wrapper .select2-selection--single:last").css("border","1px solid #2bbd5f");

                                selectElm = '';

                            }


                          }

                        });


                  }

                } else{

                  value = option;

                  function recursive_categories_ajax_call( value, level ) {

                      if( value && level ) {

                            var ebay_accid = $ebay('input[name="ebay_accid"]').val();

                              $ebay.ajax({
                                type: 'POST',
                                url: api_script.api_admin_ajax,
                                data: {'action': 'wc_ebay_catogories_childs','nonce':api_script.api_nonce,'parent':value,'level':level,'ebay_accid':ebay_accid},
                                beforeSend: function() {
                                  $ebay(".ebay-cat-wrapper .select2-selection:last .select2-selection__arrow").append("<img class='img-loader' src='"+loader+"/assets/images/loader.gif'>");
                                },
                                success: function(response) {
                            
                                  var selectElmOption = '';

                                  var selectElm = '';

                                  if( response.success != undefined ){

                                        if ( response.success.length > 0 ) {
                                          selectElmOption += '<option value="">--Select--</option>';
                                          $ebay.each(response.success,function(i,val){

                                              if( value != val.category_id ) {

                                                selectElmOption += '<option value="'+val.category_id+'">'+val.cat_name+'</option>';

                                              }

                                          });


                                          if(selectElmOption){

                                            selectElm += '<select data-level='+level+'>'+selectElmOption+'</select>';
                                            $ebay(".ebay-cat-wrapper .select2-selection:last .select2-selection__arrow .img-loader").remove();
                                              $ebay(".ebay-cat-wrapper").append(selectElm);

                                            $ebay(document).find(".ebay-cat-wrapper select:last").select2();

                                          }
                                          else{

                                            selectElm = '';

                                          }

                                    }
                                } else if(response.msg != undefined) {

                                    $ebay(".select2-selection:last .select2-selection__arrow .img-loader").remove();

                                    $ebay(document).find(".ebay-cat-wrapper .select2-selection--single:last").css("border","1px solid #2bbd5f");

                                    if( response.msg == 'Leaf category.!' ) {

                                       $ebay(document).find(".ebay-cat-wrapper select:last").attr("data-leaf","true");


                                    }
                                }


                                }

                              });


                            }

                      }

                      recursive_categories_ajax_call( value, level );


                }



              });


      })(jQuery);


  </script>

<div class="panel mapper-cat woocommerce_options_panel">

        <!--// Type-->

        <div class="store-cat-wrapper cat-wrap">

            <label for="wc-store-cat"><?php echo __( 'Store Categories', 'wc_ebay' ); ?></label>

            <select name="store-category" id="wc-store-cat" class="map-cat">

                <?php

                  if(!empty($terms)) :

                    foreach($terms as $term) : ?>

                        <option value="<?php echo 'store-'.$term->term_id; ?>"><?php echo $term->name ?></option>

                  <?php

                    endforeach;

                  else:

                    echo "<option>No Categories.</option>";

                  endif;

                  ?>

            </select>

        </div>

        <div class="ebay-cat-wrapper cat-wrap">

            <label for="wc-ebay-cat"><?php echo __( 'Ebay Categories', 'wc_ebay' ); ?></label>

            <select name="ebay-category" data-level="<?php echo "1"; ?>" id="wc-ebay-cat" class="map-cat">
                <?php

                    if(!empty($parent_categories) && count($parent_categories)>0) :

                      foreach($parent_categories as $result) :

                      ?>

                        <option value="<?php echo $result['category_id']; ?>"><?php echo $result['cat_name']; ?></option>

                      <?php

                        endforeach;

                      else : ?>

                      <option>No Categories on ebay.</option>

                    <?php endif; ?>


            </select>


        </div>

      <input type="hidden" value="<?php echo $acc_id?$acc_id:'';?>" name="ebay_accid">

      <button id="wc-leaf" class="button button-primary">Map category</button>

</div>
