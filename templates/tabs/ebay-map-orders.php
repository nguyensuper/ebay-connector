<?php

if( ! defined ( 'ABSPATH' ) )

  exit;


if( !class_exists( 'WP_List_Table' ) ){

  require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

}

if( ! class_exists ( 'WC_Ebay_Map_Orders' ) ){

  class WC_Ebay_Map_Orders extends WP_List_Table{

    function __construct(){

      parent::__construct( array(

        'singular'  => 'Ebay Orders List',
        'plural'    => 'Ebay Orders List',
        'ajax'      => false

      ) );

    }

    function prepare_items(){

      global $wpdb;

      $columns = $this->get_columns();

      $sortable = $this->get_sortable_columns();

      $hidden = $this->get_hidden_columns();

      $data = $this->table_data();

      $totalItems = count( $data );

      $user = get_current_user_ID();

      $screen = get_current_screen();

      $perpage = $this->get_items_per_page( 'rule_per_page', 20 );

      $this->_column_headers = array( $columns, $hidden, $sortable );

      if( empty( $per_page  )  || $per_page < 1 ){

        $per_page = $screen->get_option( 'per_page', 'default' );

      }

      function usort_oreorder( $a, $b ){

        $orderby = ( ! empty( $_REQUEST['orderby'] ) ) ? $_REQUEST['orderby'] : 'ebay_orderid';

        $order = ( !empty( $_REQUEST['order'] ) ) ? $_REQUEST['order'] : 'desc';

        $result = strcmp( $a[$orderby], $b[$orderby] );

        return ( $order === 'asc' ) ? $result : -$result;

      }

      usort( $data, 'usort_oreorder' );

      $totalPages = ceil( $totalItems / $perpage );

      $currentPage = $this->get_pagenum();

      $data = array_slice( $data, ( ( $currentPage - 1 ) * $perpage ), $perpage );

      $this->set_pagination_args( array(

        "total_items" => $totalItems,

        "total_pages" => $totalPages,

        "per_page"    => $perpage

      ) );

      $this->items = $data;

    }

    public function get_hidden_columns(){

      return array();

    }

    /**
     * Define the columns that are going to be used in the table
     * @return array $columns, the array of columns to use with the table
     */

    function get_columns(){

      return $columns = array(

        'ebay_orderid'    => __( 'Ebay Order ID', 'wc_ebay' ),

        'store_orderid'  => __( 'Store Order ID', 'wc_ebay' ),

        'ebay_orderstatus' => __( 'Ebay Order Status', 'wc_ebay' ),

        'created_at'   => __( 'Created At', 'wc_ebay' )
      );

    }

    function column_default( $item, $column_name ){

      switch( $column_name ){

        case 'ebay_orderid':

        case 'store_orderid':

        case 'ebay_orderstatus':

        case 'created_at':

          return $item[ $column_name ];

        default:

          return print_r( $item, true );

      }

    }


    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */

     public function get_sortable_columns(){

       return $sortable = array(

         'ebay_orderid' => array( 'ebay_orderid', true ),

         'ebay_orderstatus'  => array( 'ebay_orderstatus', true )

       );

     }


     private function table_data(){

       global $wpdb;

       $table_name=$wpdb->prefix.'woocommerce_ebay_order_map';

       $data = $eid = $sid = $status = $created_at = array();

       $s = isset( $_POST['s'] ) ? $_POST['s'] : '';

        if( !empty( $s ) ) {

          $store_data = $wpdb->get_results( "SELECT * FROM $table_name where store_id LIKE '%$s%'" );

        } else {

         $store_data = $wpdb->get_results( "SELECT * FROM $table_name");

        }

       $i=0;

       foreach ($store_data as $key => $value) {

         $eid[] = $value->ebay_order_id;
         $sid[] = $value->store_order_id;
         $status[] = $value->ebay_order_status;
         $created_at[] = $value->created_at;

         $data[] = array(

           'ebay_orderid'       => $eid[$i],

           'store_orderid'      =>  $sid[$i],

           'ebay_orderstatus'   => $status[$i],

           'created_at'         => $created_at[$i]

         );

         $i++;

       }

       return $data;

     }



     function column_id( $item ){

       $actions = array(

         'edit' =>  sprintf( '<a href="admin.php?page=wc-store-updation&sid=%s&action=edit">Edit</a>', $item[ 'id' ] ),

         'trash'  => sprintf( '<a href="admin.php?page=wc-store-updation&sid=%s&action=trash">Trash</a>', $item[ 'id' ] )

       );

       return sprintf( '%1$s %2$s', $item[ 'id' ], $this->row_actions( $actions ) );

     }

  }

}


$orders_list = new WC_Ebay_Map_Orders();

if( isset($_POST['s']) ){

  $orders_list->prepare_items($_POST['s']);


} else {

  $orders_list->prepare_items();

}

?>

<div class="ebay-tab-wrap">

  <div class="ebay-sync main-syncebay">

      <input type="hidden" name="ebay_accid" value="<?php echo $eid; ?>" />

      <button id="open_import_oro_from_ebay" title="Import Orders From eBay" class="action-split button button-primary" data-ui-id="import-oro-from-ebay">Import Orders From eBay</button>

      <button id="profiler_for_create_oro" title="Create Imported Order" class="action-split button button-primary" data-ui-id="profiler-for-create-oro">Create Imported Orders</button>

  </div>

  <form method="POST">

    <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />

    <?php

      $orders_list->search_box( 'Search', 'search-id' );

      $orders_list->display();

    ?>

  </form>

</div>
