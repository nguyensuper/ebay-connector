<?php

if( ! defined ( 'ABSPATH' ) )

  exit;

if( !class_exists( 'WP_List_Table' ) ){

  require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

}

if( ! class_exists ( 'WC_Ebay_Import_Category' ) ){

  class WC_Ebay_Import_Category extends WP_List_Table{

    function __construct(){

      parent::__construct( array(

        'singular'  => 'Ebay Import Categories',
        'plural'    => 'Ebay Import Categories',
        'ajax'      => false

      ) );

    }

    function prepare_items(){

      global $wpdb;

      $columns = $this->get_columns();

      $sortable = $this->get_sortable_columns();

      $hidden = $this->get_hidden_columns();

      $this->process_bulk_action();

      $data = $this->table_data();

      $totalItems = count( $data );

      $user = get_current_user_ID();

      $screen = get_current_screen();

      $perpage = $this->get_items_per_page( 'rule_per_page', 20 );

      $this->_column_headers = array( $columns, $hidden, $sortable );

      if( empty( $per_page  )  || $per_page < 1 ){

        $per_page = $screen->get_option( 'per_page', 'default' );

      }

      function usort_reorder( $a, $b ){

        $orderby = ( ! empty( $_REQUEST['orderby'] ) ) ? $_REQUEST['orderby'] : 'category_id';

        $order = ( !empty( $_REQUEST['order'] ) ) ? $_REQUEST['order'] : 'desc';

        $result = strcmp( $a[$orderby], $b[$orderby] );

        return ( $order === 'asc' ) ? $result : -$result;

      }

      usort( $data, 'usort_reorder' );

      $totalPages = ceil( $totalItems / $perpage );

      $currentPage = $this->get_pagenum();

      $data = array_slice( $data, ( ( $currentPage - 1 ) * $perpage ), $perpage );

      $this->set_pagination_args( array(

        "total_items" => $totalItems,

        "total_pages" => $totalPages,

        "per_page"    => $perpage

      ) );

      $this->items = $data;

    }


    /**
     * Define the columns that are going to be used in the table
     * @return array $columns, the array of columns to use with the table
     */

    function get_columns(){

      return $columns = array(

        'cb'  => '<input type="checkbox" />',

        'category_id'  => __( 'Store category', 'wc_ebay' ),

        'category_name' => __( 'Category name', 'wc_ebay' )

      );

    }

    function column_default( $item, $column_name ) {

      switch( $column_name ) {

        case 'category_id':

        case 'category_name':

          return $item[ $column_name ];

        default:

          return print_r( $item, true );

      }

    }

    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */

     public function get_sortable_columns(){

       return $sortable = array(

         'category_id'  => array( 'category_id', true ),
         'category_name'  => array( 'category_name', true )

       );

     }
     public function get_hidden_columns() {

       return array();

     }

     function column_cb( $item ){

       return sprintf('<input type="checkbox" id="store_%s" name="item_id[]" value="%s" />', $item[ 'category_id' ], $item[ 'category_id' ]);

     }

     private function table_data(){

      global $wpdb;


      $data=array();


      $args = array(
        'taxonomy' => 'product_cat',
        'orderby' => 'category_id',
          'order' => 'desc',
          'hide_empty' => false
      );

      $res = get_categories( $args );

       foreach ($res as $key => $value) {


         $data[] = array(

           'category_id'         =>  $value->term_id,

           'category_name'          =>  $value->name

         );

       }

       return $data;

     }

     function get_bulk_actions() {

            $actions = array(
                'delete'    => 'Delete'
            );
            return $actions;

    }

    function process_bulk_action() {

      global $wpdb;


      if( $this->current_action() == 'delete' ) {

          $count = 0;

          $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

          if( is_array( $_POST['item_id'] ) ) {

            foreach( $_POST['item_id'] as $item ) :

                wp_delete_term( $item, 'product_cat' );

                $count++;


            endforeach;

            ?>
            <div class="notice notice-success my-acf-notice is-dismissible" >
              <p><?php _e( $count.' Category(s) deleted successfully.', 'wc_ebay' ); ?></p>
            </div>

            <?php


          }

      } elseif( isset( $_GET['item_id'] ) && isset( $_GET['perform'] ) && $_GET['perform'] == 'delete' && isset( $_GET['tab'] ) && $_GET['tab'] == 'import_category' ) {

            $count = 0;

            $category_map_table = $wpdb->prefix.'woocommerce_ebay_category_map';

            $item = intval( $_GET['item_id'] );

            wp_delete_term( $item, 'product_cat' );

            $count++;


            ?>
            <div class="notice notice-success my-acf-notice is-dismissible" >
              <p><?php _e( 'Category deleted successfully.', 'wc_ebay' ); ?></p>
            </div>

            <?php


        }

    }

     function column_category_id( $item ){

       $aid = isset( $_GET['aid'] ) ? $_GET['aid'] : '';

       $actions = array(

         'edit' =>  sprintf( '<a href="term.php?taxonomy=product_cat&tag_ID=%d&post_type=product">Edit</a>', $item[ 'category_id' ] ),

         'delete'  => sprintf( '<a href="admin.php?page=manage-account&action=account&aid=%d&tab=import_category&perform=delete&item_id=%d">Delete</a>', $aid, $item[ 'category_id' ] )

       );

       return sprintf( '%1$s %2$s', $item[ 'category_id' ], $this->row_actions( $actions ) );

     }

  }

}


$import_category = new WC_Ebay_Import_Category();

$import_category->prepare_items();

?>

<div class="ebay-tab-wrap">

  <div class="ebay-sync main-syncebay">

      <input type="hidden" name="ebay_accid" value="<?php echo $eid; ?>" />

      <button id="import_cat_from_ebay" title="Import Categories From eBay" class="action-split button button-primary">Import Categories From eBay</button>

      <button id="profiler_for_create_cat" title="Create Imported Categories" class="action-split button button-primary">Create Imported Categories</button>


  </div>

  <form method="POST">

    <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />

    <?php

      $import_category->display();

    ?>

  </form>

</div>
