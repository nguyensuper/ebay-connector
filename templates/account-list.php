<?php

if( ! defined ( 'ABSPATH' ) )

  exit;


if( !class_exists( 'WP_List_Table' ) ){

  require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

}

if ( ! class_exists ( 'WC_Ebay_Account_List' ) ) {

    class WC_Ebay_Account_List extends WP_List_Table {

        public function __construct() {

          parent::__construct( array(

            'singular'  => 'Ebay Account List',
            'plural'    => 'Ebay Account List',
            'ajax'      => false

          ) );

        }

        function prepare_items(){

          global $wpdb;

          $columns = $this->get_columns();

          $sortable = $this->get_sortable_columns();

          $hidden = $this->get_hidden_columns();

          $this->process_bulk_action();

          $data = $this->table_data();

          $totalItems = count( $data );

          $user = get_current_user_ID();

          $screen = get_current_screen();

          $perpage = $this->get_items_per_page( 'rule_per_page', 20 );

          $this->_column_headers = array( $columns, $hidden, $sortable );

          if( empty( $per_page  )  || $per_page < 1 ){

            $per_page = $screen->get_option( 'per_page', 'default' );

          }

          function usort_reorder( $a, $b ){

            $orderby = ( ! empty( $_REQUEST['orderby'] ) ) ? $_REQUEST['orderby'] : 'id';

            $order = ( !empty( $_REQUEST['order'] ) ) ? $_REQUEST['order'] : 'desc';

            $result = strcmp( $a[$orderby], $b[$orderby] );

            return ( $order === 'asc' ) ? $result : -$result;

          }

          usort( $data, 'usort_reorder' );

          $totalPages = ceil( $totalItems / $perpage );

          $currentPage = $this->get_pagenum();

          $data = array_slice( $data, ( ( $currentPage - 1 ) * $perpage ), $perpage );

          $this->set_pagination_args( array(

            "total_items" => $totalItems,

            "total_pages" => $totalPages,

            "per_page"    => $perpage

          ) );

          $this->items = $data;

        }


        /**
         * Define the columns that are going to be used in the table
         * @return array $columns, the array of columns to use with the table
         */

        function get_columns(){

          return $columns = array(

            'cb'  => '<input type="checkbox" />',

            'id'    => __( 'ID', 'wc_ebay' ),

            'store_name'  => __( 'Store Name', 'wc_ebay' ),

            'ebay_userid' => __( 'Ebay User Id', 'wc_ebay' ),

            'status' => __( 'Status', 'wc_ebay' ),
            
            'action'   => __( 'Action', 'wc_ebay' )
          );

        }

        function column_default( $item, $column_name ) {

          switch( $column_name ) {

            case 'id':

            case 'store_name':

            case 'ebay_userid':

            case 'status':
            
            case 'action':

              return $item[ $column_name ];

            default:

              return print_r( $item, true );

          }

        }

        /**
         * Decide which columns to activate the sorting functionality on
         * @return array $sortable, the array of columns that can be sorted by the user
         */

        public function get_sortable_columns(){

          return $sortable = array(

            'id' => array( 'id', true ),

            'store_name'  => array( 'store_name', true )

          );

        }
        public function get_hidden_columns(){

          return array();

        }

        function column_cb( $item ){

          return sprintf('<input type="checkbox" id="store_%s" name="store[]" value="%s" />', $item[ 'id' ], $item[ 'id' ]);

        }
        
        function column_status($item){
          $status = 'Auto';
          $color = 'green';
          switch ($item['status']) {
            case 1:
              $status = 'Pending';
              $color = 'orange';
              break;
            case 2:
              $status = 'Fulled';
              $color = 'red';
              break;
              
            case 3:
              $status = 'Manual';
              $color = 'blue';
              break;
            
            default:
              $status = 'Auto';
              break;
          }
          
          $actions = array();
          if ($item['status'] > 0 && $item['status'] < 3) {
            $actions['active'] = sprintf( '<a href="admin.php?page=manage-account&perform=view&aid=%d">View Errors</a>', $item[ 'id' ] );
          }
          return sprintf( '<label style="color:%1$s" id="store_status_%2$s">%3$s</label> %4$s', $color, $item[ 'id' ], $status, $this->row_actions( $actions ) );
        }

        private function table_data(){

          global $wpdb;

          $table_name=$wpdb->prefix.'woocommerce_ebay_account';

          $data = $store_data = array();
          $s = isset( $_POST['s'] ) ? $_POST['s'] : '';

          if( !empty( $s ) ) {

            $store_data = $wpdb->get_results( "SELECT id,store_name,ebay_userid,status FROM $table_name where store_name LIKE '%$s%' OR ebay_userid LIKE '%$s%'");


          } else {

            $store_data = $wpdb->get_results( "SELECT id,store_name,ebay_userid,status FROM $table_name");

          }

          foreach ($store_data as $key => $value) {

            $id = $value->id;

            $data[] = array(

              'id'   => $id,

              'store_name' =>  $value->store_name,

              'ebay_userid'  => $value->ebay_userid,

              'status'  => $value->status,
              
              'action' => '<a href="?page=manage-account&action=account&aid='.$id.'" class="button button-primary">Ebay Connect</a>'

            );


          }

          return $data;

        }

        function get_bulk_actions() {

          $actions = array(
              'delete'    => 'Delete Account'
          );
          return $actions;

        }

        function process_bulk_action() {

          global $wpdb;

          $table_name=$wpdb->prefix.'woocommerce_ebay_account';

          if( $this->current_action() == 'delete' ) {

              $count = 0;

              if( is_array( $_POST['store'] ) ) {

                foreach( $_POST['store'] as $store ) :

                  $account_list = $wpdb->get_row( "SELECT id FROM $table_name WHERE id='$store'");

                  if( ! empty( $account_list ) ) {

                    $delete_ac = $wpdb->delete(
                      $table_name,
                      array(
                        'id'   => $account_list->id
                      ),
                      array( '%d' )
                    );

                    if( $delete_ac ) {

                      $count++;

                    }

                  }

                endforeach;

                ?>
                <div class="notice notice-success my-acf-notice is-dismissible" >
                  <p><?php _e( $count.' Account(s) deleted successfully.', 'wc_ebay' ); ?></p>
                </div>

                <?php
              }

            } elseif( isset( $_GET['aid'] ) && isset( $_GET['perform'] ) && $_GET['perform'] == 'delete') {

                $count = 0;

                $store = intval( $_GET['aid'] );

                $account_list = $wpdb->get_row( "SELECT id FROM $table_name WHERE id='$store'");

                if( ! empty( $account_list ) ) {

                  $delete_ac = $wpdb->delete(
                    $table_name,
                    array(
                      'id'   => $account_list->id
                    ),
                    array( '%d' )
                  );

                  if( $delete_ac ) {

                    $count++;

                  }

                }


                ?>
                <div class="notice notice-success my-acf-notice is-dismissible" >
                  <p><?php _e( 'Account deleted successfully.', 'wc_ebay' ); ?></p>
                </div>

                <?php

            } elseif ( isset( $_GET['aid'] ) && isset( $_GET['perform'] ) && $_GET['perform'] == 'active') {

                $store = intval( $_GET['aid'] );

                $account_list = $wpdb->get_row( "SELECT id FROM $table_name WHERE id='$store'");

                if( ! empty( $account_list ) ) {

                  $delete_ac = $wpdb->update(
                    $table_name,
                    array(
                      'status' => 0,
                      'status_msg' => '',
                      'update_status_at' => time()
                    ),
                    array(
                      'id'   => $account_list->id
                    )
                  );

                }

                if ($delete_ac) {
                  ?>
                  <div class="notice notice-success my-acf-notice is-dismissible" >
                    <p><?php _e( 'Account set auto successfully.', 'wc_ebay' ); ?></p>
                  </div>

                  <?php
                }
            } elseif ( isset( $_GET['aid'] ) && isset( $_GET['perform'] ) && $_GET['perform'] == 'view') {

                $store = intval( $_GET['aid'] );

                $account_list = $wpdb->get_row( "SELECT store_name, status_msg FROM $table_name WHERE id='$store'");
                $errors = json_decode($account_list->status_msg, true);
                  ?>
                  <div class="notice notice-error my-acf-notice is-dismissible" >
                    <p><?php _e( 'List error when auto(Store <b>' . $account_list->store_name . '</b>):', 'wc_ebay' ); ?></p>
                  </div>
                  <?php
                foreach ($errors as $key => $error) {
                  if (!isset($error['type'])) continue;
                  if (!isset($error['msg'])) continue;
                    ?>
                  <div class="notice notice-<?php echo strtolower($error['type']); ?> my-acf-notice is-dismissible" >
                    <p><?php _e( $error['msg'], 'wc_ebay' ); ?></p>
                  </div>
                  <?php
                }
            } elseif ( isset( $_GET['aid'] ) && isset( $_GET['perform'] ) && $_GET['perform'] == 'manual') {

                $store = intval( $_GET['aid'] );

                $account_list = $wpdb->get_row( "SELECT id FROM $table_name WHERE id='$store'");

                if( ! empty( $account_list ) ) {

                  $delete_ac = $wpdb->update(
                    $table_name,
                    array(
                      'status' => 3,
                      'status_msg' => '',
                      'update_status_at' => time()
                    ),
                    array(
                      'id'   => $account_list->id
                    )
                  );

                }

                if ($delete_ac) {
                  ?>
                  <div class="notice notice-success my-acf-notice is-dismissible" >
                    <p><?php _e( 'Account set manual successfully.', 'wc_ebay' ); ?></p>
                  </div>

                  <?php
                }
            }

        }

        function column_id( $item ){

          $actions = array(

            'edit' =>  sprintf( '<a href="admin.php?page=manage-account&action=account&aid=%s">Edit</a>', $item[ 'id' ] ),

            'delete'  => sprintf( '<a href="admin.php?page=manage-account&perform=delete&aid=%d">Delete</a>', $item[ 'id' ] )
          );
          if ($item['status'] > 0) {
            $actions['active'] = sprintf( '<a href="admin.php?page=manage-account&perform=active&aid=%d">Auto</a>', $item[ 'id' ] );
          }
          if ($item['status'] == 0) {
            $actions['manual'] = sprintf( '<a href="admin.php?page=manage-account&perform=manual&aid=%d">Manual</a>', $item[ 'id' ] );
          }
          return sprintf( '%1$s %2$s', $item[ 'id' ], $this->row_actions( $actions ) );

        }

    }

}


$account_list = new WC_Ebay_Account_List();

$account_list->prepare_items();

?>
<div class="wrap">

  <h1>Ebay Account List <a href="<?php echo admin_url() . 'admin.php?page=manage-account&action=add'; ?>" class="page-title-action">Add New</a></h1>

  <form method="POST">

    <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />

    <?php

      $account_list->search_box( 'Search', 'search-id' );

      $account_list->display();

    ?>

  </form>

</div>
