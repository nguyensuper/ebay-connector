<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$order_status = wc_get_order_statuses();
global $wpdb;
$table = $wpdb->prefix.'woocommerce_ebay_account';
$accounts = $wpdb->get_results("Select id,store_name from $table ", ARRAY_A);

// since wordpress 4.5.0
$args = array(
    'taxonomy' => 'product_cat',
    'hiUS_empty' => 0,
);

$product_categories = get_terms($args);
$countryCodeselected = get_option('countryCodeType');
$shipCountryCodeSelected = get_option('shipCountryCodeType');
$countryCodes = array(
    'AD' => 'Andorra',
    'AE' => 'United Arab Emirates',
    'AF' => 'Afghanistan',
    'AG' => 'Antigua and Barbuda',
    'AI' => 'Anguilla',
    'AL' => 'Albania',
    'AM' => 'Armenia',
    'AN' => 'Netherlands Antilles',
    'AO' => 'Angola',
    'AQ' => 'Antarctica',
    'AR' => 'Argentina',
    'AS' => 'American Samoa',
    'AT' => 'Austria',
    'AU' => 'Australia',
    'AW' => 'Aruba',
    'AZ' => 'Azerbaijan',
    'BA' => 'Bosnia and Herzegovina',
    'BB' => 'Barbados',
    'BD' => 'Bangladesh',
    'BE' => 'Belgium',
    'BF' => 'Burkina Faso',
    'BG' => 'Bulgaria',
    'BH' => 'Bahrain',
    'BI' => 'Burundi',
    'BJ' => 'Benin',
    'BM' => 'Bermuda',
    'BN' => 'Brunei Darussalam',
    'BO' => 'Bolivia',
    'BR' => 'Brazil',
    'BS' => 'Bahamas',
    'BT' => 'Bhutan',
    'BV' => 'Bouvet Island',
    'BW' => 'Botswana',
    'BY' => 'Belarus',
    'BZ' => 'Belize',
    'CA' => 'Canada',
    'CC' => 'Cocos (Keeling) Islands',
    'CD' => 'Congo, The Democratic Republic of the',
    'CF' => 'Central African Republic',
    'CG' => 'Congo',
    'CH' => 'Switzerland',
    'CK' => 'Cook Islands',
    'CL' => 'Chile',
    'CM' => 'Cameroon',
    'CN' => 'China',
    'CO' => 'Colombia',
    'CR' => 'Costa Rica',
    'CU' => 'Cuba',
    'CV' => 'Cape Verde',
    'CX' => 'Christmas Island',
    'CY' => 'Cyprus',
    'CZ' => 'Czech Republic',
    'DE' => 'Germany',
    'DJ' => 'Djibouti',
    'DK' => 'Denmark',
    'DM' => 'Dominica',
    'DO' => 'Dominican Republic',
    'DZ' => 'Algeria',
    'EC' => 'Ecuador',
    'EE' => 'Estonia',
    'EG' => 'Egypt',
    'EH' => 'Western Sahara',
    'ER' => 'Eritrea',
    'ES' => 'Spain',
    'ET' => 'Ethiopia',
    'FI' => 'Finland',
    'FJ' => 'Fiji',
    'FK' => 'Falkland Islands (Malvinas)',
    'FM' => 'Federated States of Micronesia',
    'FO' => 'Faroe Islands',
    'FR' => 'France',
    'GA' => 'Gabon',
    'GB' => 'United Kingdom',
    'GD' => 'Grenada',
    'GE' => 'Georgia',
    'GF' => 'French Guiana',
    'GG' => 'Guernse',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GL' => 'Greenland',
    'GM' => 'Gambia',
    'GN' => 'Guinea',
    'GP' => 'Guadeloupe',
    'GQ' => 'Equatorial Guinea',
    'GR' => 'Greece',
    'GS' => 'South Georgia and the South Sandwich Islands',
    'GT' => 'Guatemala',
    'GU' => 'Guam',
    'GW' => 'Guinea-Bissau',
    'GY' => 'Guyana',
    'HK' => 'Hong Kong',
    'HM' => 'Heard Island and McDonald Islands',
    'HN' => 'Honduras',
    'HR' => 'Croatia',
    'HT' => 'Haiti',
    'HU' => 'Hungary',
    'ID' => 'Indonesia',
    'IE' => 'Ireland',
    'IL' => 'Israel',
    'IN' => 'India',
    'IO' => 'British Indian Ocean Territory',
    'IQ' => 'Iraq',
    'IR' => 'Islamic Republic of Iran',
    'IS' => 'Iceland',
    'IT' => 'Italy',
    'JE' => 'Jerse',
    'JM' => 'Jamaica',
    'JO' => 'Jordan',
    'JP' => 'Japan',
    'KE' => 'Kenya',
    'KG' => 'Kyrgyzstan',
    'KH' => 'Cambodia',
    'KI' => 'Kiribati',
    'KM' => 'Comoros',
    'KN' => 'Saint Kitts and Nevis',
    'KP' => 'Democratic Peoples Republic of Korea',
    'KR' => 'Republic of Korea',
    'KW' => 'Kuwait',
    'KY' => 'Cayman Islands',
    'KZ' => 'Kazakhstan',
    'LA' => 'Lao Peoples Democratic Republic',
    'LB' => 'Lebanon',
    'LC' => 'Saint Lucia',
    'LI' => 'Liechtenstein',
    'LK' => 'Sri Lanka',
    'LR' => 'Liberia',
    'LS' => 'Lesotho',
    'LT' => 'Lithuania',
    'LU' => 'Luxembourg',
    'LV' => 'Latvia',
    'LY' => 'Libyan Arab Jamahiriya',
    'MA' => 'Morocco',
    'MC' => 'Monaco',
    'MD' => 'Republic of Moldova',
    'ME' => 'Montenegro',
    'MG' => 'Madagascar',
    'MH' => 'Marshall Islands',
    'MK' => 'The Former Yugoslav Republic of Macedonia',
    'ML' => 'Mali',
    'MM' => 'Myanmar',
    'MN' => 'Mongolia',
    'MO' => 'Macao',
    'MP' => 'Northern Mariana Islands',
    'MQ' => 'Martinique',
    'MR' => 'Mauritania',
    'MS' => 'Montserrat',
    'MT' => 'Malta',
    'MU' => 'Mauritius',
    'MV' => 'Maldives',
    'MW' => 'Malawi',
    'MX' => 'Mexico',
    'MY' => 'Malaysia',
    'MZ' => 'Mozambique',
    'NA' => 'Namibia',
    'NC' => 'New Caledonia',
    'NE' => 'Niger',
    'NF' => 'Norfolk Island',
    'NG' => 'Nigeria',
    'NI' => 'Nicaragua',
    'NL' => 'Netherlands',
    'NO' => 'Norway',
    'NP' => 'Nepal',
    'NR' => 'Nauru',
    'NU' => 'Niue',
    'NZ' => 'New Zealand',
    'OM' => 'Oman',
    'PA' => 'Panama',
    'PE' => 'Peru',
    'PF' => 'French Polynesia. Includes Tahiti',
    'PG' => 'Papua New Guinea',
    'PH' => 'Philippines',
    'PK' => 'Pakistan',
    'PL' => 'Poland',
    'PM' => 'Saint Pierre and Miquelon',
    'PN' => 'Pitcairn',
    'PR' => 'Puerto Rico',
    'PS' => 'Palestinian territory, Occupied',
    'PT' => 'Portugal',
    'PW' => 'Palau',
    'PY' => 'Paraguay',
    'QA' => 'Qatar',
    'RE' => 'Reunion',
    'RO' => 'Romania',
    'RS' => 'Serbi',
    'RU' => 'Russian Federation',
    'RW' => 'Rwanda',
    'SA' => 'Saudi Arabia',
    'SB' => 'Solomon Islands',
    'SC' => 'Seychelles',
    'SD' => 'Sudan',
    'SE' => 'Sweden',
    'SG' => 'Singapore',
    'SH' => 'Saint Helena',
    'SI' => 'Slovenia',
    'SJ' => 'Svalbard and Jan Mayen',
    'SK' => 'Slovakia',
    'SL' => 'Sierra Leone',
    'SM' => 'San Marino',
    'SN' => 'Senegal',
    'SO' => 'Somalia',
    'SR' => 'Suriname',
    'ST' => 'Sao Tome and Principe',
    'SV' => 'El Salvador',
    'SY' => 'Syrian Arab Republic',
    'SZ' => 'Swaziland',
    'TC' => 'Turks and Caicos Islands',
    'TD' => 'Chad',
    'TF' => 'French Southern Territories',
    'TG' => 'Togo',
    'TH' => 'Thailand',
    'TJ' => 'Tajikistan',
    'TK' => 'Tokelau',
    'TM' => 'Turkmenistan',
    'TN' => 'Tunisia',
    'TO' => 'Tonga',
    'TP' => 'No longer in use',
    'TR' => 'Turkey',
    'TT' => 'Trinidad and Tobago',
    'TV' => 'Tuvalu',
    'TW' => 'Taiwan, Province of China',
    'TZ' => 'Tanzania, United Republic of',
    'UA' => 'Ukraine',
    'UG' => 'Uganda',
    'US' => 'United States',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistan',
    'VA' => 'Holy See (Vatican City state)',
    'VC' => 'Saint Vincent and the Grenadines',
    'VE' => 'Venezuela',
    'VG' => 'Virgin Islands, British',
    'VI' => 'Virgin Islands, U.S',
    'VN' => 'Vietnam',
    'VU' => 'Vanuatu',
    'WF' => 'Wallis and Futuna',
    'WS' => 'Samoa',
    'YE' => 'Yemen',
    'YT' => 'Mayotte',
    'ZA' => 'South Africa',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabwe',
    'ZZ' => 'Unknown country',
);

$shipping_contry = array(
    'Americas' => 'North, South, or Latin America',
    'Asia' => 'Seller will ship to all countries in continental Asia.',
    'AT' => 'Austria',
    'AU' => 'Australia',
    'BE' => 'Belgium',
    'CA' => 'Canada',
    'CH' => 'Switzerland',
    'CN' => 'China',
    'DE' => 'Germany',
    'ES' => 'Spain',
    'Europe' => '	Seller will ship to all countries in Europe.',
    'EuropeanUnion' => 'Seller will ship to European Union member countries.',
    'FR' => 'France',
    'GB' => 'United Kingdom',
    'Greater China' => 'China and surrounding Chinese territories.',
    'HK' => 'Hong Kong',
    'IE' => 'Ireland',
    'IN' => 'India',
    'IT' => 'Italy',
    'JP' => 'Japan',
    'MX' => 'Mexico',
    'NL' => 'Netherlands',
    'None' => 'No shipping. Buyer must pick up the item.',
    'NZ' => 'New Zealand',
    'Rest of Asia	' => 'Asian territories outside of continental Asia.',
    'TW' => 'Taiwan',
    'US' => 'United States',
    'Worldwide' => 'Worldwide',
);

$Shipping_services = array(
    'InternationalPriorityShipping' => 'International Priority Shipping',
);

settings_errors();

?>

<div id="wrapper" class="ebay-settings">

	<h2 class="main-heading">Ebay Settings </h2>

	<div id="dashboard_right_now" class="formcontainer postbox">

		<div class="toggle-handler">
			<h2 class="hndle ui-sortable-handle"><span>General Settings</span></h2>
		</div>
		<div class="inside">

			<div class="main">

				<form method="post" action="options.php">

					<?php settings_fields('ebay-settings-group'); ?>

					<?php do_settings_sections('ebay-settings-group'); ?>

					<table class="form-table button-setting-table">

						<tbody>

							<tr id="row_multiebaystorewcconnect_general_settings_mode">
								<td class="label">
									<label for="multiebaystorewcconnect_general_settings_mode">
										<span>Mode</span>
									</label>
								</td>
								<td class="value">
									<select id="multiebaystorewcconnect_general_settings_mode" name="ebaygeneral_settings_mode">
										<option value="1" <?php echo (get_option('ebaygeneral_settings_mode') == '1') ? 'selected' : ''; ?>>Sandbox</option>
										<option value="2" <?php if (get_option('ebaygeneral_settings_mode') == '2') {
    echo 'selected';
} ?>>Production</option>
									</select>
								</td>
							</tr>
                            
                            <tr id="row_multiebaystorewcconnect_general_settings_dev_id">
                                <td class="label">
                                    <label for="multiebaystorewcconnect_general_settings_mode">
                                        <span>Dev ID</span>
                                    </label>
                                </td>
                                <td class="value">
                                    <input id="multiebaystorewcconnect_general_settings_dev_id" name="ebay_dev_id" value="<?php echo get_option('ebay_dev_id') ? get_option('ebay_dev_id') : ''; ?>" type="text">
                                </td>
                            </tr>
                            
                            <tr id="row_multiebaystorewcconnect_general_settings_app_id">
                                <td class="label">
                                    <label for="multiebaystorewcconnect_general_settings_mode">
                                        <span>App ID</span>
                                    </label>
                                </td>
                                <td class="value">
                                    <input id="multiebaystorewcconnect_general_settings_app_id" name="ebay_app_id" value="<?php echo get_option('ebay_app_id') ? get_option('ebay_app_id') : ''; ?>" type="text">
                                </td>
                            </tr>
                            
                            <tr id="row_multiebaystorewcconnect_general_settings_cert_id">
                                <td class="label">
                                    <label for="multiebaystorewcconnect_general_settings_mode">
                                        <span>Cert ID</span>
                                    </label>
                                </td>
                                <td class="value">
                                    <input id="multiebaystorewcconnect_general_settings_cert_id" name="ebay_cert_id" value="<?php echo get_option('ebay_cert_id') ? get_option('ebay_cert_id') : ''; ?>" type="text">
                                </td>
                            </tr>
                            
                            <tr id="row_multiebaystorewcconnect_general_settings_ru_name">
                                <td class="label">
                                    <label for="multiebaystorewcconnect_general_settings_mode">
                                        <span>RuName</span>
                                    </label>
                                </td>
                                <td class="value">
                                    <input id="multiebaystorewcconnect_general_settings_ru_name" name="ebay_ru_name" value="<?php echo get_option('ebay_ru_name') ? get_option('ebay_ru_name') : ''; ?>" type="text">
                                </td>
                            </tr>
                            
                            <tr id="row_multiebaystorewcconnect_general_settings_ebay_backlink">
                                <td class="label">
                                    <label for="multiebaystorewcconnect_general_settings_mode">
                                        <span>Back link</span>
                                    </label>
                                </td>
                                <td class="value">
                                    <input readonly="true" id="multiebaystorewcconnect_general_settings_ebay_backlink" name="ebay_backlink" value="<?php echo $_SERVER['REQUEST_SCHEME'] . "://" .$_SERVER['HTTP_HOST']; ?>/wp-content/plugins/ebay-connector/ebay/redirect.php" type="text">
                                    <p class="notes">
                                        <i>Copy for config ebay app</i>
                                    </p>
                                </td>
                            </tr>
                            
                            
                            
							<tr id="row_multiebaystorewcconnect_payment_options_default_category">

								<td class="label">

									<label for="multiebaystorewcconnect_payment_options_default_category">

										<span>Default Category</span>

									</label>

								</td>

								<td class="value">

									<select id="multiebaystorewcconnect_payment_options_default_category" name="ebay_default_category">

										<?php

                                        if (!empty($product_categories)) {
                                            foreach ($product_categories as $cat_key => $cat_value) : ?>

												<option value="<?php echo $cat_value->term_id; ?>" <?php echo (get_option('ebay_default_category') == $cat_value->term_id) ? 'selected' : ''; ?>><?php echo $cat_value->name; ?></option>

											<?php

                                        endforeach;
                                        } else {
                                            echo '<option value="">No categories Available</option>';
                                        }

                                    ?>

									</select>
									<p class="notes">
										<i>When ebay category not maped with woocommerce then product assign in this category</i>
									</p>
								</td>
							</tr>
							<tr id="row_multiebaystorewcconnect_payment_options_order_status">
								<td class="label">
									<label for="multiebaystorewcconnect_payment_options_order_status">
										<span>eBay Imported Order Status</span>
									</label>
								</td>

								<td class="value">

									<select id="multiebaystorewcconnect_payment_options_order_status" name="ebay_order_status">

										<option value="">-- Please Select --</option>

										<?php foreach ($order_status as $o_key => $o_value) : ?>

											<option value="<?php echo $o_key; ?>" <?php echo (get_option('ebay_order_status') == $o_key) ? 'selected' : ''; ?>><?php echo $o_value; ?></option>

										<?php endforeach; ?>

									</select>

									<p class="notes">
										<i>Set default order status for order which imported from eBay</i>
									</p>
								</td>
							</tr>
							<tr id="row_multiebaystorewcconnect_payment_options_pro_qty">
								<td class="label">
									<label for="multiebaystorewcconnect_payment_options_pro_qty">
										<span>Defaut Product Quantity</span>
									</label>
								</td>
								<td class="value">
									<input id="multiebaystorewcconnect_payment_options_pro_qty" name="ebay_default_stock" value="<?php echo get_option('ebay_default_stock') ? get_option('ebay_default_stock') : ''; ?>" type="text">
									<p class="notes">
										<span>Set default product quantity for product which imported to eBay if product quantity is zero on store</span>
									</p>
								</td>
							</tr>

							</tr>
							<tr id="row_multiebaystorewcconnect_export_product_start_cron">
								<td class="label">
									<label for="multiebaystorewcconnect_export_product_start_cron">
										<span>Start Auto Update</span>
									</label>
								</td>
								<td class="value">

									<input id="multiebaystorewcconnect_export_product_start_cron" name="start_cron" value="<?php echo (get_option('start_cron')) ? get_option('start_cron') : true; ?>" type="checkbox" <?php echo (get_option('start_cron')) ? 'checked' : ''; ?>>
								</td>

							</tr>
								<tr>
								<td class="label">
									<label for="multiebaystoremageconnect_get_process_for_cron">
										<span>Select Process For Auto Sync</span>
									</label>
								</td>
								<td class="value">
									<select id="multiebaystoremageconnect_get_process_for_cron" name="get_process_for_cron">
										<option value='-1' <?php echo (get_option('get_process_for_cron') == '-1') ? 'selected' : ''; ?>>Select</option>
										<option value='1' <?php echo (get_option('get_process_for_cron') == '1') ? 'selected' : ''; ?>>Export Product to Ebay</option>
										<option value='2' <?php echo (get_option('get_process_for_cron') == '2') ? 'selected' : ''; ?>>Update Product From Ebay To Woo Store</option>
										<option value='3' <?php echo (get_option('get_process_for_cron') == '3') ? 'selected' : ''; ?>>Update Product From Woo Store To Ebay</option>
									</select>
								</td>

							</tr>

							<tr id="row_multiebaystorewcconnect_export_product_start_cron_from_date">
								<td class="label">
									<label for="multiebaystorewcconnect_export_product_start_cron_from_date">
										<span>Start From</span>
									</label>
								</td>
								<td class="value">
									<input type="text" name="wc_ebay_cron_start_date" id="multiebaystorewcconnect_export_product_start_cron_from_date" class="wc-ebay-start-datepicker" value="<?php echo (get_option('wc_ebay_cron_start_date')) ? get_option('wc_ebay_cron_start_date') : ''; ?>" />
								</td>

							</tr>

							<tr id="row_multiebaystorewcconnect_export_product_start_cron_to_date">
								<td class="label">
									<label for="multiebaystorewcconnect_export_product_start_cron_to_date">
										<span>End To</span>
									</label>
								</td>
								<td class="value">
									<input type="text" name="wc_ebay_cron_end_date" id="multiebaystorewcconnect_export_product_start_cron_to_date" class="wc-ebay-start-datepicker" value="<?php echo (get_option('wc_ebay_cron_end_date')) ? get_option('wc_ebay_cron_end_date') : ''; ?>" />
								</td>

							</tr>
							<tr id="row_multiebaystorewcconnect_export_product_cron_interval">
								<td class="label">
									<label for="multiebaystorewcconnect_export_product_cron_interval">
										<span>Enter Update Interval(in hours)</span>
									</label>
								</td>
								<td class="value">

									<input id="multiebaystorewcconnect_export_product_cron_interval" name="cron_interval" placeholder="1" value="<?php echo (get_option('cron_interval')) ? get_option('cron_interval') : ''; ?>" min="1" type="number">

								</td>

							</tr>


							<tr id="row_multiebaystorewcconnect_ebay_accounts">
								<td class="label">
									<label for="multiebaystorewcconnect_ebay_accounts">
										<span>Select Ebay Account</span>
									</label>
								</td>

								<td class="value">

									<select id="multiebaystorewcconnect_select_account" name="ebay_accounts">

										<option value="">-- Please Select --</option>

										<?php foreach ($accounts as $a_key => $a_value) : ?>

											<option value="<?php echo $a_value['id']; ?>" <?php echo (get_option('ebay_accounts') == $a_value['id']) ? 'selected' : ''; ?>><?php echo $a_value['store_name']; ?></option>

										<?php endforeach; ?>

									</select>

									<p class="notes">
										<i>Assign account from which you want to update products</i>
									</p>
								</td>
							</tr>
							<tr id="row_multiebaystorewcconnect_payment_options_paypal_id">
								<td class="label">
									<label for="multiebaystorewcconnect_payment_options_paypal_id">
										<span>Email Address for report of update sync</span>
									</label>
								</td>
								<td class="value">

									<input id="multiebaystorewcconnect_payment_options_paypal_id" name="ebay_email_id" placeholder="test@webkul.com" value="<?php echo (get_option('ebay_email_id')) ? get_option('ebay_email_id') : ''; ?>" type="text">

								</td>
							</tr>

							<tr id="row_multiebaystorewcconnect_item_location_pincode">
								<td class="label">
									<label for="multiebaystorewcconnect_item_location_pincode">
										<span>Pin code of item location</span>
									</label>
								</td>
								<td class="value">

									<input id="multiebaystorewcconnect_item_location_pincode" name="item_location_pincode" placeholder="123456" value="<?php echo (get_option('item_location_pincode')) ? get_option('item_location_pincode') : ''; ?>" type="text">

								</td>
							</tr>

							<tr>
								<td class="label">
									<label for="multiebaystoremageconnect_global_shipping">
										<span>Global Shipping</span>
									</label>
								</td>
								<td class="value">
									<select id="multiebaystoremageconnect_global_shipping" name="global_shipping">
										<option value='1' <?php echo (get_option('global_shipping') == '1') ? 'selected' : ''; ?>>Yes</option>
										<option value='0' <?php echo (get_option('global_shipping') == '0') ? 'selected' : ''; ?>>No</option>
									</select>
								</td>

							</tr>

						</tbody>

					</table>

			</div>
		</div>
	</div>
	<div id="dashboard_right_now" class="formcontainer postbox">

		<div class="toggle-handler">

			<h2 class="hndle ui-sortable-handle"><span>Payment Details</span></h2>

		</div>

		<div class="inside">

			<!-- <div class="main">

				<table class="form-table user-access-table">

					<tbody>

						<tr id="row_multiebaystorewcconnect_payment_options_paypal_id">
							<td class="label">
								<label for="multiebaystorewcconnect_payment_options_paypal_id">
									<span>Paypal Email Address</span>
								</label>
							</td>
							<td class="value">

								<input id="multiebaystorewcconnect_payment_options_paypal_id" name="ebay_paypal_id" placeholder="paypal@webkul.com" value="<?php echo (get_option('ebay_paypal_id')) ? get_option('ebay_paypal_id') : ''; ?>" type="text">

							</td>
						</tr>


					</tbody>
				</table>
			</div> -->


			<div class="toggle-handler">

				<h2 class="hndle ui-sortable-handle"><span>Return Policy</span></h2>

			</div>

			<div class="inside">

				<div class="main">

						<?php settings_fields('ebay-settings-group'); ?>

						<?php do_settings_sections('ebay-settings-group'); ?>

						<table class="form-table user-access-table">

							<tbody>
								<tr id="row_multiebaystoremageconnect_returns_policy_options_define">
									<td class="label">
										<label for="multiebaystoremageconnect_returns_policy_options_define">
											<span>Define Return Policy</span>
										</label>
									</td>
									<td class="value">
										<select id="multiebaystoremageconnect_returns_policy_options_define" name="returns_policy_options_define">
											<option value="ReturnsAccepted" <?php echo (get_option('returns_policy_options_define') == 'ReturnsAccepted') ? 'selected' : ''; ?>>Returns Accepted</option>
											<option value="ReturnsNotAccepted" <?php echo (get_option('returns_policy_options_define') == 'ReturnsNotAccepted') ? 'selected' : ''; ?>>Returns Not Accepted</option>
										</select>
									</td>
								</tr>
								<tr id="row_multiebaystoremageconnect_returns_policy_options_refundOption">
									<td class="label">
										<label for="multiebaystoremageconnect_returns_policy_options_refundOption">
											<span>Refund Option</span>
										</label>
									</td>
									<td class="value">
										<select id="multiebaystoremageconnect_returns_policy_options_refundOption" name="returns_policy_options_refundOption">
											<option value="ReturnsAccepted" <?php echo (get_option('returns_policy_options_refundOption') == 'MoneyBack') ? 'selected' : ''; ?>>Money Back</option>
											<option value="Replacement" <?php echo (get_option('returns_policy_options_refundOption') == 'Replacement') ? 'selected' : ''; ?>>Replacement</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="label">
										<label for="multiebaystoremageconnect_returns_policy_options_return_days">
											<span>Returns within</span>
										</label>
									</td>
									<td class="value">
										<select id="multiebaystoremageconnect_returns_policy_options_return_days" name="returns_policy_options_return_days">
											<option value="Days_14" <?php echo (get_option('returns_policy_options_return_days') == 'Days_14') ? 'selected' : ''; ?>>14 Days</option>
											<option value="Days_30" <?php echo (get_option('returns_policy_options_return_days') == 'Days_30') ? 'selected' : ''; ?>>30 Days</option>
											<option value="Days_60" <?php echo (get_option('returns_policy_options_return_days') == 'Days_60') ? 'selected' : ''; ?>>60 Days</option>
										</select>
									</td>

								</tr>
								<tr>
									<td class="label">
										<label for="multiebaystoremageconnect_returns_policy_options_pay_by">
											<span>Pay By</span>
										</label>
									</td>
									<td class="value">
										<select id="multiebaystoremageconnect_returns_policy_options_pay_by" name="returns_policy_options_pay_by">
											<option value="Buyer" <?php echo (get_option('returns_policy_options_pay_by') == 'Buyer') ? 'selected' : ''; ?>>Buyer</option>
											<option value="Seller" <?php echo (get_option('returns_policy_options_pay_by') == 'Seller') ? 'selected' : ''; ?>>Seller</option>
										</select>
									</td>

								</tr>
								<tr>
									<td class="label">
										<label for="multiebaystoremageconnect_returns_policy_options_other_info">
											<span>Other Information</span>
										</label>
									</td>
									<td class="value">
										<textarea id="multiebaystoremageconnect_returns_policy_options_other_info" name="returns_policy_options_other_info" rows="2" cols="15"><?php echo get_option('returns_policy_options_other_info') ? get_option('returns_policy_options_other_info') : ''; ?></textarea>
									</td>

								</tr>
								<tr>
									<td class="label">
										<label for="multiebaystoremageconnect_countrycodetype">
											<span>Default CountryCodeType( Required While Product Export )</span>
										</label>
									</td>
									<td class="value">

										<select id="multiebaystoremageconnect_countrycodetype" name="countrycodetype">
											<?php

                                            foreach ($countryCodes as $codeKey => $codeValue) {
                                                if ($codeKey == $countryCodeselected) {
                                                    $selectedcode = 'selected';
                                                } else {
                                                    $selectedcode = '';
                                                }
                                                echo '<option value='.$codeKey." $selectedcode >".$codeValue.'</option>';
                                            }
                                            ?>
										</select>
									</td>

								</tr>
							</tbody>
						</table>

				</div>


				<div class="toggle-handler">

					<h2 class="hndle ui-sortable-handle"><span>Listing Options</span></h2>

				</div>

				<div class="inside">

					<div class="main">

							<?php settings_fields('ebay-settings-group'); ?>

							<?php do_settings_sections('ebay-settings-group'); ?>


							<table class="form-table user-access-table">

								<tbody>
									<tr>
										<td class="label">
											<label for="multiebaystoremageconnect_listing_options_duration">
												<span>Listing Duration</span>
											</label>
										</td>
										<td class="value">
											<select id="multiebaystoremageconnect_listing_options_duration" name="ebay_listing_options_duration">
												<option value="GTC" <?php echo (get_option('ebay_listing_options_duration') == 'GTC') ? 'selected' : ''; ?>>Good Till Canceled</option>
												<option value="Days_1" <?php echo (get_option('ebay_listing_options_duration') == 'Days_1') ? 'selected' : ''; ?>>1 Day</option>
												<option value="Days_3" <?php echo (get_option('ebay_listing_options_duration') == 'Days_3') ? 'selected' : ''; ?>>3 Day</option>
												<option value="Days_5" <?php echo (get_option('ebay_listing_options_duration') == 'Days_5') ? 'selected' : ''; ?>>5 Day</option>
												<option value="Days_7" <?php echo (get_option('ebay_listing_options_duration') == 'Days_7') ? 'selected' : ''; ?>>7 Day</option>
												<option value="Days_10" <?php echo (get_option('ebay_listing_options_duration') == 'Days_10') ? 'selected' : ''; ?>>10 Day</option>
												<option value="Days_30" <?php echo (get_option('ebay_listing_options_duration') == 'Days_30') ? 'selected' : ''; ?>>30 Day</option>
											</select>
										</td>
									</tr>
								</tbody>

							</table>

					</div>


					<div class="toggle-handler">

						<h2 class="hndle ui-sortable-handle"><span>Dispatch Options</span></h2>

					</div>

					<div class="inside">

						<div class="main">

								<?php settings_fields('ebay-settings-group'); ?>

								<?php do_settings_sections('ebay-settings-group'); ?>


								<table class="form-table user-access-table">

									<tbody>
										<tr>
											<td class="label">
												<label for="multiebaystoremageconnect_dispatch_options_time">
													<span>Dispatch Time</span>
												</label>
											</td>
											<td class="value">
												<select id="multiebaystoremageconnect_dispatch_options_time" name="dispatch_options_time">
													<option value="0" <?php echo (get_option('dispatch_options_time') == '0') ? 'selected' : ''; ?>>0 Days</option>
													<option value="1" <?php echo (get_option('dispatch_options_time') == '1') ? 'selected' : ''; ?>>1 Days</option>
													<option value="2" <?php echo (get_option('dispatch_options_time') == '2') ? 'selected' : ''; ?>>2 Days</option>
													<option value="3" <?php echo (get_option('dispatch_options_time') == '3') ? 'selected' : ''; ?>>3 Days</option>
													<option value="4" <?php echo (get_option('dispatch_options_time') == '4') ? 'selected' : ''; ?>>4 Days</option>
													<option value="5" <?php echo (get_option('dispatch_options_time') == '5') ? 'selected' : ''; ?>>5 Days</option>
													<option value="10" <?php echo (get_option('dispatch_options_time') == '10') ? 'selected' : ''; ?>>10 Days</option>
													<option value="15" <?php echo (get_option('dispatch_options_time') == '15') ? 'selected' : ''; ?>>15 Days</option>
													<option value="20" <?php echo (get_option('dispatch_options_time') == '20') ? 'selected' : ''; ?>>20 Days</option>
													<option value="30" <?php echo (get_option('dispatch_options_time') == '30') ? 'selected' : ''; ?>>30 Days</option>
												</select>
											</td>

										</tr>
									</tbody>

								</table>

						</div>

						<div class="toggle-handler">

							<h2 class="hndle ui-sortable-handle"><span>International Shipping Service</span></h2>

						</div>

						<div class="inside">

							<div class="main">

									<?php settings_fields('ebay-settings-group'); ?>

									<?php do_settings_sections('ebay-settings-group'); ?>


									<table class="form-table user-access-table">

										<tbody>

											<tr>
												<td class="label">
													<label for="multiebaystoremageconnect_international_shipping_options_ship_service">
														<span>International Shipping Service</span>
													</label></td>
												<td class="value">
													<select id="multiebaystoremageconnect_international_shipping_options_ship_service" name="international_shipping_options_ship_service">
														<option value="NotSelected">-</option>
														<option value="USPSFirstClassMailInternational" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSFirstClassMailInternational') ? 'selected' : ''; ?>>USPS First Class Mail International / First Class Package International Service </option>
														<option value="USPSPriorityMailInternational" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSPriorityMailInternational') ? 'selected' : ''; ?>>USPS Priority Mail International </option>
														<option value="USPSPriorityMailInternationalFlatRateEnvelope" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSPriorityMailInternationalFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail International Flat Rate Envelope </option>
														<option value="USPSPriorityMailInternationalSmallFlatRateBox" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSPriorityMailInternationalSmallFlatRateBox') ? 'selected' : ''; ?>>USPS Priority Mail International Small Flat Rate Box </option>
														<option value="USPSPriorityMailInternationalFlatRateBox" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSPriorityMailInternationalFlatRateBox') ? 'selected' : ''; ?>>USPS Priority Mail International Medium Flat Rate Box </option>
														<option value="USPSPriorityMailInternationalLargeFlatRateBox" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSPriorityMailInternationalLargeFlatRateBox') ? 'selected' : ''; ?>>USPS Priority Mail International Large Flat Rate Box </option>
														<option value="USPSPriorityMailInternationalPaddedFlatRateEnvelope" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSPriorityMailInternationalPaddedFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail International Padded Flat Rate Envelope </option>
														<option value="USPSPriorityMailInternationalLegalFlatRateEnvelope" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSPriorityMailInternationalLegalFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail International Legal Flat Rate Envelope </option>
														<option value="USPSExpressMailInternational" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSExpressMailInternational') ? 'selected' : ''; ?>>USPS Priority Mail Express International </option>
														<option value="USPSExpressMailInternationalFlatRateEnvelope" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSExpressMailInternationalFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail Express International Flat Rate Envelope </option>
														<option value="USPSExpressMailInternationalLegalFlatRateEnvelope" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSExpressMailInternationalLegalFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail Express International Legal Flat Rate Envelope </option>
														<option value="USPSFirstClassMailInternationalLetter" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSFirstClassMailInternationalLetter') ? 'selected' : ''; ?>>USPS First Class Mail International Letter </option>
														<option value="USPSFirstClassMailInternationalLargeEnvelope" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSFirstClassMailInternationalLargeEnvelope') ? 'selected' : ''; ?>>USPS First Class Mail International Large Envelope </option>
														<option value="USPSFirstClassMailInternationalParcel" <?php echo (get_option('international_shipping_options_ship_service') == 'USPSFirstClassMailInternationalParcel') ? 'selected' : ''; ?>>USPS First Class Mail International Parcel </option>
														<optgroup label="------------------------------------" class="cell-selectOptgrp">
														</optgroup>
														<option value="UPSWorldWideExpressPlus" <?php echo (get_option('international_shipping_options_ship_service') == 'UPSWorldWideExpressPlus') ? 'selected' : ''; ?>>UPS Worldwide Express Plus </option>
														<option value="UPSWorldWideExpress" <?php echo (get_option('international_shipping_options_ship_service') == 'UPSWorldWideExpress') ? 'selected' : ''; ?>>UPS Worldwide Express </option>
														<option value="UPSWorldWideExpedited" <?php echo (get_option('international_shipping_options_ship_service') == 'UPSWorldWideExpedited') ? 'selected' : ''; ?>>UPS Worldwide Expedited </option>
														<option value="UPSWorldwideSaver" <?php echo (get_option('international_shipping_options_ship_service') == 'UPSWorldwideSaver') ? 'selected' : ''; ?>>UPS Worldwide Saver </option>
														<option value="UPSStandardToCanada" <?php echo (get_option('international_shipping_options_ship_service') == 'UPSStandardToCanada') ? 'selected' : ''; ?>>UPS Standard to Canada </option>
														<optgroup label="------------------------------------" class="cell-selectOptgrp">
														</optgroup>
														<option value="FedExInternationalEconomy" <?php echo (get_option('international_shipping_options_ship_service') == 'FedExInternationalEconomy') ? 'selected' : ''; ?>>FedEx International Economy </option>
														<option value="FedExInternationalPriority" <?php echo (get_option('international_shipping_options_ship_service') == 'FedExInternationalPriority') ? 'selected' : ''; ?>>FedEx International Priority </option>
														<option value="FedExGroundInternationalToCanada" <?php echo (get_option('international_shipping_options_ship_service') == 'FedExGroundInternationalToCanada') ? 'selected' : ''; ?>>FedEx Ground International to Canada </option>
														<optgroup label="------------------------------------" class="cell-selectOptgrp"></optgroup>
														<option value="StandardInternational" <?php echo (get_option('international_shipping_options_ship_service') == 'StandardInternational') ? 'selected' : ''; ?>>Standard International Shipping </option>
														<option value="ExpeditedInternational" <?php echo (get_option('international_shipping_options_ship_service') == 'ExpeditedInternational') ? 'selected' : ''; ?>>Expedited International Shipping </option>
														<option value="OtherInternational" <?php echo (get_option('international_shipping_options_ship_service') == 'OtherInternational') ? 'selected' : ''; ?>>Economy International Shipping </option>
														<option value="ExpeditedDeliveryToRussia" <?php echo (get_option('international_shipping_options_ship_service') == 'ExpeditedDeliveryToRussia') ? 'selected' : ''; ?>>Expedited International Courier Delivery to Russia </option>
														<option value="US_RUTrackedFromChina" <?php echo (get_option('international_shipping_options_ship_service') == 'US_RUTrackedFromChina') ? 'selected' : ''; ?>>RU Tracked Packet from China </option>
														<optgroup label="------------------------------------" class="cell-selectOptgrp"></optgroup>
														<option value="US_IntlEconomyShippingFromGC" <?php echo (get_option('international_shipping_options_ship_service') == 'US_IntlEconomyShippingFromGC') ? 'selected' : ''; ?>>Economy Shipping from China/Hong Kong/Taiwan to worldwide </option>
														<option value="US_IntlStandardShippingFromGC" <?php echo (get_option('international_shipping_options_ship_service') == 'US_IntlStandardShippingFromGC') ? 'selected' : ''; ?>>Standard Shipping from China/Hong Kong/Taiwan to worldwide </option>
														<option value="US_IntlExpeditedShippingFromGC" <?php echo (get_option('international_shipping_options_ship_service') == 'US_IntlExpeditedShippingFromGC') ? 'selected' : ''; ?>>Expedited Shipping from China/Hong Kong/Taiwan to worldwide </option>
														<option value="US_IntlEconomySppedPAK" <?php echo (get_option('international_shipping_options_ship_service') == 'US_IntlEconomySppedPAK') ? 'selected' : ''; ?>>Economy SpeedPAK from China/Hong Kong/Taiwan </option>
														<option value="US_IntlStandardSppedPAK" <?php echo (get_option('international_shipping_options_ship_service') == 'US_IntlStandardSppedPAK') ? 'selected' : ''; ?>>Standard SpeedPAK from China/Hong Kong/Taiwan </option>
														<option value="US_IntlExpeditedSppedPAK" <?php echo (get_option('international_shipping_options_ship_service') == 'US_IntlExpeditedSppedPAK') ? 'selected' : ''; ?>>Expedited SpeedPAK from China/Hong Kong/Taiwan </option>
													</select>
													<p class="notes">
														<span>Woocommerce product created on eBay With this International Shipping Method</span>
													</p>
												</td>

											</tr>

											<tr>
												<td class="label">
													<label for="multiebaystoremageconnect_international_shipping_options_ship_service_priority">
														<span>International Shipping Service Priority</span>
													</label>
												</td>
												<td class="value">
													<input id="multiebaystoremageconnect_international_shipping_options_ship_service_priority" name="international_shipping_options_ship_service_priority" value="<?php echo get_option('international_shipping_options_ship_service_priority') ? get_option('international_shipping_options_ship_service_priority') : ''; ?>" type="text">
												</td>

											</tr>

											<tr>
												<td class="label">
													<label for="multiebaystoremageconnect_international_shipping_options_ship_service_cost">
														<span>International Shipping Service Cost</span>
													</label>
												</td>
												<td class="value">
													<input id="multiebaystoremageconnect_international_shipping_options_ship_service_cost" name="international_shipping_options_ship_service_cost" value="<?php echo get_option('international_shipping_options_ship_service_cost') ? get_option('international_shipping_options_ship_service_cost') : ''; ?>" type="text">
													<p class="notes">
														<span>Shipping Amount that apply on product when sync with ebay</span>
													</p>
												</td>

											</tr>
											<tr>
												<td class="label">
													<label for="multiebaystoremageconnect_international_shipping_options_ship_service_additional_cost">
														<span>International Shipping Service Additional Cost</span>
													</label>
												</td>
												<td class="value">
													<input id="multiebaystoremageconnect_international_shipping_options_ship_service_additional_cost" name="international_shipping_options_ship_service_additional_cost" value="<?php echo get_option('international_shipping_options_ship_service_additional_cost') ? get_option('international_shipping_options_ship_service_additional_cost') : ''; ?>" type="text">
													<p class="notes">
														<span>Additional Shipping Amount that apply on product when sync with ebay</span>
													</p>
												</td>
											</tr>

											<tr>
												<td class="label">
													<label for="multiebaystoremageconnect_shipCountryCodeType">
														<span>Ship to location( Required While Product Export )</span>
													</label>
												</td>
												<td class="value">

													<select id="multiebaystoremageconnect_shipCountryCodeType" name="shipCountryCodeType">
														<?php

                                                        foreach ($shipping_contry as $codeKey => $codeValue) {
                                                            if ($codeKey == $shipCountryCodeSelected) {
                                                                $selectedcode = 'selected';
                                                            } else {
                                                                $selectedcode = '';
                                                            }
                                                            echo '<option value='.$codeKey." $selectedcode >".$codeValue.'</option>';
                                                        }
                                                        ?>
													</select>
												</td>

											</tr>
										</tbody>

									</table>

							</div>



							<div class="toggle-handler">

								<h2 class="hndle ui-sortable-handle"><span>Domestic Shipping Options</span></h2>

							</div>

							<div class="inside">

								<div class="main">

										<?php settings_fields('ebay-settings-group'); ?>

										<?php do_settings_sections('ebay-settings-group'); ?>

										<table class="form-table user-access-table">

											<tbody>

												<tr>
													<td class="label">
														<label for="multiebaystoremageconnect_shipping_options_ship_service_priority">
															<span>Shipping Service Priority</span>
														</label>
													</td>
													<td class="value">
														<input id="multiebaystoremageconnect_shipping_options_ship_service_priority" name="shipping_options_ship_service_priority" value="<?php echo get_option('shipping_options_ship_service_priority') ? get_option('shipping_options_ship_service_priority') : ''; ?>" type="text">
													</td>

												</tr>
												<tr>
													<td class="label">
														<label for="multiebaystoremageconnect_shipping_options_ship_service">
															<span>Shipping Service</span>
														</label></td>
													<td class="value">
														<select id="multiebaystoremageconnect_shipping_options_ship_service" name="shipping_options_ship_service">
															<option value="NotSelected">-</option>
															<optgroup label="Economy services">
																<option value="USPSParcel" <?php echo (get_option('shipping_options_ship_service') == 'USPSParcel') ? 'selected' : ''; ?>>USPS Parcel Select Ground (2 to 9 business days)</option>
																<option value="USPSMedia" <?php echo (get_option('shipping_options_ship_service') == 'USPSMedia') ? 'selected' : ''; ?>>USPS Media Mail (2 to 8 business days)</option>
																<option value="FedExSmartPost" <?php echo (get_option('shipping_options_ship_service') == 'FedExSmartPost') ? 'selected' : ''; ?>>FedEx SmartPost (2 to 8 business days)</option>
																<option value="USPSStandardPost" <?php echo (get_option('shipping_options_ship_service') == 'USPSStandardPost') ? 'selected' : ''; ?>>USPS Retail Ground (2 to 9 business days)</option>
																<option value="Other" <?php echo (get_option('shipping_options_ship_service') == 'Other') ? 'selected' : ''; ?>>Economy Shipping (1 to 10 business days)</option>
																<option value="US_UPSSurePost" <?php echo (get_option('shipping_options_ship_service') == 'US_UPSSurePost') ? 'selected' : ''; ?>>UPS Surepost (1 to 6 business days)</option>
																<option value="US_DGMSmartMailGround" <?php echo (get_option('shipping_options_ship_service') == 'US_DGMSmartMailGround') ? 'selected' : ''; ?>>DGM SmartMail Ground (3 to 8 business days)</option>
															</optgroup>
															<optgroup label="Standard services" class="cell-selectOptgrp">
																<option value="USPSFirstClass" <?php echo (get_option('shipping_options_ship_service') == 'USPSFirstClass') ? 'selected' : ''; ?>>USPS First Class Package (2 to 3 business days)</option>
																<option value="FedExHomeDelivery" <?php echo (get_option('shipping_options_ship_service') == 'FedExHomeDelivery') ? 'selected' : ''; ?>>FedEx Ground or FedEx Home Delivery (1 to 5 business days)</option>
																<option value="UPSGround" <?php echo (get_option('shipping_options_ship_service') == 'UPSGround') ? 'selected' : ''; ?>>UPS Ground (1 to 5 business days)</option>
																<option value="ShippingMethodStandard" <?php echo (get_option('shipping_options_ship_service') == 'ShippingMethodStandard') ? 'selected' : ''; ?>>Standard Shipping (1 to 5 business days)</option>
																<option value="US_DGMSmartMailExpedited" <?php echo (get_option('shipping_options_ship_service') == 'US_DGMSmartMailExpedited') ? 'selected' : ''; ?>>DGM SmartMail Expedited (2 to 5 business days)</option>
															</optgroup>
															<optgroup label="Expedited services" class="cell-selectOptgrp">
																<option value="USPSPriority" <?php echo (get_option('shipping_options_ship_service') == 'USPSPriority') ? 'selected' : ''; ?>>USPS Priority Mail (1 to 3 business days)</option>
																<option value="ShippingMethodExpress" <?php echo (get_option('shipping_options_ship_service') == 'ShippingMethodExpress') ? 'selected' : ''; ?>>Expedited Shipping (1 to 3 business days)</option>
																<option value="USPSPriorityFlatRateEnvelope" <?php echo (get_option('shipping_options_ship_service') == 'USPSPriorityFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail Flat Rate Envelope (1 to 3 business days)</option>
																<option value="USPSPriorityMailSmallFlatRateBox" <?php echo (get_option('shipping_options_ship_service') == 'USPSPriorityMailSmallFlatRateBox') ? 'selected' : ''; ?>>USPS Priority Mail Small Flat Rate Box (1 to 3 business days)</option>
																<option value="USPSPriorityFlatRateBox" <?php echo (get_option('shipping_options_ship_service') == 'USPSPriorityFlatRateBox') ? 'selected' : ''; ?>>USPS Priority Mail Medium Flat Rate Box (1 to 3 business days)</option>
																<option value="USPSPriorityMailLargeFlatRateBox" <?php echo (get_option('shipping_options_ship_service') == 'USPSPriorityMailLargeFlatRateBox') ? 'selected' : ''; ?>>USPS Priority Mail Large Flat Rate Box (1 to 3 business days)</option>
																<option value="USPSPriorityMailPaddedFlatRateEnvelope" <?php echo (get_option('shipping_options_ship_service') == 'USPSPriorityMailPaddedFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail Padded Flat Rate Envelope (1 to 3 business days)</option>
																<option value="USPSPriorityMailLegalFlatRateEnvelope" <?php echo (get_option('shipping_options_ship_service') == 'USPSPriorityMailLegalFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail Legal Flat Rate Envelope (1 to 3 business days)</option>
																<option value="USPSExpressMail" <?php echo (get_option('shipping_options_ship_service') == 'USPSExpressMail') ? 'selected' : ''; ?>>USPS Priority Mail Express (1 business day)</option>
																<option value="USPSExpressFlatRateEnvelope" <?php echo (get_option('shipping_options_ship_service') == 'USPSExpressFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail Express Flat Rate Envelope (1 business day)</option>
																<option value="USPSExpressMailLegalFlatRateEnvelope" <?php echo (get_option('shipping_options_ship_service') == 'USPSExpressMailLegalFlatRateEnvelope') ? 'selected' : ''; ?>>USPS Priority Mail Express Legal Flat Rate Envelope (1 business day)</option>
																<option value="UPS3rdDay" <?php echo (get_option('shipping_options_ship_service') == 'UPS3rdDay') ? 'selected' : ''; ?>>UPS 3 Day Select (3 business days)</option>
																<option value="UPS2ndDay" <?php echo (get_option('shipping_options_ship_service') == 'UPS2ndDay') ? 'selected' : ''; ?>>UPS 2nd Day Air (2 business days)</option>
																<option value="FedExExpressSaver" <?php echo (get_option('shipping_options_ship_service') == 'FedExExpressSaver') ? 'selected' : ''; ?>>FedEx Express Saver (1 to 3 business days)</option>
																<option value="FedEx2Day" <?php echo (get_option('shipping_options_ship_service') == 'FedEx2Day') ? 'selected' : ''; ?>>FedEx 2Day (1 to 2 business days)</option>
															</optgroup>
															<optgroup label="One-day services" class="cell-selectOptgrp">
																<option value="ShippingMethodOvernight" <?php echo (get_option('shipping_options_ship_service') == 'ShippingMethodOvernight') ? 'selected' : ''; ?>>One-day Shipping (1 business day)</option>
																<option value="UPSNextDay" <?php echo (get_option('shipping_options_ship_service') == 'UPSNextDay') ? 'selected' : ''; ?>>UPS Next Day Air Saver (1 business day)</option>
																<option value="UPSNextDayAir" <?php echo (get_option('shipping_options_ship_service') == 'UPSNextDayAir') ? 'selected' : ''; ?>>UPS Next Day Air (1 business day)</option>
																<option value="FedExPriorityOvernight" <?php echo (get_option('shipping_options_ship_service') == 'FedExPriorityOvernight') ? 'selected' : ''; ?>>FedEx Priority Overnight (1 business day)</option>
																<option value="FedExStandardOvernight" <?php echo (get_option('shipping_options_ship_service') == 'FedExStandardOvernight') ? 'selected' : ''; ?>>FedEx Standard Overnight (1 business day)</option>
															</optgroup>
															<optgroup label="Economy services from outside US" class="cell-selectOptgrp">
																<option value="EconomyShippingFromOutsideUS" <?php echo (get_option('shipping_options_ship_service') == 'EconomyShippingFromOutsideUS') ? 'selected' : ''; ?>>Economy Shipping from outside US (11 to 23 business days)</option>
																<option value="US_EconomySppedPAK" <?php echo (get_option('shipping_options_ship_service') == 'US_EconomySppedPAK') ? 'selected' : ''; ?>>Economy SpeedPAK from China/Hong Kong/Taiwan (10 to 15 business days)</option>
																<option value="US_EconomyShippingFromGC" <?php echo (get_option('shipping_options_ship_service') == 'US_EconomyShippingFromGC') ? 'selected' : ''; ?>>Economy Shipping from China/Hong Kong/Taiwan to worldwide (11 to 35 business days)</option>
																<option value="US_EconomyShippingFromIN" <?php echo (get_option('shipping_options_ship_service') == 'US_EconomyShippingFromIN') ? 'selected' : ''; ?>>Economy Shipping from India (8 to 13 business days)</option>
																<option value="US_MailServiceFromIndia" <?php echo (get_option('shipping_options_ship_service') == 'US_MailServiceFromIndia') ? 'selected' : ''; ?>>Mail Service from India (14 to 27 business days)</option>
															</optgroup>
															<optgroup label="Standard services from outside US" class="cell-selectOptgrp">
																<option value="StandardShippingFromOutsideUS" <?php echo (get_option('shipping_options_ship_service') == 'StandardShippingFromOutsideUS') ? 'selected' : ''; ?>>Standard Shipping from outside US (5 to 10 business days)</option>
																<option value="US_StandardSppedPAK" <?php echo (get_option('shipping_options_ship_service') == 'US_StandardSppedPAK') ? 'selected' : ''; ?>>Standard SpeedPAK from China/Hong Kong/Taiwan (8 to 12 business days)</option>
																<option value="US_StandardShippingFromGC" <?php echo (get_option('shipping_options_ship_service') == 'US_StandardShippingFromGC') ? 'selected' : ''; ?>>Standard Shipping from China/Hong Kong/Taiwan to worldwide (7 to 19 business days)</option>
																<option value="US_StandardShippingFromIN" <?php echo (get_option('shipping_options_ship_service') == 'US_StandardShippingFromIN') ? 'selected' : ''; ?>>Standard Shipping from India (5 to 12 business days)</option>
															</optgroup>
															<optgroup label="Expedited services from outside US" class="cell-selectOptgrp">
																<option value="ExpeditedShippingFromOutsideUS" <?php echo (get_option('shipping_options_ship_service') == 'ExpeditedShippingFromOutsideUS') ? 'selected' : ''; ?>>Expedited Shipping from outside US (1 to 4 business days)</option>
																<option value="US_FedExIntlEconomy" <?php echo (get_option('shipping_options_ship_service') == 'US_FedExIntlEconomy') ? 'selected' : ''; ?>>FedEx International Economy (2 to 4 business days)</option>
																<option value="US_ExpeditedSppedPAK" <?php echo (get_option('shipping_options_ship_service') == 'US_ExpeditedSppedPAK') ? 'selected' : ''; ?>>Expedited SpeedPAK from China/Hong Kong/Taiwan (5 to 9 business days)</option>
																<option value="US_ExpeditedShippingFromGC" <?php echo (get_option('shipping_options_ship_service') == 'US_ExpeditedShippingFromGC') ? 'selected' : ''; ?>>Expedited Shipping from China/Hong Kong/Taiwan to worldwide (2 to 7 business days)</option>
																<option value="US_ExpeditedShippingFromIN" <?php echo (get_option('shipping_options_ship_service') == 'US_ExpeditedShippingFromIN') ? 'selected' : ''; ?>>Expedited Shipping from India (3 to 9 business days)</option>
															</optgroup>
															<optgroup label="Freight" class="cell-selectOptgrp">
																<option value="FlatRateFreight" <?php echo (get_option('shipping_options_ship_service') == 'FlatRateFreight') ? 'selected' : ''; ?>>Flat Rate Freight </option>
															</optgroup>
														</select>
														<p class="notes">
															<span>Woocommerce product created on eBay With this Shipping Method</span>
														</p>
													</td>

												</tr>
												<tr>
													<td class="label">
														<label for="multiebaystoremageconnect_shipping_options_ship_service_cost">
															<span>Shipping Service Cost</span>
														</label>
													</td>
													<td class="value">
														<input id="multiebaystoremageconnect_shipping_options_ship_service_cost" name="shipping_options_ship_service_cost" value="<?php echo get_option('shipping_options_ship_service_cost') ? get_option('shipping_options_ship_service_cost') : ''; ?>" type="text">
														<p class="notes">
															<span>Shipping Amount that apply on product when sync with ebay</span>
														</p>
													</td>

												</tr>
												<tr>
													<td class="label">
														<label for="multiebaystoremageconnect_shipping_options_ship_service_additional_cost">
															<span>Shipping Service Additional Cost</span>
														</label>
													</td>
													<td class="value">
														<input id="multiebaystoremageconnect_shipping_options_ship_service_additional_cost" name="shipping_options_ship_service_additional_cost" value="<?php echo get_option('shipping_options_ship_service_additional_cost') ? get_option('shipping_options_ship_service_additional_cost') : ''; ?>" type="text">
														<p class="notes">
															<span>Additional Shipping Amount that apply on product when sync with ebay</span>
														</p>
													</td>
												</tr>
												<tr>
													<td class="label">
														<label for="multiebaystoremageconnect_shipping_options_ship_time_min">
															<span>Shipping Time Min</span>
														</label>
													</td>
													<td class="value">
														<input id="multiebaystoremageconnect_shipping_options_ship_time_min" name="shipping_options_ship_time_min" value="<?php echo get_option('shipping_options_ship_time_min') ? get_option('shipping_options_ship_time_min') : ''; ?>" type="text">
														<p class="notes">
															<span>Woocommerce product created on eBay With this Shipping Method</span>
														</p>
													</td>

												</tr>
												<tr>
													<td class="label">
														<label for="multiebaystoremageconnect_shipping_options_ship_time_max">
															<span>Shipping Time Max</span>
														</label>
													</td>
													<td class="value">
														<input id="multiebaystoremageconnect_shipping_options_ship_time_max" name="shipping_options_ship_time_max" value="<?php echo get_option('shipping_options_ship_time_max') ? get_option('shipping_options_ship_time_max') : ''; ?>" type="text">
														<p class="notes">
															<span>Woocommerce product created on eBay With this Shipping Method</span>
														</p>
													</td>

												</tr>
												<tr>
													<td class="label">
														<label for="multiebaystoremageconnect_shipping_options_ship_free">
															<span>Shipping Free</span>
														</label>
													</td>
													<td class="value">
														<select id="multiebaystoremageconnect_shipping_options_ship_free" name="shipping_options_ship_free">
															<option value="1" <?php echo (get_option('shipping_options_ship_free') == '1') ? 'selected' : ''; ?>>Yes</option>
															<option value="0" <?php echo (get_option('shipping_options_ship_free') == '0') ? 'selected' : ''; ?>>No</option>
														</select>
														<p class="notes">
															<span>Woocommerce product created on eBay With this Shipping Method</span>
														</p>
													</td>

												</tr>

											</tbody>

										</table>

										<?php submit_button(); ?>

								</div>

							</div>
                            
                            <div class="toggle-handler">

                                <h2 class="hndle ui-sortable-handle"><span>Slack log url</span></h2>

                            </div>

                            <div class="inside">

                                <div class="main">

                                    <?php settings_fields('ebay-settings-group'); ?>

                                    <?php do_settings_sections('ebay-settings-group'); ?>

                                    <table class="form-table user-access-table">

                                        <tbody>
                                            <tr id="row_multiebaystorewcconnect_general_slack_info_url">
                                                <td class="label">
                                                    <label for="multiebaystorewcconnect_general_settings_mode">
                                                        <span>Info url</span>
                                                    </label>
                                                </td>
                                                <td class="value">
                                                    <input id="multiebaystorewcconnect_general_slack_info_url" name="ebay_slack_info_url" value="<?php echo get_option('ebay_slack_info_url') ? get_option('ebay_slack_info_url') : ''; ?>" type="text">
                                                </td>
                                            </tr>
                                            <tr id="row_multiebaystorewcconnect_general_slack_error_url">
                                                <td class="label">
                                                    <label for="multiebaystorewcconnect_general_settings_mode">
                                                        <span>Error url</span>
                                                    </label>
                                                </td>
                                                <td class="value">
                                                    <input id="multiebaystorewcconnect_general_slack_error_url" name="ebay_slack_error_url" value="<?php echo get_option('ebay_slack_error_url') ? get_option('ebay_slack_error_url') : ''; ?>" type="text">
                                                </td>
                                            </tr>
                                            <tr id="row_multiebaystorewcconnect_general_info_get_url">
                                                <td class="label">
                                                </td>
                                                <td class="value">
                                                    <p class="notes">
                                                        <a href="https://api.slack.com/messaging/webhooks" target="_blank">How to get Application hook url</a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <?php submit_button(); ?>
                                </form>
                            </div>


						</div>

					</div>