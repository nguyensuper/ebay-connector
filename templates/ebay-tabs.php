<?php

if( ! defined ( 'ABSPATH' ) )

  exit;

$action=$_GET['action'];

$page=$_GET['page'];
$aid=isset($_GET['aid'])?$_GET['aid']:'';

if( !empty( $page ) && $page == 'manage-account' && !empty( $action ) ) {

?>
    <div class="wrap ebay-connector">

        <nav class="nav-tab-wrapper">
            <div class="head-wrap">
                <h1 class="wp-heading-inline">Manage Ebay Account</h1><a href="admin.php?page=manage-account" class="page-title-action">Back</a>
            </div>
            <?php
                if ($aid) {

                    echo '<p>Account ID #'.$aid.'</p>';
                }

                            $ebay_data_tabs = apply_filters( 'woocommerce_ebay_data_tabs', array(
                                'account' => array(
                                    'label'  => __( 'Connect Ebay Account', 'wc_ebay' ),
                                    'target' => 'ebay_account',
                                    'class'  => 'ebay_account active',
                                )
                            ) );

                            if(!empty($aid)){

                                $ebay_data_tabs = apply_filters( 'woocommerce_ebay_data_tabs', array(

                                    'account' => array(
                                        'label'  => __( 'Connect Ebay Account', 'wc_ebay' ),
                                        'target' => 'ebay_account',
                                        'class'  => 'ebay_account active',
                                    ),
                                    'import_category'=>array(
                                        'label'  => __( 'Import Categories', 'wc_ebay' ),
                                        'target' => 'import_category',
                                        'class'  => 'import_category',
                                    ),
                                    'category'=>array(
                                        'label'  => __( 'Map Category', 'wc_ebay' ),
                                        'target' => 'map_category',
                                        'class'  => 'map_category',
                                    ),
                                    'product'=>array(
                                        'label'  => __( 'Map Product', 'wc_ebay' ),
                                        'target' => 'map_product',
                                        'class'  => 'map_product',
                                    ),
                                    'order'=>array(
                                        'label'  => __( 'Map Order', 'wc_ebay' ),
                                        'target' => 'map_order',
                                        'class'  => 'map_order',
                                    ),
                                    'export'=>array(
                                        'label'  => __( 'Export to Ebay', 'wc_ebay' ),
                                        'target' => 'export_to_ebay',
                                        'class'  => 'export_to_ebay',
                                    ),
                                    'feedback'=>array(
                                        'label'  => __( 'Feedback', 'wc_ebay' ),
                                        'target' => 'feed_back_to_ebay',
                                        'class'  => 'feed_back_to_ebay',
                                    )
                                    )
                                );


                            }

                            $current_tab = empty( $_GET['tab'] ) ? 'ebay_account' : sanitize_title( $_GET['tab'] );

                            foreach ( $ebay_data_tabs as $key => $tab ) {

                                ?>
                                <a href="<?php echo admin_url( 'admin.php?page=manage-account&action=account&aid='.$aid.'&tab=' .$tab['target']);  ?>" class="nav-tab <?php  echo ( $current_tab == $tab['target'] ?'nav-tab-active' : '' ) ; ?>"><span><?php echo  esc_html( $tab['label'] ); ?></span></a>

                                <?php
                            }

                        ?>
                    </ul>

                    <div class="wc-product-import-section">

                        <div class="wc-ebay-info-bar">

                            <img class="wc-loader-image" src="<?php echo EBAY_API. '/assets/images/loader.gif'; ?>">

                        </div>

                        <div class="wc-product-import-section-body">

                        </div>

                        <div class="wc-ebay-product-time-interval main-syncebay">
                          <table class="form-table">
                            <thead>

                              <tr>
                                <th>Fields</th>
                                <th>Options</th>
                              </tr>

                            </thead>

                            <tbody>
                              <tr valign="top">

                                  <th scope="row" class="titledesc">
                                    <label for="wc-ebay-start-date-product"><?php echo __('Start Time From', 'wc_ebay'); ?></label>
                                  </th>

                                  <td class="forminp">
                                    <input type="text" name="wc-ebay-start-date" id="wc-ebay-start-date-product" class="wc-ebay-start-datepicker-product" />
                                    <p class="description"><?php echo __('Specifies the earliest (oldest) date to use in a date range filter based on item start time. Each of the time ranges must be a value less than 120 days.', 'wc_ebay') ?></p>
                                  </td>
                              </tr>

                              <tr valign="top">

                                  <th scope="row" class="titledesc">
                                    <label for="wc-ebay-end-date-product"><?php echo __('Start Time To', 'wc_ebay'); ?></label>
                                  </th>

                                  <td class="forminp">
                                    <input type="text" name="wc-ebay-end-date" id="wc-ebay-end-date-product" class="wc-ebay-end-datepicker-product" />
                                    <p class="description"><?php echo __('Specifies the latest (most recent) date to use in a date range filter based on item start time.', 'wc_ebay') ?></p>
                                  </td>
                              </tr>
                            </tbody>

                          </table>

                          <button id="import_pro_from_ebay" title="Import Product From eBay" class="action-split button button-primary" data-ui-id="import-pro-from-ebay"><?php echo __('Start', 'wc_ebay'); ?></button>
                        </div>

                        <div class="wc-ebay-order-time-interval main-syncebay">
                          <table class="form-table">
                            <thead>

                              <tr>
                                <th>Fields</th>
                                <th>Options</th>
                              </tr>

                            </thead>

                            <tbody>
                              <tr valign="top">

                                  <th scope="row" class="titledesc">
                                    <label for="wc-ebay-start-date-order"><?php echo __('Start Time From', 'wc_ebay'); ?></label>
                                  </th>

                                  <td class="forminp">
                                    <input type="text" name="wc-ebay-start-date" id="wc-ebay-start-date-order" class="wc-ebay-start-datepicker-order" />
                                    <p class="description"><?php echo __('Specifies the earliest (oldest) date to use in a date range filter based on item start time. Each of the time ranges must be a value less than 120 days.', 'wc_ebay') ?></p>
                                  </td>
                              </tr>

                              <tr valign="top">

                                  <th scope="row" class="titledesc">
                                    <label for="wc-ebay-end-date-order"><?php echo __('Start Time To', 'wc_ebay'); ?></label>
                                  </th>

                                  <td class="forminp">
                                    <input type="text" name="wc-ebay-end-date" id="wc-ebay-end-date-order" class="wc-ebay-end-datepicker-order" />
                                    <p class="description"><?php echo __('Specifies the latest (most recent) date to use in a date range filter based on item start time.', 'wc_ebay') ?></p>
                                  </td>
                              </tr>
                            </tbody>

                          </table>

                          <button id="import_oro_from_ebay" title="Import Product From eBay" class="action-split button button-primary" data-ui-id="import-pro-from-ebay"><?php echo __('Start', 'wc_ebay'); ?></button>
                        </div>

                    </div>

                    <div class="postbox ebay-tab-content">
                        <?php do_action( 'wk_ebay_tab_view_' . $current_tab,$aid ); ?>
                    </div>
        </nav>

    </div>


<?php
}
