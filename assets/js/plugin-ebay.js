$ebay=jQuery.noConflict();

(function($ebay){

	$ebay(document).ready(function(){

		$ebay("#sync_from_ebay").on("click",".action-toggle", function(){
			$ebay(this).parent("#sync_from_ebay").toggleClass("active");
		});

		$ebay("select[name='ebay_default_category']").select2();

		$ebay("select[id='wc-store-cat']").select2();

		$ebay("select[id='wc-ebay-cat']").select2();

		if( $ebay('#ebay_condition_id_select').length ) {
			$ebay('#ebay_condition_id_select').select2();
		}

		$ebay('#multiebaystorewcconnect_export_product_start_cron_from_date').datepicker({
			dateFormat: 'yy-mm-dd',
			minDate: 0,
			// maxDate: 0,
			onSelect: function(datetext){
				var d = new Date(); // for now
				var h = d.getHours();
				h = (h < 10) ? ("0" + h) : h;

				var m = d.getMinutes();
				m = (m < 10) ? ("0" + m) : m;

				var s = d.getSeconds();
				s = (s < 10) ? ("0" + s) : s;

				datetext = datetext + " " + h + ":" + m + ":" + s;
				$ebay('#multiebaystorewcconnect_export_product_start_cron_from_date').val(datetext);
			}
		});

		$ebay('#multiebaystorewcconnect_export_product_start_cron_to_date').datepicker({
			dateFormat: 'yy-mm-dd',
			minDate: 0,
			// maxDate: 0,
			onSelect: function(datetext){
				var d = new Date(); // for now
				var h = d.getHours();
				h = (h < 10) ? ("0" + h) : h;

				var m = d.getMinutes();
				m = (m < 10) ? ("0" + m) : m;

				var s = d.getSeconds();
				s = (s < 10) ? ("0" + s) : s;

				datetext = datetext + " " + h + ":" + m + ":" + s;
				$ebay('#multiebaystorewcconnect_export_product_start_cron_to_date').val(datetext);
			}
		});

		if ($ebay('input#wc-ebay-start-date-product').length) {
			$ebay('input#wc-ebay-start-date-product').datepicker({
				dateFormat: 'yy-mm-dd',
				maxDate: 0,
				onSelect: function(datetext){
		      var d = new Date(); // for now
		      var h = d.getHours();
		  		h = (h < 10) ? ("0" + h) : h;

		  		var m = d.getMinutes();
		      m = (m < 10) ? ("0" + m) : m;

		      var s = d.getSeconds();
		      s = (s < 10) ? ("0" + s) : s;

		  		datetext = datetext + "T" + h + ":" + m + ":" + s;
		      $ebay('input#wc-ebay-start-date-product').val(datetext);
		    }
			});
			$ebay('input#wc-ebay-end-date-product').datepicker({
				dateFormat : 'yy-mm-dd',
				maxDate: 0,
				onSelect: function(datetext){
		      var d = new Date(); // for now
		      var h = d.getHours();
		  		h = (h < 10) ? ("0" + h) : h;

		  		var m = d.getMinutes();
		      m = (m < 10) ? ("0" + m) : m;

		      var s = d.getSeconds();
		      s = (s < 10) ? ("0" + s) : s;

		  		datetext = datetext + "T" + h + ":" + m + ":" + s
		      $ebay('input#wc-ebay-end-date-product').val(datetext);
		    }
			});
		}

		if ($ebay('input#wc-ebay-start-date-order').length) {
			$ebay('input#wc-ebay-start-date-order').datepicker({
				dateFormat: 'yy-mm-dd',
				maxDate: 0,
				onSelect: function(datetext){
		      var d = new Date(); // for now
		      var h = d.getHours();
		  		h = (h < 10) ? ("0" + h) : h;

		  		var m = d.getMinutes();
		      m = (m < 10) ? ("0" + m) : m;

		      var s = d.getSeconds();
		      s = (s < 10) ? ("0" + s) : s;

		  		datetext = datetext + "T" + h + ":" + m + ":" + s;
		      $ebay('input#wc-ebay-start-date-order').val(datetext);
		    }
			});
			$ebay('input#wc-ebay-end-date-order').datepicker({
				dateFormat : 'yy-mm-dd',
				maxDate: 0,
				onSelect: function(datetext){
		      var d = new Date(); // for now
		      var h = d.getHours();
		  		h = (h < 10) ? ("0" + h) : h;

		  		var m = d.getMinutes();
		      m = (m < 10) ? ("0" + m) : m;

		      var s = d.getSeconds();
		      s = (s < 10) ? ("0" + s) : s;

		  		datetext = datetext + "T" + h + ":" + m + ":" + s
		      $ebay('input#wc-ebay-end-date-order').val(datetext);
		    }
			});
		}

		$ebay(".main-syncebay").on("click",".action-split", function(){

			$ebay(".wc-product-import-section-body").empty();

			var checker = $ebay(this).attr("id");

			var ebay_accid = $ebay('input[name="ebay_accid"]').val();

			if(ebay_accid){
				if (checker == 'open_import_pro_from_ebay') {
					$ebay(".wc-product-import-section .wc-ebay-product-time-interval").show();

					$ebay(".wc-product-import-section").show();

				} else if( checker == 'import_pro_from_ebay') {

					var startDate = $ebay('#wc-ebay-start-date-product').val();

					var endDate = $ebay('#wc-ebay-end-date-product').val();

					if (startDate == '' || endDate == '') {
						alert('Please select start and end date.');
						return false;
					}

					var diffDate =  new Date(endDate) - new Date(startDate);

					var daysRange = Math.floor(diffDate / (1000 * 60 * 60 * 24));

					if (daysRange < 1 || daysRange > 120) {
						alert('Time range must be a value less than 120 days and greater than 0.');
						return false;
					}

					var pageNumber = 1;
					function importProductFromEbay(pageNumber) {
							$ebay.ajax({
								type: 'POST',
								url: api_script.api_admin_ajax,
								data: {
									"action": "ebay_import_products_from_api",
									"nonce": api_script.api_nonce,
									"ebay_accid": ebay_accid,
									"start_date": startDate,
									"end_date": endDate,
									"page_number": pageNumber
								},
								beforeSend: function() {
									if (pageNumber === 1) {
										$ebay(".wc-product-import-section .wc-ebay-product-time-interval").hide();
										$ebay(".wc-product-import-section .wc-ebay-info-bar").show();
										$ebay(".wc-product-import-section").show();
										$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Starting Execution...</p></div>');
										$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>Please don\'t close or refresh the window while importing product(s).</p></div>');
									}
								},
								success: function(response) {

									if( response ) {

										if(response != undefined ) {

											if (response.length > 0) {

												var index = 0;

												var batch = 1;

												var batchSize = 5;

												if (batchSize > response.length) {
													batchSize = response.length;
												}
												console.log(batchSize);


												$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>Fetching process completed for page <strong>' + pageNumber + '</strong>.!</p></div>');
												$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p> Saving products raw data to database initialized.!</p></div>');
												$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Batch Process Started for <strong>' + response.length + '</strong> products. Batch size will be <strong>' + batchSize + '</strong>.</p></div>');

												var responseLength = response.length;

												function recursive_ajax(index) {

														var productData = response.splice( index, batchSize );

														var raw_data = JSON.stringify(productData)

														$ebay.ajax({
															type: 'POST',
															url: api_script.api_admin_ajax,
															data: {'action': 'ebay_import_products_raw_data','nonce':api_script.api_nonce,'raw_data':raw_data },
															beforeSend: function(){
															},
															success: function(last_response) {
																console.log(last_response)
																if (productData.length > 0) {
																		if(last_response) {
																			$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-success"><p>'+last_response+' Product imported from ebay successfully and saved as raw data.!</p></div>');
																		} else{

																			$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>No New Raw products to be imported in batch <strong>' + batch + '</strong>.</p></div>');
																		}
																		batch++;
																		recursive_ajax(index);
																} else {
																	if (responseLength >= 100) {
																		pageNumber++;
																		importProductFromEbay(pageNumber);
																	} else {
																		$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Process Completed.!</p></div>');
																		$ebay(".wc-product-import-section .wc-ebay-info-bar").hide();
																		setTimeout(function(){
																			location.reload();
																		}, 1500);
																	}
																}
															}
														});
												}
												recursive_ajax(index);
											} else {
												$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>No data found.</p></div>');
												$ebay(".wc-product-import-section .wc-ebay-info-bar").hide();
												setTimeout(function(){
													location.reload();
												}, 1500);
											}
										}

									}

								}
							});
					}
					importProductFromEbay(pageNumber);

				} else if( checker == 'profiler_for_create_pro' ) {

					$ebay.ajax({
						type: 'POST',
						url: api_script.api_admin_ajax,
						data: {"action": "get_ebay_import_products_api_data","nonce":api_script.api_nonce },
						beforeSend: function(){
							$ebay(".wc-product-import-section .wc-ebay-info-bar").show();
							$ebay(".wc-product-import-section").show();
							$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Fetching Raw data from database.!</p></div>');
						},
						success: function(response) {

							if( response.length ) {
								var index = 0;
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>Fetching process Completed.!</p></div>');
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p> Importing Products to database initialized.!</p></div>');
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Total '+response.length+' product(s) to create. Process will proceed in batch of 1.</p></div>');
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Batch Process Started</p></div>');
								function recursive_ajax(index) {

									var product = response.splice( index, 1 );

									var raw_data = JSON.stringify(product);

									$ebay.ajax({
										type: 'POST',
										url: api_script.api_admin_ajax,
										data: {'action': 'ebay_import_products','nonce':api_script.api_nonce,'raw_data':raw_data },
										beforeSend: function(){
										},
										success: function(last_response) {
											console.log(last_response)
											if(Object.keys(last_response).length != 0) {

												if( last_response.skip != undefined && last_response.new != undefined ) {

													if( product.length > 0) {
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>'+last_response.skip+' Products Skipped due to duplicate sku or may be already synced.!</p></div>');
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>'+last_response.new+' Products imported Successfully.!</p></div>');
														recursive_ajax( index );
													} else {

														$ebay(".wc-product-import-section .wc-ebay-info-bar").hide();
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Process Completed.!</p></div>');
														setTimeout(function(){
															location.reload();
														}, 1500);
													}


												}


											}else{

												alert('There is some error While importing products to database!');

											}

										}
									});
								}

								recursive_ajax(index);

							}else{

								alert('There are no new products to import into database!');

							}

						}
					});


				} else if (checker == 'open_import_oro_from_ebay') {

					$ebay(".wc-product-import-section .wc-ebay-order-time-interval").show();

					$ebay(".wc-product-import-section").show();

				} else if( checker == 'import_oro_from_ebay' ) {

					var startDate = $ebay('#wc-ebay-start-date-order').val();
					console.log(startDate)
					var endDate = $ebay('#wc-ebay-end-date-order').val();
					console.log(endDate)
					if (startDate == '' || endDate == '') {
						alert('Please select start and end date.');
						return false;
					}

					var diffDate =  new Date(endDate) - new Date(startDate);

					var daysRange = Math.floor(diffDate / (1000 * 60 * 60 * 24));

					if (daysRange < 1 || daysRange > 120) {
						alert('Time range must be a value less than 120 days and greater than 0.');
						return false;
					}

					$ebay.ajax({
						type: 'POST',
						url: api_script.api_admin_ajax,
						data: {
							"action": "ebay_import_orders_from_api",
							"nonce":api_script.api_nonce,
							"ebay_accid":ebay_accid,
							"start_date": startDate,
							"end_date": endDate
						},
						beforeSend: function(){
							$ebay(".wc-product-import-section .wc-ebay-info-bar").show();
							$ebay(".wc-product-import-section").show();
							$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Fetching data from Ebay.!</p></div>');
						},
						success: function(response) {

							if( response.error_msg != undefined ) {

								if(  response.error_msg == false ) {

									alert( response.data );
									location.reload();

								} else {

									alert('There is some error in importing Orders!');
									$ebay(".wc-product-import-section .wc-ebay-info-bar").hide();
									$ebay(".wc-product-import-section").hide();

								}

							}else{

								alert('There is some error in importing Orders!');

							}


						}

					});

				} else if( checker == 'profiler_for_create_oro' ) {

					$ebay.ajax({
						type: 'POST',
						url: api_script.api_admin_ajax,
						data: {"action": "get_ebay_import_orders_api_data","nonce":api_script.api_nonce},
						beforeSend: function() {
							$ebay(".wc-product-import-section .wc-ebay-info-bar").show();
							$ebay(".wc-product-import-section").show();
							$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Fetching Raw data from database.!</p></div>');
						},
						success: function( response ) {

							if( response.length ) {

								var index = 0;

								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>Fetching process Completed.!</p></div>');
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p> Importing Orders to database initialized.!</p></div>');
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Batch Process Started</p></div>');

								function recursive_ajax(index) {

									var orders = response.splice( index, 10 );

									var raw_data = JSON.stringify(orders);

									$ebay.ajax({
										type: 'POST',
										url: api_script.api_admin_ajax,
										data: {'action': 'ebay_import_orders','nonce':api_script.api_nonce,'raw_data':raw_data },
										beforeSend: function(){

										},
										success: function(last_response) {

											if(Object.keys(last_response).length != 0) {

												if( last_response.skip != undefined && last_response.new != undefined ) {

													if( orders.length > 0) {

														if( last_response.errmsg != undefined ) {

															$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>'+last_response.errmsg+'</p></div>');

														}
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>'+last_response.skip+' Orders Skipped due to duplicate sku or may be already synced.!</p></div>');
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>'+last_response.new+' Orders imported Successfully.!</p></div>');
														recursive_ajax( index );
													} else {
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Process Completed.!</p></div>');
														setTimeout(function(){
															location.reload();
														}, 1500);
													}


												}


											}else{

												alert('There is some error While importing Orders to database!');

											}

										}
									});
								}

								recursive_ajax(index);

							}else{

								alert('There are no new products to import into database!');

							}

						}

					});


				} else if( checker == 'import_cat_from_ebay') {

					$ebay.ajax({
						type: 'POST',
						url: api_script.api_admin_ajax,
						data: {"action": "ebay_import_cat_from_api","nonce":api_script.api_nonce,"ebay_accid":ebay_accid},
						beforeSend: function(){
							$ebay(".wc-product-import-section .wc-ebay-info-bar").show();
							$ebay(".wc-product-import-section").show();
							$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Starting Execution...</p></div>');
							$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>Please don\'t close or refresh the window while importing category(s).</p></div>');
						},
						success: function(response) {
							if( response ) {

								if(response.success != undefined ) {

									var index = 0;

									$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>Fetching process Completed.!</p></div>');
									$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p> Saving categories raw data to database initialized.!</p></div>');
									$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Batch Process Started.</p></div>');

									function recursive_ajax(index) {

											var catRawdata = response.success

											var catData = catRawdata.splice( index, 1000 )

											var raw_data = JSON.stringify(catData);

											$ebay.ajax({
												type: 'POST',
												url: api_script.api_admin_ajax,
												data: {'action': 'ebay_import_categories_raw_data','nonce':api_script.api_nonce,'raw_data':raw_data },
												beforeSend: function(){
												},
												success: function(last_response) {
													if (catData.length > 0) {
															if(last_response) {
																$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-success"><p>'+last_response+' Category imported from ebay successfully and saved as raw data.!</p></div>');
																recursive_ajax(index);
															} else{

																$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>No New Raw Categories to be imported.!</p></div>');

															}
													} else {
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Process Completed.!</p></div>');
														setTimeout(function(){
															location.reload();
														}, 1500);
													}
													$ebay(".wc-product-import-section .wc-ebay-info-bar").hide();
												}
											});
									}
									recursive_ajax(index);

								} else if( response.msg != undefined ) {

									alert( response.msg );
									location.reload();

								}

							}


						}
					});

				}  else if( checker == 'profiler_for_create_cat') {

					$ebay.ajax({
						type: 'POST',
						url: api_script.api_admin_ajax,
						data: {"action": "get_ebay_import_category_api_data","nonce":api_script.api_nonce },
						beforeSend: function(){
							$ebay(".wc-product-import-section .wc-ebay-info-bar").show();
							$ebay(".wc-product-import-section").show();
							$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Fetching Raw data from database.!</p></div>');
						},
						success: function(response) {
							if( response.length ) {
								var index = 0;
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>Featching process Completed.!</p></div>');
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p> Importing Categories to database initialized.!</p></div>');
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Batch Process Started</p></div>');

								function recursive_ajax(index) {

									var categories = response.splice( index, 60 );

									var raw_data = JSON.stringify(categories);

									$ebay.ajax({
										type: 'POST',
										url: api_script.api_admin_ajax,
										data: {'action': 'ebay_import_categories_finally','nonce':api_script.api_nonce,'raw_data':raw_data },
										beforeSend: function(){
										},
										success: function(last_response) {

											if(Object.keys(last_response).length != 0) {

												if( last_response.skip != undefined && last_response.new != undefined ) {

													if( categories.length > 0) {
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-error"><p>'+last_response.skip+' Category(s) Skipped as may be already synced.!</p></div>');
														$ebay(".wc-product-import-section").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>'+last_response.new+' Category(s) imported Successfully.!</p></div>');
														recursive_ajax( index );
													}

													$ebay(".wc-product-import-section .wc-ebay-info-bar").hide();

												}


											} else{

												alert('There is some error While importing Category(s) to database!');

											}

										}
									});
								}

								recursive_ajax(index);

							}else{

								alert('There are no new products to import into database!');

							}

						}
					});

				}

			} else{

				alert("Syncing products from ebay needs ebay account id");

			}


		});
		$ebay(".main-syncebay-mes").on("click",".action-split", function(){
			$ebay(".wc-product-import-section-body").empty();

			var checker = $ebay(this).attr("id");
			
			var ebay_accid = $ebay('input[name="ebay_accid"]').val();
			
			if( checker == 'import_message_from_ebay' ) {
				$ebay.ajax({
					type: 'POST',
					url: api_script.api_admin_ajax,
					data: {"action": "import_message_from_ebay","ebay_accid": ebay_accid,"nonce":api_script.api_nonce},
					beforeSend: function() {
						$ebay(".wc-product-import-section .wc-ebay-info-bar").show();
						$ebay(".wc-product-import-section").show();
						$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-info"><p>Fetching data from ebay.!</p></div>');
						console.log('beforeSend');
					},
					success: function( response ) {
						var count = 0;
						if (typeof response.count !== 'undefined') {
							count = response.count;
						}
						$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-success"><p>Fetching process Completed.!</p></div>');
						$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-success"><p>Have '+ count +' new message(s).!</p></div>');
						console.log(response.error);
						if (response.error.length) {
							$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-warning"><p>=============== ****** Errors ***** ===============</p></div>');
							response.error.forEach(function(val) {
								$ebay(".wc-product-import-section-body").append('<div class="wc-ebay-notice wc-ebay-notice-'+ val.Ack +'"><p>'+ val.msg +'</p></div>');
							});
						}
						$ebay(".wc-product-import-section .wc-ebay-info-bar").hide();
					}
				});
			}
		});
		$ebay('#messenger-reply').on('submit', function(e) {
			e.preventDefault();
			var id = $ebay('#messenger-reply input[name="id"]').val();
			var content = $ebay('#messenger-reply textarea[name="content"]').val();
			var ebay_accid = $ebay('input[name="ebay_accid"]').val();
			$ebay.ajax({
				type: 'POST',
				url: api_script.api_admin_ajax,
				data: {"action": "reply_message_ebay","id": id,"content": content,"ebay_accid":ebay_accid,"nonce":api_script.api_nonce},
				beforeSend: function() {
					$ebay('#messenger-popup textarea').attr('disabled', 'disabled');
					$ebay('#messenger-popup button').attr('disabled', 'disabled');
					$ebay('#messenger-popup .wc-ebay-info-bar').show();
				},
				success: function( response ) {
					if (response.Ack == 'Success' || response.Ack == 'Warning') {
						$ebay('#messenger-popup').hide();
						alert('Success');
					} else {
						alert('Error!' + response.msg);
					}
					$ebay('#messenger-popup textarea')[0].removeAttribute('disabled');
					$ebay('#messenger-popup button')[0].removeAttribute('disabled');
					$ebay('#messenger-popup .wc-ebay-info-bar').hide();
				}

			});
		});
		$ebay('#messenger-popup').on('click', '.close', function() {
			$ebay('#messenger-popup input[name="id"]').val(0);

			$ebay('#messenger-popup .content').html('');
			$ebay('#messenger-popup').hide();
		});
		$ebay("table").on("click", ".view-messgener", function(e){
			e.preventDefault();
			var id = $ebay(this).attr("id");
			$ebay.ajax({
				type: 'POST',
				url: api_script.api_admin_ajax,
				data: {"action": "get_message_ebay","id": id,"nonce":api_script.api_nonce},
				beforeSend: function() {
					$ebay('#messenger-popup .wc-ebay-info-bar').show();
					$ebay('#messenger-popup').show();
				},
				success: function( response ) {
					$ebay('#messenger-popup input[name="id"]').val(response.id);
					$ebay('#messenger-popup .content').html(response.ebay_messenger_content);
					if (typeof response.ebay_external_message_id == 'undefined' || response.ebay_external_message_id <= 0) {
						$ebay('#messenger-popup textarea').attr('disabled', 'disabled');
						$ebay('#messenger-popup button').attr('disabled', 'disabled');
					} else {
						$ebay('#messenger-popup textarea')[0].removeAttribute('disabled');
						$ebay('#messenger-popup button')[0].removeAttribute('disabled');
					}
					$ebay('#messenger-popup').show();
					$ebay('#messenger-popup .wc-ebay-info-bar').hide();
				}

			});
		});


	});

	var process = {

		process_complete: function( elm ) {

			elm.find(".wc-ebay-info-bar").hide();
			elm.find(".wc-product-import-section-body").empty();
		}
	};
})(jQuery);
