=== woocommerce ebay connector ===
Contributors: webkul
Tags: form, database, db, data, value, shortcode, submit
Requires at least: 3.7
Tested up to: 4.7.3
Stable tag: 4.0
License: GNU/GPL for more info see license.txt included with plugin
License URI: http://www.gnu.org/licenseses/gpl-2.0.html

Wordpress WooCommerce ebay connector is a e-coomerce based plugin via which admin can import information related to product, categories, and orders from eBay to WooCommerce store.
The plugin allows the admin to import eBay store category directly to the WooCommerce store to keep both the stores more uniform.
And, can also export the product from the WooCommerce store to eBay store.
This will list the items from WooCommerce store directly onto your eBay account.

== Description ==
	Features
		Import simple and variable eBay products in WooCommerce store.
		Import eBay categories in WooCommerce store.
		Import eBay orders in WooCommerce store.
		Export simple products from WooCommerce store to eBay.
		Can map eBay categories with WooCommerce store category.
		Admin can update imported product details.
		Admin can configure the return policy details for exported products.
		The product information like SKU, name, description, stock, images, and categories will get export and import along with the product.
		Products synchronization with eBay condition and specification.
		The admin can configure multiple eBay accounts.
		Sandbox mode is available for testing and development.
		Configure default product payment and shipping details.


== Installation ==

1. Upload the `ebay-connector` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Configure the plugin using the 'woocommerce Multi Account ebay' menu


== Installation Crontab ==
0. Cài Curl nếu chưa có (kiểm tra đã cài đặt: curl --version):
	- Với Debian linux: sudo apt-get install curl
	- Với CentOS linux: sudo yum install curl
1. sudo crontab -e
2. Nhập cron
	2.1 Bấm phím i để vào chế độ sửa
	2.2 Nhập vào dòng mới: */30 * * * * curl {domain}/wp-content/plugins/ebay-connector/includes/cron.php
	2.3 Thay thế {domain} bằng tên domain của website
3. Lưu cron
	3.1 Bấm phím ESC
	3.2 Nhập :wq sau đó enter
4. Khởi động lại crontab
	- Với Debian Linux: sudo /etc/init.d/cron restart
	- Với CentOS Linux: sudo /etc/init.d/crond restart
	
	

Initial release


