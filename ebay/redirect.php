<?php
if(empty($_GET['code'])) {
	die('declined');
}
header("Location: " . $_SERVER['REQUEST_SCHEME'] . "://" .$_SERVER['HTTP_HOST'] . "/wp-admin/admin.php?page=manage-account&action=". (($_GET['state'] ?? '') ? 'account' : 'add')  . "&aid=" . ($_GET['state'] ?? '') . "&code=" . urlencode($_GET['code'] ?? ''));
die();
// wordpress.local/wp-content/plugins/ebay-connector/ebay/redirect.php?state=1&code=abc
?>