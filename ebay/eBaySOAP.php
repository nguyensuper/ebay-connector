<?php
namespace Ebay;

class eBaySOAP {
    
    private $_headers = null;
    private $_session = null;
    
    public function __construct(eBaySession $session)
    {
        $this->_session = $session;
    }
    
    private function renderXmlBody($args, $callname)
    {
        $optional = [
            'rootElementName' => $callname . 'Request',
            '_attributes' => ['xmlns' => 'urn:ebay:apis:eBLBaseComponents']
        ];
        $arrToXml = new ArrayToXml($args, $optional, false, 'utf-8');
        return $arrToXml->toXml();
    }
    
    public function getRefreshToken($code)
    {
        $domain = 'https://api.ebay.com';
        if (get_option('ebaygeneral_settings_mode') == 1) {
            $domain = 'https://api.sandbox.ebay.com';
        }
        $header = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . base64_encode(get_option('ebay_app_id') . ':' . get_option('ebay_cert_id'))
        ];
        $params = [
            'grant_type'   => 'authorization_code',
            'redirect_uri' => get_option('ebay_ru_name'),
            'code'         => trim($code)
        ];
        $curl = new Curl();
       
        $response = $curl->to($domain . '/identity/v1/oauth2/token')
            ->withHeaders($header)
            ->withData($params)
            ->post();
        $response = json_decode($response, true);
        return $response;
    }
    
    public function getToken()
    {
        $refreshToken = $this->_session->token;
        // for test
        // $refreshToken = 'v^1.1#i^1#r^1#p^3#f^0#I^3#t^Ul4xMF82OjlEQUE1RUI3ODdBQkFCMEUyNDQ0RDk5NEUxRDUxMTREXzNfMSNFXjI2MA==';
        $domain = 'https://api.ebay.com';
        if (get_option('ebaygeneral_settings_mode') == 1) {
            $domain = 'https://api.sandbox.ebay.com';
        }
        $header = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . base64_encode(get_option('ebay_app_id') . ':' . get_option('ebay_cert_id'))
        ];
        $params = [
            'grant_type'    => 'refresh_token',
            'scope'         => 'https://api.ebay.com/oauth/api_scope',
            'refresh_token' => $refreshToken
        ];
        $curl = new Curl();
        
        $response = $curl->to($domain . '/identity/v1/oauth2/token')
            ->withHeaders($header)
            ->withData($params)
            ->post();
        $response = json_decode($response, true);
        return $response['access_token'] ?? '';
    }
    
    private function buildHeader($callname)
    {
        $token = $this->getToken();
        return [
            'Content-Type: application/xml',
            'X-EBAY-API-CALL-NAME: ' . $callname,
            'X-EBAY-API-SITEID: ' . $this->_session->site,
            'X-EBAY-API-COMPATIBILITY-LEVEL: 891',
            'X-EBAY-API-DETAIL-LEVEL: 0',
            'X-EBAY-API-IAF-TOKEN: ' . $token,
        ];
    }
    
    private function parseXml($xmlStr)
    {
        $xml = simplexml_load_string($xmlStr);
        $json = json_encode($xml);
        return json_decode($json, false);
    }
    
    public function __call($function, $args)
    {
        $callname = $function;
        $siteid = $this->_session->site;
        // $version = $args[0]['Version'];
        $appid = $this->_session->app;
        $routing = 'default'; // XXX: hardcoded
        
        $body = $this->renderXmlBody($args[0], $callname);
        $curl = new Curl();
        return $this->parseXml($curl->to($this->_session->location)
            ->withOption('SSL_VERIFYPEER', false)
            ->withOption('SSL_VERIFYHOST', false)
            ->withHeaders($this->buildHeader($callname))
            ->withData($body)
            ->post());
    }
    
}

// // Main class for communication with eBay Web services via SOAP
// class eBaySOAP extends \SoapClient
// {
//     private $_headers = null;
//     private $_session = null;

//     public function __construct(eBaySession $session)
//     { 
//         $this->_session = $session;
//         $this->__setHeaders();
//         parent::__construct($session->wsdl, $session->options);
//     }

//     private function __setHeaders()
//     {
//         $eBayAuth = new eBayAuth($this->_session);
//         $headerBody = new \SoapVar($eBayAuth, SOAP_ENC_OBJECT);
//         $headers = [new \SOAPHeader('urn:ebay:apis:eBLBaseComponents', 'RequesterCredentials', $headerBody)];
//         $this->_headers = $headers;
//     }

//     public function __call($function, $args)
//     {
//         $callname = $function;
//         $siteid = $this->_session->site;
//         $version = $args[0]['Version'];
//         $appid = $this->_session->app;
//         $routing = 'default'; // XXX: hardcoded

//         $queryString = http_build_query(
//             [
//                 'callname' => $callname,
//                 'siteid' => $siteid,
//                 'version' => $version,
//                 'appid' => $appid,
//                 'Routing' => $routing
//             ]
//         );
//         $location = "{$this->_session->location}?{$queryString}";

//         return $this->__soapCall($function, $args, ['location' => $location], $this->_headers);
//     }
// }
