<?php
/**
 * Plugin Name: Woocommerce Ebay Connector
 * Plugin URI: https://store.webkul.com/woocommerce-ebay-connector.html
 * Description: Woocommerce ebay connector will import or export products, orders or categories.
 * Version: 1.0
 * Author: Webkul
 * Author URI: http://webkul.com
 * Domain Path: plugins/ebay-connector
 * License: GNU/GPL for more info see license.txt included with plugin
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wc_ebay.
**/

/*---------------------------------------------------------------------------------------------*/

if (!defined('ABSPATH')) {
    // Exit if accessed directly.
    exit;
}

add_action('admin_init', 'ebay_check_woocommerce_is_installed');

/*
    Check if woocommerce plugin is already installed.
*/
function ebay_check_woocommerce_is_installed()
{
    if (!function_exists('WC')) {
        // echo 'please install or activate woocommerce plugin to use this plugin';
        add_action('admin_notices', 'wkebay_woocommerce_missing_notice');
        // exit;
    }
}

/*
    Function to show message if woocommerce is not installed.
*/
function wkebay_woocommerce_missing_notice()
{
    echo '<div class="error"><p>'.sprintf(__('WooCommerce Ebay Connector depends on the last version of %s or later to work!', 'marketplace'), '<a href="http://www.woothemes.com/woocommerce/" target="_blank">'.__('WooCommerce 3.0', 'wc_ebay').'</a>').'</p></div>';
}

if (!class_exists('eBay_Connector')) :

    final class eBay_Connector
    {
        private static $_instance = null;

        private static $access_token;

        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }

        private function includes()
        {
            // Include required files.
            require_once 'ebay/eBayAmountType.php';

            require_once 'ebay/eBayAuth.php';

            require_once 'ebay/eBayCredentials.php';

            require_once 'ebay/eBayFeesType.php';
            require_once 'ebay/eBayFeeType.php';
            require_once 'ebay/eBayGetSearchResultsResponseType.php';
            require_once 'ebay/eBaySearchResultItemArrayType.php';
            require_once 'ebay/eBaySearchResultItemType.php';
            
            require_once 'ebay/ArrayToXml.php';
            require_once 'ebay/Curl.php';
            require_once 'ebay/eBaySOAP.php';

            require_once 'ebay/eBaySession.php';

            require_once 'ebay/eBayUtils.php';

            require_once 'includes/class-ebay-install.php';

            require_once 'includes/admin/index.php';

            require_once 'includes/class-ebay-functions.php';

            require_once 'includes/class-ebay-account.php';

            require_once 'includes/class-ebay-form-handler.php';

            require_once 'includes/class-ebay-protected.php';

            require_once 'includes/admin/index.php';

            require_once 'includes/admin/export.php';

            require_once 'includes/admin/categories.php';

            require_once 'includes/admin/products.php';

            require_once 'includes/admin/orders.php';
            
            require_once 'includes/admin/messenger.php';

            require_once 'includes/class-template-loader.php';

            require_once 'includes/admin/export-sync.php';

            require_once 'includes/admin/update-to-ebay.php';

            require_once 'includes/admin/update-from-ebay.php';
        }

        public function __construct()
        {
            ob_start();

            // Auto-load classes on demand.
            if (function_exists('__autoload')) {
                spl_autoload_register('__autoload');
            }

            $this->ebay_define_constants();

            add_action('admin_init', array($this, 'register_ebay_settings'));

            $this->includes();
        }

        public function register_ebay_settings()
        {
            register_setting('ebay-settings-group', 'ebaygeneral_settings_mode', array($this, 'ebay_setting_validation_text_required'));
            
            register_setting('ebay-settings-group', 'ebay_dev_id', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'ebay_app_id', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'ebay_cert_id', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'ebay_ru_name', array($this, 'ebay_setting_validation_text_required'));
            // register_setting('ebay-settings-group', 'ebay_backlink', array($this, 'ebay_setting_validation_text_required'));
            
            register_setting('ebay-settings-group', 'ebay_paypal_id', array($this));
            register_setting('ebay-settings-group', 'ebay_email_id', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'item_location_pincode', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'global_shipping', array($this, 'ebay_setting_validation_text_required'));

            register_setting('ebay-settings-group', 'cron_interval', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'start_cron');
            register_setting('ebay-settings-group', 'wc_ebay_cron_start_date', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'get_process_for_cron');
            register_setting('ebay-settings-group', 'wc_ebay_cron_end_date', array($this, 'ebay_setting_validation_text_required'));

            register_setting('ebay-settings-group', 'ebay_default_category', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'ebay_order_status', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'ebay_accounts', array($this));
            register_setting('ebay-settings-group', 'ebay_default_stock', array($this, 'ebay_setting_validation_text_required'));

            register_setting('ebay-settings-group', 'returns_policy_options_define', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'returns_policy_options_refundOption', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'returns_policy_options_return_days', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'returns_policy_options_pay_by', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'returns_policy_options_other_info', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'ebay_listing_options_duration', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'dispatch_options_time', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'shipping_options_ship_service_priority', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'international_shipping_options_ship_service_priority', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'shipping_options_ship_service', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'international_shipping_options_ship_service', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'shipping_options_ship_service_cost', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'international_shipping_options_ship_service_cost', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'shipping_options_ship_service_additional_cost', array($this, 'shipping_options_ship_cost_callback'));
            register_setting('ebay-settings-group', 'international_shipping_options_ship_service_additional_cost', array($this, 'shipping_options_ship_cost_callback'));

            register_setting('ebay-settings-group', 'shipping_options_ship_time_min', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'shipping_options_ship_time_max', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'shipping_options_ship_free', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'countrycodetype', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'shipCountryCodeType', array($this, 'ebay_setting_validation_text_required'));
            
            register_setting('ebay-settings-group', 'last_fech_setting', array($this));
            
            register_setting('ebay-settings-group', 'ebay_slack_info_url', array($this, 'ebay_setting_validation_text_required'));
            register_setting('ebay-settings-group', 'ebay_slack_error_url', array($this, 'ebay_setting_validation_text_required'));
        }

        public function ebay_setting_validation_text_required($data)
        {
            if (null == $data) {
                add_settings_error(
                    'requiredTextFieldEmpty',
                    'empty',
                    'Fields cannot be empty',
                    'error'
                );
            }

            return sanitize_text_field($data);
        }

        public function shipping_options_ship_cost_callback($data)
        {
            $cost = get_option('shipping_options_ship_service_cost');

            if (intval($data) > intval($cost) || $data == null) {
                add_settings_error(
                    'requiredTextFieldEmpty',
                    'empty',
                    'Additional shipping cost cannot be greater than shipping cost OR cannot be left blank',
                    'error'
                );
            } else {
                return $data;
            }
        }

        public function ebay_define_constants()
        {
            define('EBAY_PLUGIN_FILE', __FILE__);

            define('EBAY_DIR', untrailingslashit(plugin_dir_path(__FILE__)));

            define('EBAY_VERSION', '1.0');

            define('EBAY_API', plugin_dir_url(__FILE__));
        }
    }

endif;

new eBay_Connector();
